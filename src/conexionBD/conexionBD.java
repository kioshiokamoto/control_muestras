/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexionBD;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author Okamoto
 */
public class conexionBD {
    public String usuario = "SOPORTE";
    public String password= "SOPORTE";
    public String url="jdbc:sqlserver://192.168.100.30;databasename=BDMUESTRAS";
//    public String url="jdbc:sqlserver://192.168.101.50;databasename=BDMUESTRAS";
//      public String url="jdbc:sqlserver://192.168.1.64;databasename=BDMUESTRAS";
//      public String url="jdbc:sqlserver://192.168.43.155;databasename=BDMUESTRAS";
    public Connection cn= null;
    public Statement st = null;
    public PreparedStatement pst = null;
    public Statement conexion(){
        try {
                cn = DriverManager.getConnection(url,usuario,password);
                st=cn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            //System.out.println("Conectado.");
        } 
        catch (SQLException e) 
        {
          System.err.println("Error. " + e);
          
        }
        return st;
    }
    public boolean ifExists(String sSQL, String nId) throws SQLException {
    PreparedStatement ps = cn.prepareStatement(sSQL);
    ps.setString(1, nId);
    ResultSet rs = ps.executeQuery();
    return rs.next();
    }
    public boolean ifExists2(String sSQL, String prom,String nId) throws SQLException {
    PreparedStatement ps = cn.prepareStatement(sSQL);
    ps.setString(1, prom);
    ps.setString(2, nId);
    ResultSet rs = ps.executeQuery();
    return rs.next();
    }
    public int StockInt(String codigo) {
    try (PreparedStatement stmt = cn.prepareStatement("SELECT stock FROM GENERAL_STOCK WHERE codigo = ?")) {
        stmt.setString(1, codigo);

        try(ResultSet rs = stmt.executeQuery()) {
            if (!rs.next()) {
                throw new SQLException("No se encontró registro con el codigo: " + codigo);
            }

            return rs.getInt("stock");
        }
    } catch (SQLException e) {
        JOptionPane.showMessageDialog(null, "SQLException:\n" + e, "Error: StockInt()", JOptionPane.ERROR_MESSAGE);
    }
        return 0;
    }
    public int StockPromotor(String codigo,String promotor) {
    try (PreparedStatement stmt = cn.prepareStatement("SELECT stock FROM STOCK_"+promotor +" WHERE codigo = ?")) {
        stmt.setString(1, codigo);

        try(ResultSet rs = stmt.executeQuery()) {
            if (!rs.next()) {
                throw new SQLException("No se encontró registro con el codigo: " + codigo);
            }

            return rs.getInt("stock");
        }
    } catch (SQLException e) {
        JOptionPane.showMessageDialog(null, "SQLException:\n" + e, "Error: StockInt()", JOptionPane.ERROR_MESSAGE);
    }
        return 0;
    }
    public Connection getConexion() {

        return this.cn;

    }

    
    

    
}
