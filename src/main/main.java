/*
AUTOR: KIOSHI OKAMOTO
 */
package main;


import main.muestras.entregaMuestra;
import main.clientes.Clientes;
import java.awt.BorderLayout;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import java.awt.*;
import java.awt.event.*;
import javax.swing.border.LineBorder;
import adicional.ComponentResizer;
import conexionBD.conexionBD;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import main.articulos.articulos;
import main.campana.campana;
import main.promotores.promotores;
import lc.kra.system.keyboard.GlobalKeyboardHook;
import lc.kra.system.keyboard.event.GlobalKeyAdapter;
import lc.kra.system.keyboard.event.GlobalKeyEvent;
import main.articulos.consultaStock;
import main.articulos.ingresoStockPrincipal;
import main.articulos.salidaStockPrincipal;
import main.campana.cambioCampana;
import main.muestras.consultaStockPromotor;
import main.muestras.devolucionMuestra;
import main.muestras.eliminacionMuestra;
import main.muestras.modificacionMuestra;
import main.promotores.cambioPromotor;
import com.sun.java.swing.plaf.windows.WindowsLookAndFeel;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.UIManager;
import main.analisis.analisisPrincipal;
import main.control.controlPrincipal;
import main.muestras.consultaDeDocSelect;
import main.muestras.consultadeDocumentos;
import main.tiempo.tiempoPrincipal;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.swing.JRViewer;


/**
 *
 * @author Okamoto
 */
public class main extends javax.swing.JFrame {
    
    private LineBorder border = new LineBorder(Color.RED,3);
    String campana;
    conexionBD conectar = new conexionBD();
    Statement st = conectar.conexion();
    public String rucAct,campanaActual;
    
    public main() {
        initComponents();
        ajustarPantalla();
        this.setLocationRelativeTo(null);   
        menu();
        this.getRootPane().setBorder(border);
        resize();
        barraInferior();
        globalKeyboard();
        promotorInicio();
        campanaInicio();
    }
    
    
    
    public void globalKeyboard(){
        GlobalKeyboardHook keyboardHook = new GlobalKeyboardHook(false);
        keyboardHook.addKeyListener(new GlobalKeyAdapter() {
			@Override 
                        public void keyPressed(GlobalKeyEvent event) {
//                            System.out.println(event);
                            if(event.getVirtualKeyCode()==GlobalKeyEvent.VK_ESCAPE){
                                
//                                System.out.println("funciona esto");
                            }
                            if(event.getVirtualKeyCode()==GlobalKeyEvent.VK_F11){
                                cambioDeCampana();
                            }
                            if(event.getVirtualKeyCode()==GlobalKeyEvent.VK_F12){
                                cambioDePromotor();
                            }
                            
			}
			@Override 
                        public void keyReleased(GlobalKeyEvent event) {
//                            System.out.println(event + "tambien esto");
                        }
		});
    }
    
    
    
    //FECHA
    public void barraInferior(){
        Date objDate = new Date();
        String strDateFormat = "dd-MM-YYYY";
        SimpleDateFormat objSDF = new SimpleDateFormat(strDateFormat);
//        System.out.println(objSDF.format(objDate));
        lFechaAct.setText(objSDF.format(objDate));
    }
    public void recibeUsuario(String usuario){
        lUsuarioAct.setText(usuario);
    }
    public void menu(){
        JMenuBar menubar = new JMenuBar();
        jPanel2.add(menubar,BorderLayout.NORTH);
        //MENU PRINCIPAL
//        JMenu resumen=new JMenu("Resumen");
        JMenu guias=new JMenu("Guías");
        JMenu control=new JMenu("Control");
        JMenu analisis=new JMenu("Análisis");
        JMenu tiempo= new JMenu("Tiempo");
        JMenu reportes=new JMenu("Reportes");
        JMenu utilidades= new JMenu("Utilidades");
//        menubar.add(resumen);    
        menubar.add(guias);
        menubar.add(control);
        menubar.add(analisis);
        menubar.add(tiempo);
//        menubar.add(reportes);
        menubar.add(utilidades);
        //SUBMENU CONTROL
        JMenuItem controlPrinc = new JMenuItem("Control de Entregas/Devoluciones");
        JMenuItem controlReporte = new JMenuItem("Reporte de muestras");
        control.add(controlPrinc);
        control.add(controlReporte);
        //SUBMENU ANALISIS
        JMenuItem aPrincipal = new JMenuItem("Analisis de promotor");
        analisis.add(aPrincipal);
        //SUBMENU GUIAS
        JMenuItem gEntrega= new JMenuItem("Entrega");
        JMenuItem gDevol= new JMenuItem("Devolución");
        JMenuItem gModDoc= new JMenuItem("Modificación de doc.");
        JMenuItem gElimDoc= new JMenuItem("Eliminación de doc.");
        JMenuItem gConsulta = new JMenuItem("Consulta Stock de promotor");
        JMenuItem gDocumentos = new JMenuItem("Consulta de documentos");
        guias.add(gEntrega);
        guias.add(gDevol);
        guias.add(gModDoc);
        guias.add(gElimDoc);
        guias.add(gDocumentos);
        guias.add(gConsulta);
        
        //SUBMENU UTILIDADES
        JMenu uCrear= new JMenu("Crear");
        JMenu stockGen=new JMenu("Stock Sistema");
        JMenuItem uIngresoStock= new JMenuItem("Ingreso Stock");
        JMenuItem uSalidaStock= new JMenuItem("Salida Stock");
        JMenuItem uCambioPromotor= new JMenuItem("Cambio Promotor");
        JMenuItem uCambioCampana= new JMenuItem("Cambio Campaña");
        JMenuItem uConsultaStock=new JMenuItem("Consulta Stock");
        utilidades.add(uCrear);
        utilidades.add(stockGen);
        stockGen.add(uConsultaStock);
        stockGen.add(uIngresoStock);
        stockGen.add(uSalidaStock);
        utilidades.add(uCambioPromotor);
        utilidades.add(uCambioCampana);
        JMenuItem ucCliente = new JMenuItem("Clientes");
        JMenuItem ucPromotores=new JMenuItem("Promotores");
        JMenuItem ucCampanas=new JMenuItem("Campañas");
        JMenuItem ucEdiciones=new JMenuItem("Ediciones - grados (Artículos)");
        uCrear.add(ucCliente);
        uCrear.add(ucPromotores);
        uCrear.add(ucCampanas);
        uCrear.add(ucEdiciones);
        
        //SUBMENU TIEMPO
        JMenuItem tPrincipal= new JMenuItem("Visualizar estancia de muestras");
        tiempo.add(tPrincipal);
        
        //ACTION LISTENER CONTROL
        controlPrinc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controlCampana();
                
            }
        });
        controlReporte.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                reporteControl();
            }
        });
        //ACTION LISTENER ANALISIS
        aPrincipal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                aPrincipal();
            }
        });
        //ACTION LISTENER TIEMPO
        tPrincipal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tiempo();
            }
        });
        
        //ACTION LISTENER STOCK EN SISTEMA
        uIngresoStock.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ingresoStock();
            }
        });
       uSalidaStock.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                salidaStock();
            }
        });
        uConsultaStock.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                consultaDeStock();
            }
        });
        //ACTION LISTENER PARA GUIAS
        gEntrega.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                entregaMuestra m1= new entregaMuestra();
                m1.promotor= rucAct;
                m1.campana=campanaActual;
                m1.setVisible(true); 
            }     
        });
        gDevol.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                devolucionMuestra dm= new devolucionMuestra();
                dm.campana=campanaActual;
                dm.promotor=rucAct;
//                System.out.println(rucAct);%
                dm.setVisible(true);
            }
        });
        gModDoc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                modificacionMuestra mm= new modificacionMuestra(new javax.swing.JFrame(),true);
                mm.promotor=rucAct;
                mm.campana=campanaActual;
                mm.completarTabla();
                mm.setVisible(true);
            }
        });
        gElimDoc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                eliminacionMuestra em= new eliminacionMuestra(new javax.swing.JFrame(),true);
                em.promotor=rucAct;
                em.campana=campanaActual;
                
                em.completarTabla();
                em.setVisible(true);
            }
        });
        gConsulta.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                consultaStockPromotor();
            }
            
        });
        gDocumentos.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                consultaDocumentos();
            }
        });
        
        //ACTION LISTENER PARA UTILIDADES
        ucCliente.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Clientes cc = new Clientes();
                cc.setVisible(true);
            }
        });
        ucPromotores.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                promotores prom=new promotores();
                prom.campana(lCampanaAct.getText());
                prom.setVisible(true);
            }
        });
        ucCampanas.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                creaCampana();
            }
        });
        ucEdiciones.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               articulos art=new articulos();
               art.setVisible(true);
            }
        });
        //ACTION LISTENER PARA CAMBIO DE PROMOTOR / CAMPAÑAS
        uCambioPromotor.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cambioDePromotor();
            }
        });
        uCambioCampana.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cambioDeCampana();
            }
        });
    } 
    public void reporteControl(){
        //REPORTE EN PDF
        conectar.conexion();
        SimpleDateFormat format1 = new SimpleDateFormat("dd-MM-yyyy");
        String date1 = lFechaAct.getText();
        JasperReport jr = null;
        JDialog reporte = new JDialog(new javax.swing.JFrame(),true);
        reporte.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getClassLoader().getResource("imagenes/coredise.png")));
        reporte.setSize(950,1050);
        reporte.setLocationRelativeTo(null);
        reporte.setTitle("Reporte de Control de muestras");
        try {
            Map parametro = new HashMap();
            parametro.put("parameter1",date1);
            parametro.put("parameter2",rucAct);
            parametro.put("parameter3",campanaActual);
            JasperPrint jp = JasperFillManager.fillReport(this.getClass().getClassLoader().getResourceAsStream("reportes/reporteControlGeneral.jasper"),parametro,conectar.getConexion());
            
            JRViewer jv = new JRViewer(jp);
            jv.setZoomRatio((float) 0.75);
            reporte.getContentPane().add(jv);
            
            reporte.setVisible(true);
        } catch (JRException ex) {
            Logger.getLogger(consultaDeDocSelect.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void aPrincipal(){
        analisisPrincipal ap = new analisisPrincipal(new javax.swing.JFrame(), true);
        ap.campana=campanaActual;
        ap.promotor=rucAct;
        ap.fecha=lFechaAct.getText();
        ap.completarTabla();
        ap.setVisible(true);
    }
    public void tiempo(){
        tiempoPrincipal tp = new tiempoPrincipal(new javax.swing.JFrame(),true);
        tp.campana=campanaActual;
        tp.promotor=rucAct;
        tp.fecha=lFechaAct.getText();
        tp.completarTabla();
        tp.setVisible(true);
    }
    
    public void controlCampana(){
        controlPrincipal cp= new controlPrincipal(new javax.swing.JFrame(),true);
        cp.promotor=rucAct;
        cp.campana=campanaActual;
        cp.completarTabla();
        
        cp.setVisible(true);
    }
    public void consultaStockPromotor(){
        consultaStockPromotor csp= new consultaStockPromotor(this,true);
        csp.promotor=rucAct;
        csp.completarTabla();
        csp.setVisible(true);
    }
    public void consultaDocumentos(){
        consultadeDocumentos cdd = new consultadeDocumentos(this, true);
        cdd.promotor=rucAct;
        cdd.campana=campanaActual;
        cdd.completarTabla();
        cdd.setVisible(true);
    }
    public void creaCampana() {
        campana cam=new campana(this,true);
        cam.setVisible(true);
    }
    public void ingresoStock(){
        ingresoStockPrincipal  isp= new  ingresoStockPrincipal(this,true);
        isp.setVisible(true);
    }
    public void salidaStock(){
        salidaStockPrincipal ssp= new salidaStockPrincipal(this,true);
        ssp.setVisible(true);
    }
    public void consultaDeStock(){
        consultaStock cs= new consultaStock();
        cs.setVisible(true);
//        cs.setAlwaysOnTop(true);//puede modificarse dependiendo de la comodidad del usuario
    }
    public void cambioDePromotor(){
        cambioPromotor cp= new cambioPromotor(this,true);
        cp.setVisible(true);
        if(!cp.isVisible() && cp.bandera==0){
            lPromotorAct.setText((String) cp.cboxPromotor.getSelectedItem());
            rucAct=cp.ruc.get(cp.promotores.indexOf((String) cp.cboxPromotor.getSelectedItem()));
            cp.dispose();
            System.out.println("el ruc es " + rucAct);
        }
    }
    public void cambioDeCampana(){
        cambioCampana cc= new cambioCampana(this,true);
        cc.setVisible(true);
        if(!cc.isVisible() && cc.bandera==0){
            lCampanaAct.setText((String) cc.cboxCampana.getSelectedItem());
            campanaActual=(String) cc.cboxCampana.getSelectedItem();
            cc.dispose();
        }
        
    }
    public void resize(){
        //Permite redimensionar la ventana
        ComponentResizer cr = new ComponentResizer(); 
        Toolkit tk = Toolkit.getDefaultToolkit();
        Dimension d = tk.getScreenSize();
        
        int ancho = (int)d.getWidth();
        int alto = (int)d.getHeight();
        cr.setMinimumSize(new Dimension(300, 300));
        cr.setMaximumSize(new Dimension(ancho, alto));
        cr.registerComponent(this);
        cr.setSnapSize(new Dimension(10, 10));
    }
    public void ajustarPantalla(){
        //Se utiliza para adecuarse a cualquier pantalla
        Toolkit tk = Toolkit.getDefaultToolkit();
        Dimension d = tk.getScreenSize();
        int ancho = (int)d.getWidth();
        int alto = (int)d.getHeight();
        this.setSize(ancho, alto-50);
    }
    
    
    public void restaurar(){
        Dimension d = new Dimension(1920,1030);
        
        if(this.getWidth()>=d.getWidth() && this.getHeight()>=d.getHeight()){
            this.setSize(this.getWidth()/2, this.getHeight()/2);
        }else{
            this.setSize(1920,1030);
        }
        this.setLocationRelativeTo(null);  
    }
    public void promotorInicio(){
        ArrayList <String> promotores = new ArrayList<String>();
        ArrayList <String> ruc = new ArrayList<String>();
        try{
            ResultSet rs = st.executeQuery("select promApellidos,promRuc from promotores where promActiv='ACTIVO' order by promApellidos asc");
            while(rs.next()){
                promotores.add(rs.getString("promApellidos"));
                ruc.add(rs.getString("promRuc"));
                
            }
        }catch(SQLException e){
            System.err.println("El error: "+ e);
        };
        lPromotorAct.setText(promotores.get(0));
        rucAct=ruc.get(0);
//        System.out.println("ruc" + rucAct);
        
    }
    public void campanaInicio(){
        ArrayList <String> campana = new ArrayList<String>();
        try{
            ResultSet rs = st.executeQuery("select nombre_campana from campana order by nombre_campana asc");
            while(rs.next()){
                campana.add(rs.getString("nombre_campana"));
            }
        }catch(SQLException e){
            System.err.println("El error: "+ e);
        };
        lCampanaAct.setText(campana.get(campana.size()-1));
        campanaActual=campana.get(campana.size()-1);
    }
    //Código automático
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jYearChooser1 = new com.toedter.calendar.JYearChooser();
        jPanel1 = new javax.swing.JPanel();
        exit = new javax.swing.JLabel();
        maxim = new javax.swing.JLabel();
        minim = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        lPromotor = new javax.swing.JLabel();
        lPromotorAct = new javax.swing.JLabel();
        lFecha = new javax.swing.JLabel();
        lFechaAct = new javax.swing.JLabel();
        lUsuario = new javax.swing.JLabel();
        lUsuarioAct = new javax.swing.JLabel();
        lCampana = new javax.swing.JLabel();
        lCampanaAct = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(246, 71, 71));
        jPanel1.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jPanel1MouseDragged(evt);
            }
        });
        jPanel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel1MouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanel1MousePressed(evt);
            }
        });

        exit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/salirMain.png"))); // NOI18N
        exit.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        exit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                exitMouseClicked(evt);
            }
        });

        maxim.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/maximMain.png"))); // NOI18N
        maxim.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        maxim.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                maximMouseClicked(evt);
            }
        });

        minim.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/minimMain.png"))); // NOI18N
        minim.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        minim.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                minimMouseClicked(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Sistema de control y análisis de muestras");
        jLabel1.setToolTipText("");
        jLabel1.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jLabel1MouseDragged(evt);
            }
        });
        jLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel1MouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel1MousePressed(evt);
            }
        });

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/coredise.png"))); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(jLabel2)
                .addGap(0, 0, 0)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 483, Short.MAX_VALUE)
                .addComponent(minim)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(maxim)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(exit))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(exit, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(minim)
                    .addComponent(maxim)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel2.setLayout(new java.awt.BorderLayout());

        jPanel3.setBackground(new java.awt.Color(236, 236, 236));

        lPromotor.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        lPromotor.setText("Promotor activo:");

        lPromotorAct.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N

        lFecha.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        lFecha.setText("Fecha:");

        lFechaAct.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N

        lUsuario.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        lUsuario.setText("Usuario:");

        lUsuarioAct.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N

        lCampana.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        lCampana.setText("Campaña:");

        lCampanaAct.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lPromotor)
                .addGap(18, 18, 18)
                .addComponent(lPromotorAct, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(49, 49, 49)
                .addComponent(lFecha)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lFechaAct, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lUsuario)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lUsuarioAct, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lCampana)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lCampanaAct, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(12, 12, 12))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lPromotor)
                        .addComponent(lPromotorAct, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lFecha)
                        .addComponent(lFechaAct, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lUsuario)
                        .addComponent(lUsuarioAct, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lCampana, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lCampanaAct, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 412, Short.MAX_VALUE)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    int xx,xy;
    private void jPanel1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel1MousePressed
        xx=evt.getX();
        xy=evt.getY();
    }//GEN-LAST:event_jPanel1MousePressed

    private void jPanel1MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel1MouseDragged
        int x = evt.getXOnScreen();
        int y = evt.getYOnScreen();
        this.setLocation(x-xx,y-xy);
    }//GEN-LAST:event_jPanel1MouseDragged

    private void exitMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_exitMouseClicked
        System.exit(0);
    }//GEN-LAST:event_exitMouseClicked

    private void minimMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_minimMouseClicked
        this.setExtendedState(ICONIFIED);
    }//GEN-LAST:event_minimMouseClicked

    private void maximMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_maximMouseClicked
        restaurar();
    }//GEN-LAST:event_maximMouseClicked
    
    private void jLabel1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MousePressed
        xx=evt.getX();
        xy=evt.getY();
    }//GEN-LAST:event_jLabel1MousePressed

    private void jLabel1MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseDragged
        int x = evt.getXOnScreen();
        int y = evt.getYOnScreen();
        this.setLocation(x-xx,y-xy);
    }//GEN-LAST:event_jLabel1MouseDragged

    private void jPanel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel1MouseClicked
        if(evt.getClickCount() == 2){
            restaurar();
        }
    }//GEN-LAST:event_jPanel1MouseClicked

    private void jLabel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseClicked
        if(evt.getClickCount() == 2){
            restaurar();
        }
    }//GEN-LAST:event_jLabel1MouseClicked
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new main().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel exit;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private com.toedter.calendar.JYearChooser jYearChooser1;
    private javax.swing.JLabel lCampana;
    private javax.swing.JLabel lCampanaAct;
    private javax.swing.JLabel lFecha;
    private javax.swing.JLabel lFechaAct;
    private javax.swing.JLabel lPromotor;
    private javax.swing.JLabel lPromotorAct;
    private javax.swing.JLabel lUsuario;
    private javax.swing.JLabel lUsuarioAct;
    private javax.swing.JLabel maxim;
    private javax.swing.JLabel minim;
    // End of variables declaration//GEN-END:variables
}
