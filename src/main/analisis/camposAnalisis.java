/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.analisis;

/**
 *
 * @author Okamoto
 */
public class camposAnalisis {
    private String articulo;
    private String totalEntregadoPromotor;
    private String netoEntregadoColegio;
    private String netoDevueltoColegio;
    private String totalMuestrasPromotor;
    public camposAnalisis(String articulo,String totalEntregadoPromotor,String netoEntregadoColegio,String netoDevueltoColegio,String totalMuestrasPromotor){
        this.articulo=articulo;
        this.totalEntregadoPromotor=totalEntregadoPromotor;
        this.netoEntregadoColegio=netoEntregadoColegio;
        this.netoDevueltoColegio=netoDevueltoColegio;
        this.totalMuestrasPromotor=totalMuestrasPromotor;
    }
    public String getArticulo(){
        return articulo;
    }
    public void setArticulo(String articulo){
        this.articulo=articulo;
    }
    public String getTotalEntregadoPromotor(){
        return totalEntregadoPromotor;
    }
    public void setTotalEntregadoPromotor(String totalEntregadoPromotor){
        this.totalEntregadoPromotor=totalEntregadoPromotor;
    }
    public String getNetoEntregadoColegio(){
        return netoEntregadoColegio;
    }
    public void setNetoEntregadoColegio(String netoEntregadoColegio){
        this.netoEntregadoColegio=netoEntregadoColegio;
    }
    public String getNetoDevueltoColegio(){
        return netoDevueltoColegio;
    }
    public void setNetoDevueltoColegio(String netoDevueltoColegio){
        this.netoDevueltoColegio=netoDevueltoColegio;
    }
    public String getTotalMuestrasPromotor(){
        return totalMuestrasPromotor;
    }
    public void setTotalMuestrasPromotor(String totalMuestrasPromotor){
        this.totalMuestrasPromotor=totalMuestrasPromotor;
    }
    
    
}
