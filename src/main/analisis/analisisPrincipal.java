/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.analisis;

import conexionBD.conexionBD;
import java.awt.Color;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JTable;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import main.tiempo.camposTiempo;
import main.tiempo.tiempoPrincipal;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.swing.JRViewer;

/**
 *
 * @author Okamoto
 */
public class analisisPrincipal extends javax.swing.JDialog {
    private LineBorder border = new LineBorder(Color.RED,2);
    int xx,xy;
    DefaultTableModel modelo = new DefaultTableModel(){
        public boolean isCellEditable(int rowIndex,int columnIndex){return false;}
    };
    conexionBD conectar = new conexionBD();
    Statement st = conectar.conexion();
    public String promotor,nombrePromotor;
    public String campana,fecha;
    ArrayList <String> codigoArt= new ArrayList<String>();
    ArrayList <Integer> totalEntregadoPromotor = new ArrayList<Integer>();
    ArrayList <Integer> totalDevueltoPromotor = new ArrayList<Integer>();
    ArrayList <Integer> netoEntregadoPromotor = new ArrayList<Integer>();
    
    ArrayList <Integer> totalEntregadoColegio = new ArrayList<Integer>();
    ArrayList <Integer> totalDevueltoColegio = new ArrayList<Integer>();
    ArrayList <Integer> netoEntregadoColegio = new ArrayList<Integer>();
    
    ArrayList <Integer> stockEnPromotor = new ArrayList<Integer>();
    /**
     * Creates new form analisisPrincipal
     */
    public analisisPrincipal(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null); 
        this.getRootPane().setBorder(border);
        
    }

    public void completarTabla(){
        modelo.addColumn("Articulo");
        modelo.addColumn("Total Entregado promotor");
        modelo.addColumn("Neto Entregado colegio");
        modelo.addColumn("Neto Devuelto colegio");
        modelo.addColumn("Total muestras promotor");
        
        
        TableColumnModel columnModel = tablaControl.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(321);
        columnModel.getColumn(1).setPreferredWidth(160);
        columnModel.getColumn(2).setPreferredWidth(160);
        columnModel.getColumn(3).setPreferredWidth(160);
        columnModel.getColumn(4).setPreferredWidth(160);

        
        tablaControl.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        
        actualizarTabla();
    }
    public void actualizarTabla(){
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
        
        String cargoD1=null;
        String cargoD2=null;
        modelo.setRowCount(0);
        try{
            ResultSet rs = st.executeQuery("SELECT descrip,codigo FROM STOCK_"+promotor +" ORDER BY descrip ASC");
            while(rs.next()){
                int filaModificar = rs.getRow();
                Object [] fila = new Object[2];
                for (int i=0;i<2;i++){
                    fila[i] = rs.getObject(i+1);
                }     
                codigoArt.add((String)fila[1]);
                modelo.addRow(fila);
            }
        }catch(SQLException e){
            System.err.println("El error: "+ e);
        };
        //ALMECENA EN MEMORIA EL TOTAL ENTREGADO
        try{
            for(int i=0; i<codigoArt.size();i++){
                ResultSet rs = st.executeQuery("Select SUM(stock) from movimiento_general where tipoMov='IS' and promDoc=\'"+promotor+"\' and codigo=\'"+codigoArt.get(i)+"\'");
                while(rs.next()){
                   totalEntregadoPromotor.add(rs.getInt(1));
//                   tablaControl.setValueAt(totalEntregadoPromotor, i, 1);
                }
            }
        }catch(SQLException e){
            System.err.println("El error: "+ e);
        }
        //ALMACENA EN MEMORIA TOTAL DEVUELTO
        try{
            for(int i=0; i<codigoArt.size();i++){
                ResultSet rs = st.executeQuery("Select SUM(stock) from movimiento_general where tipoMov='SS' and promDoc=\'"+promotor+"\' and codigo=\'"+codigoArt.get(i)+"\'");
                while(rs.next()){
                   totalDevueltoPromotor.add(rs.getInt(1));
//                   tablaControl.setValueAt(totalEntregadoPromotor, i, 1);
                }
            }
        }catch(SQLException e){
            System.err.println("El error: "+ e);
        }
        //RESTA LO ENTREGADO Y DEVUELTO A PROMOTOR - LO ASIGNA EN LA TABLA
        for(int i=0; i<codigoArt.size();i++){
            netoEntregadoPromotor.add( totalEntregadoPromotor.get(i) - totalDevueltoPromotor.get(i));
            tablaControl.setValueAt(netoEntregadoPromotor.get(i), i, 1);
        }
        //ALMECENA EN MEMORIA EL TOTAL ENTREGADO A COLEGIO
        try{
            for(int i=0; i<codigoArt.size();i++){
                ResultSet rs = st.executeQuery("Select sum(stockEntregado) from MOV_DOCUMENTOS_COLEGIOS where campana=\'"+campana+"\' and rucProm=\'"+promotor+"\' and codigo=\'"+codigoArt.get(i)+"\'");
                while(rs.next()){
                   totalEntregadoColegio.add(rs.getInt(1));
                   tablaControl.setValueAt(totalEntregadoColegio.get(i), i, 2);
                }
            }
        }catch(SQLException e){
            System.err.println("El error: "+ e);
        }
        //ALMECENA EN MEMORIA EL TOTAL DEVUELTO DE COLEGIO
        try{
            for(int i=0; i<codigoArt.size();i++){
                ResultSet rs = st.executeQuery("Select sum(stockDevuelto) from MOV_DOCUMENTOS_COLEGIOS where campana=\'"+campana+"\' and rucProm=\'"+promotor+"\' and codigo=\'"+codigoArt.get(i)+"\'");
                while(rs.next()){
                   totalDevueltoColegio.add(rs.getInt(1));
                   tablaControl.setValueAt(totalDevueltoColegio.get(i), i, 3);
                }
            }
        }catch(SQLException e){
            System.err.println("El error: "+ e);
        }
        //STOCK EN PROMOTOR
        for(int i=0; i<codigoArt.size();i++){
            stockEnPromotor.add( netoEntregadoPromotor.get(i) - totalEntregadoColegio.get(i));
            tablaControl.setValueAt(stockEnPromotor.get(i), i, 4);
        }
              

    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelTabla = new javax.swing.JPanel();
        jScrolCliente = new javax.swing.JScrollPane();
        tablaControl = new javax.swing.JTable(modelo);
        jPanel2 = new javax.swing.JPanel();
        lControl = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);

        tablaControl.getTableHeader().setResizingAllowed(false);
        tablaControl.getTableHeader().setReorderingAllowed(false);
        jScrolCliente.setViewportView(tablaControl);

        javax.swing.GroupLayout panelTablaLayout = new javax.swing.GroupLayout(panelTabla);
        panelTabla.setLayout(panelTablaLayout);
        panelTablaLayout.setHorizontalGroup(
            panelTablaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelTablaLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(jScrolCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 967, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelTablaLayout.setVerticalGroup(
            panelTablaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelTablaLayout.createSequentialGroup()
                .addGap(0, 6, Short.MAX_VALUE)
                .addComponent(jScrolCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 379, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel2.setBackground(new java.awt.Color(246, 71, 71));

        lControl.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        lControl.setForeground(new java.awt.Color(255, 255, 255));
        lControl.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lControl.setText("Analisis de promotor");
        lControl.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                lControlMouseDragged(evt);
            }
        });
        lControl.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                lControlMousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lControl, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 1011, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lControl, javax.swing.GroupLayout.DEFAULT_SIZE, 27, Short.MAX_VALUE)
        );

        jButton1.setText("Salir");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Reporte");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(760, Short.MAX_VALUE)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(14, 14, 14))
        );

        jButton2.getAccessibleContext().setAccessibleName("bReporte");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelTabla, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(51, Short.MAX_VALUE)
                .addComponent(panelTabla, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 494, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void lControlMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lControlMouseDragged
        int x = evt.getXOnScreen();
        int y = evt.getYOnScreen();
        this.setLocation(x-xx,y-xy);
    }//GEN-LAST:event_lControlMouseDragged

    private void lControlMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lControlMousePressed
        xx=evt.getX();
        xy=evt.getY();
    }//GEN-LAST:event_lControlMousePressed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        int fila=0;
        String datos="";
        List resultados = new ArrayList();
        camposAnalisis tipo;
        resultados.clear();
        for(fila=0;fila<tablaControl.getRowCount();fila++){
            tipo = new camposAnalisis(String.valueOf(tablaControl.getValueAt(fila, 0)), String.valueOf(tablaControl.getValueAt(fila, 1)), String.valueOf(tablaControl.getValueAt(fila, 2)), String.valueOf(tablaControl.getValueAt(fila, 3)), String.valueOf(tablaControl.getValueAt(fila, 4)));
            resultados.add(tipo);
        }
        Map map = new HashMap();
        JasperPrint jPrint;
        JDialog reporte = new JDialog(new javax.swing.JFrame(),true);
        reporte.setSize(950,1050);
        reporte.setLocationRelativeTo(null);
        reporte.setTitle("ANALISIS DE PROMOTOR");
        map.put("parameter1","ANALISIS DE PROMOTOR");
        map.put("parameter2",fecha);
        map.put("parameter3",promotor);
        
        try {
            
            jPrint = JasperFillManager.fillReport(this.getClass().getClassLoader().getResourceAsStream("reportes/analisisPromotor1.jasper"), map,new JRBeanCollectionDataSource(resultados));
            
            JRViewer jv = new JRViewer(jPrint);
            jv.setZoomRatio((float) 0.75);
            reporte.getContentPane().add(jv);
            
            reporte.setVisible(true);
//            reporte.setAlwaysOnTop(true);
        } catch (JRException ex) {
            
            Logger.getLogger(tiempoPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(analisisPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(analisisPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(analisisPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(analisisPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                analisisPrincipal dialog = new analisisPrincipal(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrolCliente;
    private javax.swing.JLabel lControl;
    private javax.swing.JPanel panelTabla;
    private javax.swing.JTable tablaControl;
    // End of variables declaration//GEN-END:variables
}
