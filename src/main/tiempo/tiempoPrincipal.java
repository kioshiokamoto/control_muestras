/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.tiempo;

import conexionBD.conexionBD;
import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableRowSorter;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.swing.JRViewer;

/**
 *
 * @author Okamoto
 */
public class tiempoPrincipal extends javax.swing.JDialog {
    private LineBorder border = new LineBorder(Color.RED,2);
    int xx,xy;
    DefaultTableModel dm ;
    DefaultTableModel modelo = new DefaultTableModel(){
        public boolean isCellEditable(int rowIndex,int columnIndex){return false;}
    };
    conexionBD conectar = new conexionBD();
    Statement st = conectar.conexion();
    public String promotor,campana,fecha;
    
    /**
     * Creates new form tiempoPrincipal
     */
    public tiempoPrincipal(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null); 
        this.getRootPane().setBorder(border);
    }
    public void completarTabla(){
        modelo.addColumn("Estado");
        modelo.addColumn("Fecha Entrega");
        modelo.addColumn("Cargo Entrega");
        modelo.addColumn("Colegio/Institución");
        modelo.addColumn("Fecha devolución");
        modelo.addColumn("Días");

        
        
        TableColumnModel columnModel = tablaControl.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(82);;
        columnModel.getColumn(1).setPreferredWidth(110);
        columnModel.getColumn(2).setPreferredWidth(100);
        columnModel.getColumn(3).setPreferredWidth(267);
        columnModel.getColumn(4).setPreferredWidth(110);
        columnModel.getColumn(5).setPreferredWidth(45);
       
        
        tablaControl.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        actualizarTabla();
        
    }

    public void actualizarTabla(){
        java.util.Date utilDate = new java.util.Date();
        java.sql.Date fechaActual = new java.sql.Date(utilDate.getTime());
        
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
        String fecha = df.format(fechaActual);
        final long MILLSECS_POR_DIA = 24 * 60 * 60 * 1000;

        

        String cargoEntrega = null;
        Object fechaDevol=null;
        Object fechaDevol1=null;
        Object fechaDevol2=null;
        Object fechaEntrega=null;
        String estado=null;
        long dias;
        modelo.setRowCount(0);
        try{
            ResultSet rs = st.executeQuery("SELECT estado,fechaEntrega,numCargoEntrega,colegio FROM DOCUMENTOS_COLEGIOS where rucProm=\'"+promotor+"\' and campana=\'"+campana+"\' ORDER BY fechaEntrega DESC");
            while(rs.next()){
                int filaModificar = rs.getRow();
                Object [] fila = new Object[4];
                for (int i=0;i<4;i++){
                    fila[i] = rs.getObject(i+1);
                }     
                modelo.addRow(fila);
                tablaControl.setValueAt(df.format(fila[1]), filaModificar-1, 1);
            }
        }catch(SQLException e){
            System.err.println("El error: "+ e);
        };
        try{
            ResultSet rs1 = st.executeQuery("SELECT estado,fechaDevol,fechaDevolAD1,fechaDevolAD2,numCargoEntrega,fechaEntrega from DOCUMENTOS_COLEGIOS WHERE rucProm=\'"+promotor+"\' and campana=\'"+campana+"\' ORDER BY fechaEntrega DESC");
            while(rs1.next()){
                Object [] fila = new Object[6];
                for (int i=0;i<6;i++){
                    fila[i] = rs1.getObject(i+1);
                }  
                estado= (String) fila[0];
                fechaDevol =  (Date) fila[1];
                fechaDevol1= (Date) fila[2];
                fechaDevol2= (Date) fila[3];
                cargoEntrega = (String) fila[4];
                fechaEntrega = (Date) fila[5];
                java.sql.Date Devolucion = (java.sql.Date) fechaDevol;
                java.sql.Date Devolucion1 = (java.sql.Date) fechaDevol1;
                java.sql.Date Devolucion2 = (java.sql.Date) fechaDevol2;
                java.sql.Date entrega = (java.sql.Date) fechaEntrega;
                
                if(estado.equals("DEVUELTO")){
                    int filaModificar = rs1.getRow();

                    if(!(fechaDevol==null)){
                        fecha = df.format(fechaDevol);
                        tablaControl.setValueAt(fecha, filaModificar-1, 4);
                        dias = (Devolucion.getTime() - entrega.getTime())/MILLSECS_POR_DIA;
                        tablaControl.setValueAt(dias, filaModificar-1, 5);
                        
                    }
                    if(!(fechaDevol1==null)){
                        fecha = df.format(fechaDevol1);
                        tablaControl.setValueAt(fecha, filaModificar-1, 4);
                        dias = (Devolucion1.getTime() - entrega.getTime())/MILLSECS_POR_DIA;
                        tablaControl.setValueAt(dias, filaModificar-1, 5);
                    }
                    if(!(fechaDevol2==null)){
                        fecha = df.format(fechaDevol2);
                        tablaControl.setValueAt(fecha, filaModificar-1, 4);
                        dias = (Devolucion2.getTime() - entrega.getTime())/MILLSECS_POR_DIA;
                        tablaControl.setValueAt(dias, filaModificar-1, 5);
                    }

                }else{
                    fechaActual = new java.sql.Date(utilDate.getTime());
                    fecha = df.format(fechaActual);
                    int filaModificar = rs1.getRow();
                    tablaControl.setValueAt(fecha, filaModificar-1, 4);
                    dias = (fechaActual.getTime() - entrega.getTime())/MILLSECS_POR_DIA;
                    tablaControl.setValueAt(dias, filaModificar-1, 5); 
                }
            }
            
        }catch(SQLException e){
            
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        textBuscador = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        comboFiltro = new javax.swing.JComboBox<>();
        jPanel4 = new javax.swing.JPanel();
        bSalir = new javax.swing.JButton();
        bReporte = new javax.swing.JButton();
        panelTabla = new javax.swing.JPanel();
        jScrolCliente = new javax.swing.JScrollPane();
        tablaControl = new javax.swing.JTable(modelo);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(246, 71, 71));

        jLabel1.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Tiempo de muestras entregadas");
        jLabel1.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jLabel1MouseDragged(evt);
            }
        });
        jLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel1MousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 754, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
        );

        textBuscador.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        textBuscador.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textBuscadorKeyTyped(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel2.setText("Buscar:");

        jLabel3.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel3.setText("Por:");

        comboFiltro.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        comboFiltro.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Estado","Fecha entrega", "Cargo entrega","Colegio", "Fecha devolucion" }));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(textBuscador, javax.swing.GroupLayout.PREFERRED_SIZE, 313, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(38, 38, 38)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(comboFiltro, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(30, 30, 30))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2)
                        .addComponent(textBuscador, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel3))
                    .addComponent(comboFiltro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(40, 40, 40))
        );

        bSalir.setText("Salir");
        bSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bSalirActionPerformed(evt);
            }
        });

        bReporte.setText("Reporte");
        bReporte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bReporteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(509, Short.MAX_VALUE)
                .addComponent(bReporte, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(bSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bReporte, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(28, Short.MAX_VALUE))
        );

        tablaControl.getTableHeader().setResizingAllowed(false);
        tablaControl.getTableHeader().setReorderingAllowed(false);
        jScrolCliente.setViewportView(tablaControl);

        javax.swing.GroupLayout panelTablaLayout = new javax.swing.GroupLayout(panelTabla);
        panelTabla.setLayout(panelTablaLayout);
        panelTablaLayout.setHorizontalGroup(
            panelTablaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelTablaLayout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(jScrolCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 720, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelTablaLayout.setVerticalGroup(
            panelTablaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelTablaLayout.createSequentialGroup()
                .addContainerGap(14, Short.MAX_VALUE)
                .addComponent(jScrolCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(panelTabla, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(panelTabla, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(31, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 611, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel1MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseDragged
        int x = evt.getXOnScreen();
        int y = evt.getYOnScreen();
        this.setLocation(x-xx,y-xy);
    }//GEN-LAST:event_jLabel1MouseDragged

    private void jLabel1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MousePressed
        xx=evt.getX();
        xy=evt.getY();
    }//GEN-LAST:event_jLabel1MousePressed

    private void bSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bSalirActionPerformed
        dispose();
    }//GEN-LAST:event_bSalirActionPerformed
    TableRowSorter trs=null;
    private void textBuscadorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textBuscadorKeyTyped
        if(comboFiltro.getSelectedIndex()==0){
            textBuscador.addKeyListener(new KeyAdapter() {
                @Override
                public void keyReleased(KeyEvent ke){
                    trs.setRowFilter(RowFilter.regexFilter("(?i)"+textBuscador.getText(), 0));
                }
            });
            trs =new TableRowSorter(modelo);
            tablaControl.setRowSorter(trs);
        }
        if(comboFiltro.getSelectedIndex()==1){
            textBuscador.addKeyListener(new KeyAdapter() {
                @Override
                public void keyReleased(KeyEvent ke){
                    trs.setRowFilter(RowFilter.regexFilter("(?i)"+textBuscador.getText(), 1));
                }
            });
            trs =new TableRowSorter(modelo);
            tablaControl.setRowSorter(trs);
        }
        if(comboFiltro.getSelectedIndex()==2){
            textBuscador.addKeyListener(new KeyAdapter() {
                @Override
                public void keyReleased(KeyEvent ke){
                    trs.setRowFilter(RowFilter.regexFilter("(?i)"+textBuscador.getText(), 2));
                }
            });
            trs =new TableRowSorter(modelo);
            tablaControl.setRowSorter(trs);
        }
        if(comboFiltro.getSelectedIndex()==3){
            textBuscador.addKeyListener(new KeyAdapter() {
                @Override
                public void keyReleased(KeyEvent ke){
                    trs.setRowFilter(RowFilter.regexFilter("(?i)"+textBuscador.getText(), 3));
                }
            });
            trs =new TableRowSorter(modelo);
            tablaControl.setRowSorter(trs);
        }
        if(comboFiltro.getSelectedIndex()==4){
            textBuscador.addKeyListener(new KeyAdapter() {
                @Override
                public void keyReleased(KeyEvent ke){
                    trs.setRowFilter(RowFilter.regexFilter("(?i)"+textBuscador.getText(), 4));
                }
            });
            trs =new TableRowSorter(modelo);
            tablaControl.setRowSorter(trs);
        }
    }//GEN-LAST:event_textBuscadorKeyTyped

    private void bReporteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bReporteActionPerformed
        int fila=0;
        String datos="";
        List resultados = new ArrayList();
        camposTiempo tipo;
        resultados.clear();
        for(fila=0;fila<tablaControl.getRowCount();fila++){
            tipo = new camposTiempo(String.valueOf(tablaControl.getValueAt(fila, 0)), String.valueOf(tablaControl.getValueAt(fila, 1)), String.valueOf(tablaControl.getValueAt(fila, 2)), String.valueOf(tablaControl.getValueAt(fila, 3)), String.valueOf(tablaControl.getValueAt(fila, 4)), String.valueOf(tablaControl.getValueAt(fila, 5)));
            resultados.add(tipo);
        }
        Map map = new HashMap();
        JasperPrint jPrint;
        JDialog reporte = new JDialog(new javax.swing.JFrame(),true);
        reporte.setSize(950,1050);
        reporte.setLocationRelativeTo(null);
        reporte.setTitle("Reporte de tiempo");
        
        map.put("titulo","Reporte de tiempo de muestras");
        map.put("fecha",fecha);
        
        
        try {
            
            jPrint = JasperFillManager.fillReport(this.getClass().getClassLoader().getResourceAsStream("reportes/tiempoFinal.jasper"), map,new JRBeanCollectionDataSource(resultados));
            
            JRViewer jv = new JRViewer(jPrint);
            jv.setZoomRatio((float) 0.75);
            reporte.getContentPane().add(jv);
            
            reporte.setVisible(true);
//            reporte.setAlwaysOnTop(true);
        } catch (JRException ex) {
            
            Logger.getLogger(tiempoPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }//GEN-LAST:event_bReporteActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(tiempoPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(tiempoPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(tiempoPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(tiempoPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                tiempoPrincipal dialog = new tiempoPrincipal(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bReporte;
    private javax.swing.JButton bSalir;
    private javax.swing.JComboBox<String> comboFiltro;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrolCliente;
    private javax.swing.JPanel panelTabla;
    private javax.swing.JTable tablaControl;
    private javax.swing.JTextField textBuscador;
    // End of variables declaration//GEN-END:variables
}
