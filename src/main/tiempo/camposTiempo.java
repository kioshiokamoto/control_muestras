/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.tiempo;

/**
 *
 * @author Okamoto
 */
public class camposTiempo {
    private String estado;
    private String fechaEntrega;
    private String cargo;
    private String colegio;
    private String fechaDevolucion;
    private String dias;
    public camposTiempo(String estado,String fechaEntrega,String cargo,String colegio,String fechaDevolucion,String dias){
        this.estado=estado;
        this.fechaEntrega=fechaEntrega;
        this.cargo=cargo;
        this.colegio=colegio;
        this.fechaDevolucion=fechaDevolucion;
        this.dias=dias;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * @return the fechaEntrega
     */
    public String getFechaEntrega() {
        return fechaEntrega;
    }

    /**
     * @param fechaEntrega the fechaEntrega to set
     */
    public void setFechaEntrega(String fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    /**
     * @return the cargo
     */
    public String getCargo() {
        return cargo;
    }

    /**
     * @param cargo the cargo to set
     */
    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    /**
     * @return the colegio
     */
    public String getColegio() {
        return colegio;
    }

    /**
     * @param colegio the colegio to set
     */
    public void setColegio(String colegio) {
        this.colegio = colegio;
    }

    /**
     * @return the fechaDevolucion
     */
    public String getFechaDevolucion() {
        return fechaDevolucion;
    }

    /**
     * @param fechaDevolucion the fechaDevolucion to set
     */
    public void setFechaDevolucion(String fechaDevolucion) {
        this.fechaDevolucion = fechaDevolucion;
    }

    /**
     * @return the dias
     */
    public String getDias() {
        return dias;
    }

    /**
     * @param dias the dias to set
     */
    public void setDias(String dias) {
        this.dias = dias;
    }
    
}
