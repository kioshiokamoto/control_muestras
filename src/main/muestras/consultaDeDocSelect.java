/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.muestras;

import conexionBD.conexionBD;
import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.InputStream;
import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.swing.JRViewer;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Okamoto
 */
public class consultaDeDocSelect extends javax.swing.JDialog {
    private LineBorder border = new LineBorder(Color.RED,2);
    int xx,xy;
    DefaultTableModel dm ;
    DefaultTableModel modelo = new DefaultTableModel(){
        public boolean isCellEditable(int rowIndex,int columnIndex){return false;}
    };
    conexionBD conectar = new conexionBD();
    Statement st = conectar.conexion();
    public String promotor,campana;
    public String documento;
    ArrayList<Integer> stock = new ArrayList<Integer>();
    ArrayList<String> codigo= new ArrayList<String>();
            
    /**
     * Creates new form modificacionEntregaMuestra
     */
    public consultaDeDocSelect(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null); 
        this.getRootPane().setBorder(border);
//        modificarFecha();
        
    }
    public void modificarFecha(){
        java.util.Date fechaActual = new java.util.Date();
        fecha.setDate(fechaActual);   
    }
    public void pasarDatos(String documento,Date fecha,String observacion){
        tNumDoc.setText(documento);
        this.fecha.setDate(fecha);
        tObs.setText(observacion);
    }
    public void completarTabla(){
        tableArticulos.getTableHeader().setResizingAllowed(false);
        tableArticulos.getTableHeader().setReorderingAllowed(false);
        modelo.addColumn("Codigo");
        modelo.addColumn("Descripcion");
        modelo.addColumn("Cantidad");
        modelo.addColumn("Observacion");
        
        TableColumnModel columnModel = tableArticulos.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(110);
        columnModel.getColumn(1).setPreferredWidth(200);
        columnModel.getColumn(2).setPreferredWidth(70);
        columnModel.getColumn(3).setPreferredWidth(265);
//        System.out.println("el doc es" + documento);
        tableArticulos.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        modelo.setRowCount(0);
        try{
            ResultSet rs = st.executeQuery("SELECT codigo,descrip,stock,obsArt FROM MOVIMIENTO_GENERAL WHERE numDoc='"+documento+"'");
            while(rs.next()){
                Object [] fila = new Object[4];
                for (int i=0;i<4;i++){
                    fila[i] = rs.getObject(i+1);

//                    codigo.add(rs.getString("codigo"));
                    
                }     
                stock.add(rs.getInt("stock"));
                codigo.add(rs.getString("codigo"));
                modelo.addRow(fila);
            }
            

//            System.out.println("el tamaño es " + stock.size());
//            for(int i=0;i<stock.size();i++){
//                System.out.println("stock es "+ stock.get(i) );
//            }
        }catch(SQLException e){
            System.err.println("El error: "+ e);
        };
        
        
        
        
        
    }
    public void selectorDeStock(){
        entregaStockSelect ess = new entregaStockSelect(new javax.swing.JFrame(),true);
        ess.setVisible(true);
        
        if(!ess.isVisible() && filtro(ess.textCodigo.getText()) ){
            if(ess.bandera==0){
                Object [] fila = new Object[4];
                fila[0]= ess.textCodigo.getText();
                fila[1]= ess.textDesc.getText();
                fila[2]= ess.textCantidad.getText();
                fila[3]= ess.textObs.getText();
                modelo.addRow(fila); 
            }
        }     
        else{
            int mensaje = JOptionPane.showOptionDialog(null, "No se permite ingresar articulo nuevamente en tabla", "ERROR", JOptionPane.DEFAULT_OPTION,
                     JOptionPane.ERROR_MESSAGE, null, null, null);   
        }
    }
    private boolean filtro(String texto){
        for (int i = 0; i < tableArticulos.getRowCount(); i++){
             if(tableArticulos.getValueAt(i, 0).toString().equals(texto)){
                System.out.println("Ya existe articulo en tabla");
                return false;
             }
        }
        return true;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel4 = new javax.swing.JPanel();
        bCancelar = new javax.swing.JButton();
        bReporte = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        scrollArticulos = new javax.swing.JScrollPane();
        tableArticulos = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        lnumDoc = new javax.swing.JLabel();
        lFecha = new javax.swing.JLabel();
        lObs = new javax.swing.JLabel();
        tNumDoc = new javax.swing.JTextField();
        fecha = new com.toedter.calendar.JDateChooser("dd/MM/yyyy", "##/##/####", '_');
        jObs = new javax.swing.JScrollPane();
        tObs = new javax.swing.JTextArea();
        jSeparator1 = new javax.swing.JSeparator();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);

        bCancelar.setText("Cancelar");
        bCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bCancelarActionPerformed(evt);
            }
        });

        bReporte.setText("Reporte");
        bReporte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bReporteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(bReporte, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(bCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bReporte, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(20, Short.MAX_VALUE))
        );

        tableArticulos.setModel(modelo);
        scrollArticulos.setViewportView(tableArticulos);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(scrollArticulos, javax.swing.GroupLayout.PREFERRED_SIZE, 652, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrollArticulos, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        lnumDoc.setText("Nro. Documento:");

        lFecha.setText("Fecha de entrega:");

        lObs.setText("Observación:");

        tNumDoc.setEnabled(false);
        tNumDoc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tNumDocActionPerformed(evt);
            }
        });

        fecha.setAutoscrolls(true);
        fecha.setEnabled(false);

        tObs.setColumns(20);
        tObs.setRows(5);
        tObs.setEnabled(false);
        jObs.setViewportView(tObs);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(68, 68, 68)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(lnumDoc)
                        .addGap(18, 18, 18)
                        .addComponent(tNumDoc, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lFecha)
                        .addGap(18, 18, 18)
                        .addComponent(fecha, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(lObs)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jObs, javax.swing.GroupLayout.PREFERRED_SIZE, 445, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jSeparator1)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lnumDoc)
                        .addComponent(lFecha)
                        .addComponent(tNumDoc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(fecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lObs)
                    .addComponent(jObs, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel1.setBackground(new java.awt.Color(246, 71, 71));

        jLabel1.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Documento");
        jLabel1.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jLabel1MouseDragged(evt);
            }
        });
        jLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel1MousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public void limpiarSistema(){
        String query="UPDATE \"STOCK_"+promotor+"\""+" set stock=? where codigo=?";
        String query2="UPDATE GENERAL_STOCK set stock=? where codigo=?";
        try{
            for(int i=0;i<this.stock.size();i++){
                PreparedStatement ps = conectar.cn.prepareStatement(query);
                ps.setInt(1, conectar.StockPromotor(this.codigo.get(i), promotor)-this.stock.get(i));
                ps.setString(2, this.codigo.get(i));
                int ejecuta1= ps.executeUpdate();
                PreparedStatement ps2 = conectar.cn.prepareStatement(query2);
                ps2.setInt(1, conectar.StockInt(this.codigo.get(i))+this.stock.get(i));
                ps2.setString(2, this.codigo.get(i));
                int ejecuta2= ps2.executeUpdate();
                
            }
            
        }catch(SQLException e){
            System.err.println("El error es "+ e);
        }
    }
    private void bCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bCancelarActionPerformed
        dispose();
    }//GEN-LAST:event_bCancelarActionPerformed

    private void tNumDocActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tNumDocActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tNumDocActionPerformed

    private void jLabel1MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseDragged
        int x = evt.getXOnScreen();
        int y = evt.getYOnScreen();
        this.setLocation(x-xx,y-xy);
    }//GEN-LAST:event_jLabel1MouseDragged

    private void jLabel1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MousePressed
        xx=evt.getX();
        xy=evt.getY();
    }//GEN-LAST:event_jLabel1MousePressed

    private void bReporteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bReporteActionPerformed
        //REPORTE EN PDF
        conectar.conexion();
        SimpleDateFormat format1 = new SimpleDateFormat("dd-MM-yyyy");
        String date1 = format1.format(fecha.getDate());
        JasperReport jr = null;
        JDialog reporte = new JDialog(new javax.swing.JFrame(),true);
        reporte.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getClassLoader().getResource("imagenes/coredise.png")));
        reporte.setSize(950,1050);
        reporte.setLocationRelativeTo(null);
        reporte.setTitle("Consulta de Documento");
        try {
            Map parametro = new HashMap();
            parametro.put("parameter1", tNumDoc.getText());
            parametro.put("parameter2", date1);
            JasperPrint jp = JasperFillManager.fillReport(this.getClass().getClassLoader().getResourceAsStream("reportes/consultaDeDocSelect.jasper"),parametro,conectar.getConexion());
            
            JRViewer jv = new JRViewer(jp);
            jv.setZoomRatio((float) 0.75);
            reporte.getContentPane().add(jv);
            
            reporte.setVisible(true);
        } catch (JRException ex) {
            Logger.getLogger(consultaDeDocSelect.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_bReporteActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(consultaDeDocSelect.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(consultaDeDocSelect.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(consultaDeDocSelect.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(consultaDeDocSelect.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                consultaDeDocSelect dialog = new consultaDeDocSelect(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bCancelar;
    private javax.swing.JButton bReporte;
    private com.toedter.calendar.JDateChooser fecha;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jObs;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lFecha;
    private javax.swing.JLabel lObs;
    private javax.swing.JLabel lnumDoc;
    private javax.swing.JScrollPane scrollArticulos;
    private javax.swing.JTextField tNumDoc;
    private javax.swing.JTextArea tObs;
    private javax.swing.JTable tableArticulos;
    // End of variables declaration//GEN-END:variables
}
