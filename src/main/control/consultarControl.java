/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.control;

import com.toedter.calendar.JTextFieldDateEditor;
import conexionBD.conexionBD;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import main.muestras.consultaDeDocSelect;
import main.muestras.modificacionStockSelectMuestra;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.swing.JRViewer;

/**
 *
 * @author Okamoto
 */
public class consultarControl extends javax.swing.JDialog {
    private LineBorder border = new LineBorder(Color.RED,2);
    int xx,xy;
    DefaultTableModel modelo = new DefaultTableModel(){
        public boolean isCellEditable(int rowIndex,int columnIndex){return false;}
    };
    conexionBD conectar = new conexionBD();
    Statement st = conectar.conexion();
    public String promotor,contacto,cargoContacto,distritoColegio,campana;
    ArrayList<Integer> stock = new ArrayList<Integer>();
    ArrayList<String> codigo= new ArrayList<String>();
    String colegioInicial;
    
    /**
     * Creates new form nuevoControl
     */
    public consultarControl(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null); 
        this.getRootPane().setBorder(border);
//        modificarFecha();
    }
    public void modificarFecha(){
        java.util.Date fechaActual = new java.util.Date();
        fechaEntrega.setDate(fechaActual);   
        JTextFieldDateEditor dateEditor = (JTextFieldDateEditor)fechaEntrega.getComponent(1);
        dateEditor.setHorizontalAlignment(JTextField.CENTER);
    }
    public void completarTabla(){
        modelo.addColumn("Codigo");
        modelo.addColumn("Descripcion");
        modelo.addColumn("Cantidad E.");
        modelo.addColumn("Cantidad D.");
        modelo.addColumn("Conteo");
        
        
        TableColumnModel columnModel = tablaArticulos.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(110);
        columnModel.getColumn(1).setPreferredWidth(264);
        columnModel.getColumn(2).setPreferredWidth(80);
        columnModel.getColumn(3).setPreferredWidth(80);
        columnModel.getColumn(3).setPreferredWidth(80);
        
       
        
        tablaArticulos.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        modelo.setRowCount(0);
        
        try{
            ResultSet rs = st.executeQuery("SELECT codigo,descrip,stockEntregado,stockDevuelto,conteo from MOV_DOCUMENTOS_COLEGIOS WHERE numCargoEntrega=\'"+textCargo.getText()+"\' and campana=\'"+campana+"\'");
            while(rs.next()){
                Object [] fila = new Object[5];
                for (int i=0;i<5;i++){
                    fila[i] = rs.getObject(i+1);
                }     
                stock.add(rs.getInt("stockEntregado"));
                codigo.add(rs.getString("codigo"));
                modelo.addRow(fila);
            }
        }catch(SQLException e){
            System.err.println("El error: "+ e);
        };
        try{
            ResultSet rs = st.executeQuery("SELECT observacion,distrito,contactoColegio,estado,numCargoDevol,fechaDevol,numCargoDevolAD1,fechaDevolAD1,numCargoDevolAD2,fechaDevolAD2 from DOCUMENTOS_COLEGIOS WHERE numCargoEntrega=\'"+textCargo.getText()+"\' and campana=\'"+campana+"\'");
            while(rs.next()){
                Object [] fila = new Object[10];
                for (int i=0;i<10;i++){
                    fila[i] = rs.getObject(i+1);
                }     
                textObservacion.setText((String) fila[0]);
                textDistrito.setText((String) fila[1]);
                textContacto.setText((String) fila[2]);
                String estado = ((String) fila[3]);
                if(estado.equals("REVISION")){
                    lEstado.setForeground(Color.YELLOW);
                    lEstado.setText(estado);
                }
                if(estado.equals("PEDIDO")){
                    lEstado.setForeground(Color.GREEN);
                    lEstado.setText(estado);
                }
                if(estado.equals("DEVUELTO")){
                    lEstado.setForeground(Color.RED);
                    lEstado.setText(estado);
                }
                if(estado.equals("TITULO GRATUITO")){
                    lEstado.setForeground(Color.ORANGE);
                    lEstado.setText(estado);
                }
                if(estado.equals("POR DEVOLVER")){
                    lEstado.setForeground(Color.RED);
                    lEstado.setText(estado);
                }
               textDevol1.setText((String) fila[4]);
               fechaDevol1.setDate((Date) fila[5]);
               textDevol2.setText((String) fila[6]);
               fechaDevol2.setDate((Date) fila[7]);
               textDevol3.setText((String) fila[8]);
               fechaDevol3.setDate((Date) fila[9]);
                
            }
        }catch(SQLException e){
            System.err.println("El error: "+ e);
        };
        

        
    }
    public void recibeDatos(String cargoEntrega,Date fecha,String colegio){
        textCargo.setText(cargoEntrega);
        fechaEntrega.setDate(fecha);
        textColegio.setText(colegio);
        colegioInicial=colegio;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSeparator1 = new javax.swing.JSeparator();
        jRadioButton1 = new javax.swing.JRadioButton();
        jPanel1 = new javax.swing.JPanel();
        lControl = new javax.swing.JLabel();
        panelRegistro = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        textCargo = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        fechaEntrega = new com.toedter.calendar.JDateChooser();
        textColegio = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        textObservacion = new javax.swing.JTextArea();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        textDistrito = new javax.swing.JTextField();
        textContacto = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        textDevol1 = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        fechaDevol1 = new com.toedter.calendar.JDateChooser();
        jLabel9 = new javax.swing.JLabel();
        textDevol2 = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        fechaDevol2 = new com.toedter.calendar.JDateChooser();
        jLabel11 = new javax.swing.JLabel();
        textDevol3 = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        fechaDevol3 = new com.toedter.calendar.JDateChooser();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel13 = new javax.swing.JLabel();
        lEstado = new javax.swing.JLabel();
        panelBotones = new javax.swing.JPanel();
        bSalir = new javax.swing.JButton();
        bReporte = new javax.swing.JButton();
        panelArticulos = new javax.swing.JPanel();
        jScrolCliente = new javax.swing.JScrollPane();
        tablaArticulos = new javax.swing.JTable(modelo);

        jRadioButton1.setText("jRadioButton1");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(246, 71, 71));

        lControl.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        lControl.setForeground(new java.awt.Color(255, 255, 255));
        lControl.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lControl.setText("Consulta de muestra");
        lControl.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                lControlMouseDragged(evt);
            }
        });
        lControl.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                lControlMousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lControl, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 688, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lControl, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        jLabel1.setText("Cargo de entrega:");

        jLabel2.setText("Colegio:");

        jLabel3.setText("Observación:");

        textCargo.setEnabled(false);
        textCargo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textCargoKeyTyped(evt);
            }
        });

        jLabel4.setText("Fecha de entrega:");

        fechaEntrega.setEnabled(false);
        fechaEntrega.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N

        textColegio.setEditable(false);
        textColegio.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                textColegioMouseClicked(evt);
            }
        });
        textColegio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                textColegioKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textColegioKeyTyped(evt);
            }
        });

        textObservacion.setEditable(false);
        textObservacion.setColumns(20);
        textObservacion.setRows(5);
        jScrollPane1.setViewportView(textObservacion);

        jLabel5.setText("Distrito:");

        jLabel6.setText("Contacto:");

        textDistrito.setEditable(false);
        textDistrito.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                textDistritoMouseClicked(evt);
            }
        });
        textDistrito.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                textDistritoKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textDistritoKeyTyped(evt);
            }
        });

        textContacto.setEditable(false);
        textContacto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                textContactoMouseClicked(evt);
            }
        });
        textContacto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                textContactoKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textContactoKeyTyped(evt);
            }
        });

        jLabel7.setText("Cargo de devol.:");

        textDevol1.setEnabled(false);
        textDevol1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textDevol1KeyTyped(evt);
            }
        });

        jLabel8.setText("Fecha de devol.:");

        fechaDevol1.setEnabled(false);
        fechaDevol1.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N

        jLabel9.setText("Cargo de devol.:");

        textDevol2.setEnabled(false);
        textDevol2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textDevol2KeyTyped(evt);
            }
        });

        jLabel10.setText("Fecha de devol.:");

        fechaDevol2.setEnabled(false);
        fechaDevol2.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N

        jLabel11.setText("Cargo de devol.:");

        textDevol3.setEnabled(false);
        textDevol3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textDevol3KeyTyped(evt);
            }
        });

        jLabel12.setText("Fecha de devol.:");

        fechaDevol3.setEnabled(false);
        fechaDevol3.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N

        jLabel13.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel13.setText("Estado:");

        lEstado.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        lEstado.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lEstado.setText(" ");

        javax.swing.GroupLayout panelRegistroLayout = new javax.swing.GroupLayout(panelRegistro);
        panelRegistro.setLayout(panelRegistroLayout);
        panelRegistroLayout.setHorizontalGroup(
            panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator2)
            .addGroup(panelRegistroLayout.createSequentialGroup()
                .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelRegistroLayout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7)
                            .addComponent(jLabel9)
                            .addComponent(jLabel11))
                        .addGap(18, 18, 18)
                        .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(panelRegistroLayout.createSequentialGroup()
                                .addComponent(textDevol3, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel12)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(fechaDevol3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(panelRegistroLayout.createSequentialGroup()
                                .addComponent(textDevol2, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel10)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(fechaDevol2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(jScrollPane1)
                            .addComponent(textContacto)
                            .addGroup(panelRegistroLayout.createSequentialGroup()
                                .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(textCargo)
                                    .addComponent(textColegio, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel5))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(fechaEntrega, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(textDistrito, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(panelRegistroLayout.createSequentialGroup()
                                .addComponent(textDevol1, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(fechaDevol1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGroup(panelRegistroLayout.createSequentialGroup()
                        .addGap(33, 33, 33)
                        .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 281, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(lEstado, javax.swing.GroupLayout.PREFERRED_SIZE, 313, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(40, Short.MAX_VALUE))
        );
        panelRegistroLayout.setVerticalGroup(
            panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelRegistroLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(lEstado))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panelRegistroLayout.createSequentialGroup()
                        .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel1)
                                .addComponent(textCargo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel4))
                            .addComponent(fechaEntrega, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel2)
                                .addComponent(textColegio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel5)
                                .addComponent(textDistrito, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(textContacto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel8)
                                .addComponent(textDevol1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(fechaDevol1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7))
                        .addGap(13, 13, 13)
                        .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9)
                            .addComponent(textDevol2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel10)))
                    .addComponent(fechaDevol2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel11)
                        .addComponent(textDevol3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel12))
                    .addComponent(fechaDevol3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        bSalir.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        bSalir.setText("Salir");
        bSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bSalirActionPerformed(evt);
            }
        });

        bReporte.setText("Reporte");
        bReporte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bReporteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelBotonesLayout = new javax.swing.GroupLayout(panelBotones);
        panelBotones.setLayout(panelBotonesLayout);
        panelBotonesLayout.setHorizontalGroup(
            panelBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelBotonesLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(bReporte, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(bSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30))
        );
        panelBotonesLayout.setVerticalGroup(
            panelBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBotonesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bReporte, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(15, Short.MAX_VALUE))
        );

        tablaArticulos.getTableHeader().setResizingAllowed(false);
        tablaArticulos.getTableHeader().setReorderingAllowed(false);
        jScrolCliente.setViewportView(tablaArticulos);

        javax.swing.GroupLayout panelArticulosLayout = new javax.swing.GroupLayout(panelArticulos);
        panelArticulos.setLayout(panelArticulosLayout);
        panelArticulosLayout.setHorizontalGroup(
            panelArticulosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelArticulosLayout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(jScrolCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 615, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(41, Short.MAX_VALUE))
        );
        panelArticulosLayout.setVerticalGroup(
            panelArticulosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelArticulosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrolCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 286, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelRegistro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(panelArticulos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(panelBotones, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(panelRegistro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelArticulos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelBotones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 742, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void lControlMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lControlMouseDragged
        int x = evt.getXOnScreen();
        int y = evt.getYOnScreen();
        this.setLocation(x-xx,y-xy);
    }//GEN-LAST:event_lControlMouseDragged

    private void lControlMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lControlMousePressed
        xx=evt.getX();
        xy=evt.getY();
    }//GEN-LAST:event_lControlMousePressed

    private void bSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bSalirActionPerformed
        dispose();
    }//GEN-LAST:event_bSalirActionPerformed

    private void textColegioMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_textColegioMouseClicked
        if(evt.getClickCount()==2){
            selectorColegio();
        }
    }//GEN-LAST:event_textColegioMouseClicked

    private void textColegioKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textColegioKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_F1){
            selectorColegio();
        }
    }//GEN-LAST:event_textColegioKeyPressed

    private void textCargoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textCargoKeyTyped
        char c=evt.getKeyChar();
        if(!isNumeric(String.valueOf(c))){
            getToolkit().beep();
                evt.consume();
        }
        
        if(textCargo.getText().length()==10){
            evt.consume();
        }
        
    }//GEN-LAST:event_textCargoKeyTyped
    public void selectorStock(){
       agregarArticulo aa= new agregarArticulo(new javax.swing.JFrame(),true);
       aa.promotor=promotor;
        aa.setVisible(true);
        if(!aa.isVisible() && filtro(aa.textCodigo.getText())){
            if(aa.bandera==0){
                Object [] fila = new Object[3];
                fila[0]= aa.textCodigo.getText();
                fila[1]= aa.textDescrip.getText();
                fila[2]= aa.textCantidad.getText();
                modelo.addRow(fila); 
            }      
        } 
        else{
            int mensaje = JOptionPane.showOptionDialog(null, "No se permite ingresar articulo nuevamente en tabla", "ERROR", JOptionPane.DEFAULT_OPTION,
                     JOptionPane.ERROR_MESSAGE, null, null, null);   
        }
    }
    private boolean filtro(String texto){
        for (int i = 0; i < tablaArticulos.getRowCount(); i++){
             if(tablaArticulos.getValueAt(i, 0).toString().equals(texto)){
                System.out.println("Ya existe articulo en tabla");
                return false;
             }
        }
        return true;
    } 
    
    private void textColegioKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textColegioKeyTyped
        evt.consume();
    }//GEN-LAST:event_textColegioKeyTyped

    private void textDistritoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_textDistritoMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_textDistritoMouseClicked

    private void textDistritoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textDistritoKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_textDistritoKeyPressed

    private void textDistritoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textDistritoKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_textDistritoKeyTyped

    private void textContactoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_textContactoMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_textContactoMouseClicked

    private void textContactoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textContactoKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_textContactoKeyPressed

    private void textContactoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textContactoKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_textContactoKeyTyped

    private void textDevol1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textDevol1KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_textDevol1KeyTyped

    private void textDevol2KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textDevol2KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_textDevol2KeyTyped

    private void textDevol3KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textDevol3KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_textDevol3KeyTyped

    private void bReporteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bReporteActionPerformed
        conectar.conexion();
        SimpleDateFormat format1 = new SimpleDateFormat("dd-MM-yyyy");
        String date1 = format1.format(fechaEntrega.getDate());
        String date2="";
        if(!(fechaDevol1.getDate()==null)){
            date2 = format1.format(fechaDevol1.getDate());
        }
        String date3 ="";
        if(!(fechaDevol2.getDate()==null)){
            date3 = format1.format(fechaDevol2.getDate());
        }
        String date4="";
        if(!(fechaDevol3.getDate()==null)){
            date4 = format1.format(fechaDevol3.getDate());
        }

        JasperReport jr = null;
        JDialog reporte = new JDialog(new javax.swing.JFrame(),true);
        reporte.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getClassLoader().getResource("imagenes/coredise.png")));
        reporte.setSize(950,1050);
        reporte.setLocationRelativeTo(null);
        reporte.setTitle("Consulta de Documento");
        try {
            Map parametro = new HashMap();
            parametro.put("parameter1", textCargo.getText());
            parametro.put("parameter2", date1);
            parametro.put("parameter3", textDevol1.getText());
            parametro.put("parameter4", date2);
            parametro.put("parameter5", textDevol2.getText());
            parametro.put("parameter6", date3);
            parametro.put("parameter7", textDevol3.getText());
            parametro.put("parameter8", date4);
            parametro.put("parameter9", textColegio.getText());
            parametro.put("parameter10", textDistrito.getText());
            parametro.put("parameter11", textContacto.getText());
            parametro.put("parameter12", textObservacion.getText());
            parametro.put("parameter13", lEstado.getText());
            
            JasperPrint jp = JasperFillManager.fillReport(this.getClass().getClassLoader().getResourceAsStream("reportes/consultaControl.jasper"),parametro,conectar.getConexion());
            
            JRViewer jv = new JRViewer(jp);
            jv.setZoomRatio((float) 0.75);
            reporte.getContentPane().add(jv);
            
            reporte.setVisible(true);
        } catch (JRException ex) {
            Logger.getLogger(consultaDeDocSelect.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_bReporteActionPerformed
    //Metodo para validar si lo ingresado es numerico   
    public static boolean isNumeric(String str){
        NumberFormat formatter = NumberFormat.getInstance();
        ParsePosition pos = new ParsePosition(0);
        formatter.parse(str, pos);
        return str.length() == pos.getIndex();
    }
    public void selectorColegio(){
        selectorColegio sc= new selectorColegio(new javax.swing.JFrame(),true);
        sc.promotor=promotor;
        sc.setVisible(true);
        if(!sc.isVisible()&& sc.bandera==0){
            textColegio.setText(sc.colegio);
            contacto = sc.contacto;
            cargoContacto=sc.cargoContacto;
            distritoColegio=sc.distrito;
            sc.dispose();
        }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(consultarControl.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(consultarControl.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(consultarControl.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(consultarControl.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                consultarControl dialog = new consultarControl(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bReporte;
    private javax.swing.JButton bSalir;
    private com.toedter.calendar.JDateChooser fechaDevol1;
    private com.toedter.calendar.JDateChooser fechaDevol2;
    private com.toedter.calendar.JDateChooser fechaDevol3;
    private com.toedter.calendar.JDateChooser fechaEntrega;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JScrollPane jScrolCliente;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JLabel lControl;
    private javax.swing.JLabel lEstado;
    private javax.swing.JPanel panelArticulos;
    private javax.swing.JPanel panelBotones;
    private javax.swing.JPanel panelRegistro;
    private javax.swing.JTable tablaArticulos;
    private javax.swing.JTextField textCargo;
    private javax.swing.JTextField textColegio;
    private javax.swing.JTextField textContacto;
    private javax.swing.JTextField textDevol1;
    private javax.swing.JTextField textDevol2;
    private javax.swing.JTextField textDevol3;
    private javax.swing.JTextField textDistrito;
    private javax.swing.JTextArea textObservacion;
    // End of variables declaration//GEN-END:variables
}
