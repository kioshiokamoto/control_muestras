/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.control;

import conexionBD.conexionBD;
import java.awt.Color;
import java.awt.Shape;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.geom.RoundRectangle2D;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author Okamoto
 */
public class controlPrincipal extends javax.swing.JDialog {
    private LineBorder border = new LineBorder(Color.RED,2);
    int xx,xy;
    DefaultTableModel modelo = new DefaultTableModel(){
        public boolean isCellEditable(int rowIndex,int columnIndex){return false;}
    };
    conexionBD conectar = new conexionBD();
    Statement st = conectar.conexion();
    public String promotor;
    public String campana;

    /**
     * Creates new form controlPrincipal
     */
    public controlPrincipal(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null); 
        this.getRootPane().setBorder(border);
//        completarTabla();
    }
    
    public void completarTabla(){
        modelo.addColumn("Estado");
        modelo.addColumn("N°Cargo - E");
        modelo.addColumn("Fecha Entrega");
        modelo.addColumn("Colegio/Institución");
        modelo.addColumn("Distrito");
        modelo.addColumn("Contacto");
        modelo.addColumn("Cargo(s)");

        modelo.addColumn("N°Cargo - D");
        modelo.addColumn("Fecha de devol.");
        
        
        TableColumnModel columnModel = tablaControl.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(92);
        columnModel.getColumn(1).setPreferredWidth(80);
        columnModel.getColumn(2).setPreferredWidth(100);
        columnModel.getColumn(3).setPreferredWidth(247);
        columnModel.getColumn(4).setPreferredWidth(130);
        columnModel.getColumn(5).setPreferredWidth(180);
        columnModel.getColumn(6).setPreferredWidth(110);

        columnModel.getColumn(7).setPreferredWidth(80);
        columnModel.getColumn(8).setPreferredWidth(100);
       
        
        tablaControl.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        actualizarTabla();
        contadorColegios();
        
    }
    public void actualizarTabla(){
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
        
        String cargoD1=null;
        String cargoD2=null;
        modelo.setRowCount(0);
        try{
            ResultSet rs = st.executeQuery("SELECT estado,numCargoEntrega,fechaEntrega,colegio,distrito,contactoColegio,cargoContacto,numCargoDevol,fechaDevol FROM DOCUMENTOS_COLEGIOS where rucProm=\'"+promotor+"\' and campana=\'"+campana+"\' ORDER BY fechaEntrega DESC");
            while(rs.next()){
                int filaModificar = rs.getRow();
                Object [] fila = new Object[9];
                for (int i=0;i<9;i++){
                    fila[i] = rs.getObject(i+1);
                }     
                modelo.addRow(fila);
                tablaControl.setValueAt(df.format(fila[2]), filaModificar-1, 2);
            }
        }catch(SQLException e){
            System.err.println("El error: "+ e);
        };
        try{
            ResultSet rs1 = st.executeQuery("SELECT numCargoDevolAD1,numCargoDevolAD2 from DOCUMENTOS_COLEGIOS WHERE rucProm=\'"+promotor+"\' and campana=\'"+campana+"\' ORDER BY fechaEntrega DESC");
            while(rs1.next()){
                Object [] fila = new Object[2];
                for (int i=0;i<2;i++){
                    fila[i] = rs1.getObject(i+1);
                }  
                cargoD1= (String)fila[0];
                cargoD2=(String) fila[1];
//                System.out.println("CARGO D1\t" + cargoD1);
                if(!(cargoD1==null)){
                    int filaModificar = rs1.getRow();
                    tablaControl.setValueAt("          +", filaModificar-1, 7);
                    tablaControl.setValueAt("              +", filaModificar-1, 8); 
                }
                
            }
        }catch(SQLException e){
            
        }
        contadorColegios();
    }
    public void contadorColegios(){
        int cantidadColegios=0;
        try{
            ResultSet rs = st.executeQuery("SELECT count(distinct colegio) FROM DOCUMENTOS_COLEGIOS where rucProm=\'"+promotor+"\' and campana=\'"+campana+"\'");
            while(rs.next()){
                cantidadColegios = rs.getInt(1);
            }
            
        }catch(SQLException e){
            System.err.println("El error: "+ e);
        };
        lColegios.setText(String.valueOf(cantidadColegios));   
    }
       

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lControl = new javax.swing.JLabel();
        panelFiltro = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        textBuscador = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        comboFiltro = new javax.swing.JComboBox<>();
        panelTabla = new javax.swing.JPanel();
        jScrolCliente = new javax.swing.JScrollPane();
        tablaControl = new javax.swing.JTable(modelo);
        panelInformacion = new javax.swing.JPanel();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel3 = new javax.swing.JLabel();
        lColegios = new javax.swing.JLabel();
        panelBotones = new javax.swing.JPanel();
        bSalir = new javax.swing.JButton();
        paneBotonEntrega = new javax.swing.JPanel();
        bNuevo = new javax.swing.JButton();
        bEditar = new javax.swing.JButton();
        bEliminar = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        bDevolTotal = new javax.swing.JButton();
        bDevolucionParcial = new javax.swing.JButton();
        bEliminarDevol = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        bConteo = new javax.swing.JButton();
        bEditarEstado = new javax.swing.JButton();
        bEliminarConteo = new javax.swing.JButton();
        bConsultar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(246, 71, 71));

        lControl.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        lControl.setForeground(new java.awt.Color(255, 255, 255));
        lControl.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lControl.setText("Control de colegios");
        lControl.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                lControlMouseDragged(evt);
            }
        });
        lControl.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                lControlMousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lControl, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lControl, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        jLabel1.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel1.setText("Buscar:");

        textBuscador.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        textBuscador.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textBuscadorKeyTyped(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel2.setText("Por:");

        comboFiltro.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        comboFiltro.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Estado","Colegio","Distrito", "Cargo entrega","Contacto", "Cargo devolucion" }));

        javax.swing.GroupLayout panelFiltroLayout = new javax.swing.GroupLayout(panelFiltro);
        panelFiltro.setLayout(panelFiltroLayout);
        panelFiltroLayout.setHorizontalGroup(
            panelFiltroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelFiltroLayout.createSequentialGroup()
                .addGap(62, 62, 62)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(textBuscador, javax.swing.GroupLayout.PREFERRED_SIZE, 575, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(79, 79, 79)
                .addComponent(jLabel2)
                .addGap(26, 26, 26)
                .addComponent(comboFiltro, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelFiltroLayout.setVerticalGroup(
            panelFiltroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelFiltroLayout.createSequentialGroup()
                .addContainerGap(18, Short.MAX_VALUE)
                .addGroup(panelFiltroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(textBuscador, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(comboFiltro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(14, 14, 14))
        );

        tablaControl.getTableHeader().setResizingAllowed(false);
        tablaControl.getTableHeader().setReorderingAllowed(false);
        jScrolCliente.setViewportView(tablaControl);

        javax.swing.GroupLayout panelTablaLayout = new javax.swing.GroupLayout(panelTabla);
        panelTabla.setLayout(panelTablaLayout);
        panelTablaLayout.setHorizontalGroup(
            panelTablaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelTablaLayout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(jScrolCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 1125, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(44, Short.MAX_VALUE))
        );
        panelTablaLayout.setVerticalGroup(
            panelTablaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelTablaLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrolCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jLabel3.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel3.setText("Total de colegios:");

        lColegios.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        lColegios.setText("     ");

        javax.swing.GroupLayout panelInformacionLayout = new javax.swing.GroupLayout(panelInformacion);
        panelInformacion.setLayout(panelInformacionLayout);
        panelInformacionLayout.setHorizontalGroup(
            panelInformacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addComponent(jSeparator2)
            .addGroup(panelInformacionLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lColegios)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelInformacionLayout.setVerticalGroup(
            panelInformacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelInformacionLayout.createSequentialGroup()
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                .addGroup(panelInformacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(lColegios))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        bSalir.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        bSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/exit.png"))); // NOI18N
        bSalir.setText("Salir");
        bSalir.setSelected(true);
        bSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bSalirActionPerformed(evt);
            }
        });

        paneBotonEntrega.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION), "Entrega", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 12))); // NOI18N

        bNuevo.setText("Nuevo");
        bNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bNuevoActionPerformed(evt);
            }
        });

        bEditar.setText("Editar");
        bEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bEditarActionPerformed(evt);
            }
        });

        bEliminar.setText("Eliminar");
        bEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bEliminarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout paneBotonEntregaLayout = new javax.swing.GroupLayout(paneBotonEntrega);
        paneBotonEntrega.setLayout(paneBotonEntregaLayout);
        paneBotonEntregaLayout.setHorizontalGroup(
            paneBotonEntregaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(paneBotonEntregaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(bNuevo, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(bEditar, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(bEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        paneBotonEntregaLayout.setVerticalGroup(
            paneBotonEntregaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, paneBotonEntregaLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(paneBotonEntregaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bNuevo, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bEditar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0))
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 12)), "Devolución", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 12))); // NOI18N

        bDevolTotal.setText("Devol. Total");
        bDevolTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bDevolTotalActionPerformed(evt);
            }
        });

        bDevolucionParcial.setText("Devol. Parcial");
        bDevolucionParcial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bDevolucionParcialActionPerformed(evt);
            }
        });

        bEliminarDevol.setText("Eliminar");
        bEliminarDevol.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bEliminarDevolActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(bDevolTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(bDevolucionParcial, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(76, 76, 76)
                        .addComponent(bEliminarDevol, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bDevolTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bDevolucionParcial, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bEliminarDevol, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 12)), "Pedido", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 12))); // NOI18N

        bConteo.setText("Conteo");
        bConteo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bConteoActionPerformed(evt);
            }
        });

        bEditarEstado.setText("Editar Estado");
        bEditarEstado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bEditarEstadoActionPerformed(evt);
            }
        });

        bEliminarConteo.setText("Eliminar Conteo");
        bEliminarConteo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bEliminarConteoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(bConteo, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bEliminarConteo, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(bEditarEstado, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(83, 83, 83))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bConteo, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bEliminarConteo, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(bEditarEstado, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        bConsultar.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        bConsultar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Check.png"))); // NOI18N
        bConsultar.setText("Consultar");
        bConsultar.setSelected(true);
        bConsultar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bConsultarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelBotonesLayout = new javax.swing.GroupLayout(panelBotones);
        panelBotones.setLayout(panelBotonesLayout);
        panelBotonesLayout.setHorizontalGroup(
            panelBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBotonesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(paneBotonEntrega, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(panelBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bConsultar, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelBotonesLayout.setVerticalGroup(
            panelBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBotonesLayout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addGroup(panelBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(paneBotonEntrega, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(panelBotonesLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(bConsultar)
                        .addGap(18, 18, 18)
                        .addComponent(bSalir)))
                .addContainerGap(39, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(panelFiltro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(panelTabla, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(panelInformacion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panelBotones, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelFiltro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelTabla, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelInformacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(panelBotones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(17, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void lControlMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lControlMouseDragged
        int x = evt.getXOnScreen();
        int y = evt.getYOnScreen();
        this.setLocation(x-xx,y-xy);
    }//GEN-LAST:event_lControlMouseDragged

    private void lControlMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lControlMousePressed
        xx=evt.getX();
        xy=evt.getY();
    }//GEN-LAST:event_lControlMousePressed

    private void bSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bSalirActionPerformed
        dispose();
    }//GEN-LAST:event_bSalirActionPerformed

    private void bConsultarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bConsultarActionPerformed
        int filaseleccionada=tablaControl.getSelectedRow();
        SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
        if(filaseleccionada==-1){
            JOptionPane.showMessageDialog(null, "No se ha seleccionado ningun documento");
        }else{
            String cargoEntrega= (String) String.valueOf(tablaControl.getValueAt(filaseleccionada, 1)); 
            
            String fechaString =  (String) tablaControl.getValueAt(filaseleccionada, 2);
            Date fecha =null;
            try{
                fecha = formato.parse(fechaString);
            }catch(ParseException ex){
                ex.printStackTrace();
            }
            String colegio = (String) String.valueOf(tablaControl.getValueAt(filaseleccionada, 3)); 
            consultarControl cc = new consultarControl(new javax.swing.JFrame(),true);
            cc.campana=campana;

           cc.recibeDatos(cargoEntrega, fecha, colegio);
           cc.promotor=promotor;
           cc.completarTabla();
           cc.setVisible(true);
        }
        
       
    }//GEN-LAST:event_bConsultarActionPerformed

    private void bNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bNuevoActionPerformed
        nuevoControl nc = new nuevoControl(new javax.swing.JFrame(),true);
        nc.promotor=promotor;
        nc.campana=campana;
        nc.completarTabla();
        nc.setVisible(true);
        if(!nc.isVisible()){
            actualizarTabla();
        }
    }//GEN-LAST:event_bNuevoActionPerformed
    TableRowSorter trs=null;
    private void textBuscadorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textBuscadorKeyTyped
        if(comboFiltro.getSelectedIndex()==0){
            textBuscador.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent ke){
                trs.setRowFilter(RowFilter.regexFilter("(?i)"+textBuscador.getText(), 0));
            }
            });
            trs =new TableRowSorter(modelo);
            tablaControl.setRowSorter(trs);
        }
        if(comboFiltro.getSelectedIndex()==1){
            textBuscador.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent ke){
                trs.setRowFilter(RowFilter.regexFilter("(?i)"+textBuscador.getText(), 3));
            }
            });
            trs =new TableRowSorter(modelo);
            tablaControl.setRowSorter(trs);
        }
        if(comboFiltro.getSelectedIndex()==2){
            textBuscador.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent ke){
                trs.setRowFilter(RowFilter.regexFilter("(?i)"+textBuscador.getText(), 4));
            }
            });
            trs =new TableRowSorter(modelo);
            tablaControl.setRowSorter(trs);
        }
        if(comboFiltro.getSelectedIndex()==3){
            textBuscador.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent ke){
                trs.setRowFilter(RowFilter.regexFilter("(?i)"+textBuscador.getText(), 1));
            }
            });
            trs =new TableRowSorter(modelo);
            tablaControl.setRowSorter(trs);
        }
        if(comboFiltro.getSelectedIndex()==4){
            textBuscador.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent ke){
                trs.setRowFilter(RowFilter.regexFilter("(?i)"+textBuscador.getText(), 5));
            }
            });
            trs =new TableRowSorter(modelo);
            tablaControl.setRowSorter(trs);
        }
        if(comboFiltro.getSelectedIndex()==5){
            textBuscador.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent ke){
                trs.setRowFilter(RowFilter.regexFilter("(?i)"+textBuscador.getText(), 8));
            }
            });
            trs =new TableRowSorter(modelo);
            tablaControl.setRowSorter(trs);
        }
    }//GEN-LAST:event_textBuscadorKeyTyped

    private void bEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bEliminarActionPerformed
        int seleccionar = tablaControl.getSelectedRow();
        int bandera=0;
        ResultSet rs = null;
        
        String query1 = "DELETE FROM MOV_DOCUMENTOS_COLEGIOS WHERE numCargoEntrega=? and campana=\'"+campana+"\'";
        String query2 = "DELETE FROM DOCUMENTOS_COLEGIOS WHERE numCargoEntrega=? and campana=\'"+campana+"\'";
        String query3 = "UPDATE \"STOCK_"+promotor+"\""+" set stock=? where codigo=?";
        String codigo,stock;
        
        if(seleccionar==-1){
            JOptionPane.showMessageDialog(null, "No se ha seleccionado ningun documento");
        }else{
            String comparacion = ((String) String.valueOf(tablaControl.getValueAt(seleccionar, 0)));
            String comparacion1 = ((String) String.valueOf(tablaControl.getValueAt(seleccionar, 7)));
            if(comparacion.equals("REVISION") && comparacion1.equals("null")){
                int mensaje1 = JOptionPane.showConfirmDialog(null, "¿Desea realizar devolución todal de muestras?", "Confirmar devolución total", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                if(mensaje1==0){
                    String devolucion = String.valueOf(tablaControl.getValueAt(seleccionar, 7)); 
                    if(devolucion.equals("+")|| !devolucion.equals("null")){
                        int mensaje = JOptionPane.showOptionDialog(null, "Operacion invalida, cuenta con cargos de devolucion", "ERROR", JOptionPane.DEFAULT_OPTION,
                                         JOptionPane.ERROR_MESSAGE, null, null, null); 

                    }else{        
                        String cargoEntrega= (String) String.valueOf(tablaControl.getValueAt(seleccionar, 1)); 


                        try{
                            rs= st.executeQuery("select * from MOV_DOCUMENTOS_COLEGIOS where numCargoEntrega=\'"+cargoEntrega+"\' and campana=\'"+campana+"\'");
                            if(rs.next()){
                                codigo=rs.getString(5);
                                stock=rs.getString(7);
                                int stockAntes=0;
                                stockAntes = conectar.StockPromotor(codigo, promotor);
                                int cantidad= stockAntes + Integer.parseInt(stock);
                                PreparedStatement ps = conectar.cn.prepareStatement(query3);
                                ps.setInt(1,cantidad);
                                ps.setString(2, codigo);
                                int ejecucionPromotor= ps.executeUpdate();
                            }
                        }catch(SQLException ex){
                            System.err.println("Error en ingreso stock a promotor:\t" + ex);
                            bandera=1;
                        }
                        if(bandera==0){
                            try{
                                PreparedStatement ps= conectar.cn.prepareStatement(query1); 
                                ps.setString(1,cargoEntrega);
                                int ejecucionMovimiento= ps.executeUpdate();
                                ps= conectar.cn.prepareStatement(query2); 
                                ps.setString(1, cargoEntrega);
                                int ejecucionDocumento= ps.executeUpdate();
                                int mensaje = JOptionPane.showOptionDialog(null, "Se elimino correctamente documento", "Grabado", JOptionPane.DEFAULT_OPTION,
                                             JOptionPane.INFORMATION_MESSAGE, null, null, null);
                            }catch(SQLException e){
                                System.err.println("Error en:\t" + e);
                            }
                        }
                    }
                    actualizarTabla();

                }
            }else{
                int mensaje = JOptionPane.showOptionDialog(null, "Operacion invalida, cuenta con cargos de devolucion", "ERROR", JOptionPane.DEFAULT_OPTION,
                                         JOptionPane.ERROR_MESSAGE, null, null, null); 
            }
            
            
            
            
        }
    }//GEN-LAST:event_bEliminarActionPerformed

    private void bEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bEditarActionPerformed
        int filaseleccionada=tablaControl.getSelectedRow();
        SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
        if(filaseleccionada==-1){
            JOptionPane.showMessageDialog(null, "No se ha seleccionado ningun documento");
        }else{
            String devolucion = String.valueOf(tablaControl.getValueAt(filaseleccionada, 7)); 
            if(devolucion.equals("+")|| !devolucion.equals("null")){
                int mensaje = JOptionPane.showOptionDialog(null, "Operacion invalida, cuenta con cargos de devolucion", "ERROR", JOptionPane.DEFAULT_OPTION,
                                 JOptionPane.ERROR_MESSAGE, null, null, null); 
                
            }else{
                String cargoEntrega= (String) String.valueOf(tablaControl.getValueAt(filaseleccionada, 1)); 
                String fechaString =  (String) tablaControl.getValueAt(filaseleccionada, 2);
                Date fecha =null;
                try{
                    fecha = formato.parse(fechaString);
                }catch(ParseException ex){
                    ex.printStackTrace();
                }
                String colegio = (String) String.valueOf(tablaControl.getValueAt(filaseleccionada, 3)); 
                editarControl ec = new editarControl(new javax.swing.JFrame(), true);
                ec.campana=campana;
                ec.recibeDatos(cargoEntrega, fecha, colegio);
                ec.promotor=promotor;
                ec.completarTabla();
                ec.setVisible(true);
                if(!ec.isVisible()){
                    actualizarTabla();
                }
            }
            
        }
    }//GEN-LAST:event_bEditarActionPerformed

    private void bDevolTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bDevolTotalActionPerformed
        int filaseleccionada=tablaControl.getSelectedRow();
        if(filaseleccionada==-1){
            JOptionPane.showMessageDialog(null, "No se ha seleccionado ningun documento");
        }else{
            String comparacion = ((String) String.valueOf(tablaControl.getValueAt(filaseleccionada, 0)));
            if(comparacion.equals("REVISION") || comparacion.equals("POR DEVOLVER")){
                int mensaje = JOptionPane.showConfirmDialog(null, "¿Desea realizar devolución todal de muestras?", "Confirmar devolución total", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                if(mensaje==0){
                    devolucionTotal dt= new devolucionTotal(new javax.swing.JFrame(),true);
                    String cargoEntrega= (String) String.valueOf(tablaControl.getValueAt(filaseleccionada, 1)); 
                    dt.cargoEntrega=cargoEntrega;
                    dt.promotor=promotor;
                    if(comparacion.equals("POR DEVOLVER")){
                        dt.devolucion= "PEDIDO";
                    }
                    dt.setVisible(true);
                    if(!dt.isVisible()){
                        actualizarTabla();
                    }
                }
            }else{
                int mensaje = JOptionPane.showOptionDialog(null, "Operacion invalida, documento no esta en REVISION o POR DEVOLVER", "ERROR", JOptionPane.DEFAULT_OPTION,
                                 JOptionPane.ERROR_MESSAGE, null, null, null); 
            }
            
        }
        
        
    }//GEN-LAST:event_bDevolTotalActionPerformed

    private void bEliminarDevolActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bEliminarDevolActionPerformed
        int filaseleccionada=tablaControl.getSelectedRow();
        ArrayList<Integer> stockEntregado = new ArrayList<Integer>();
        ArrayList<Integer> stockDevuelto = new ArrayList<Integer>();
        ArrayList<String> codigo= new ArrayList<String>();
        String query1= "update MOV_DOCUMENTOS_COLEGIOS set stockEntregado=?,stockDevuelto=?,estado=? where codigo=? and numCargoEntrega=? and campana=\'"+campana+"\'";  
        String query2= "update DOCUMENTOS_COLEGIOS set estado=?,numCargoDevol=?,fechaDevol=?,numCargoDevolAD1=?,fechaDevolAD1=?,numCargoDevolAD2=?,fechaDevolAD2=? where numCargoEntrega=? and campana=\'"+campana+"\'";
        String query3="UPDATE \"STOCK_"+promotor+"\""+" set stock=? where codigo=?;";
        if(filaseleccionada==-1){
            JOptionPane.showMessageDialog(null, "No se ha seleccionado ningun documento");
        }else{
            String comparacion = ((String) String.valueOf(tablaControl.getValueAt(filaseleccionada, 0)));
            String cargoEntrega = ((String) String.valueOf(tablaControl.getValueAt(filaseleccionada, 1)));
            String cargoDevol = ((String) String.valueOf(tablaControl.getValueAt(filaseleccionada, 7)));
            if(comparacion.equals("REVISION") || comparacion.equals("DEVUELTO")){
                if(!cargoDevol.equals("null")){
                    int mensaje = JOptionPane.showConfirmDialog(null, "¿Desea eliminar los cargos de devolución?", "Confirmar devolución total", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                    if(mensaje==0){
                        try{
                            ResultSet rs = st.executeQuery("SELECT codigo,stockEntregado,stockDevuelto from MOV_DOCUMENTOS_COLEGIOS WHERE numCargoEntrega=\'"+cargoEntrega+"\' and campana=\'"+campana+"\'");
                            while(rs.next()){
                                Object [] fila = new Object[3];
                                for (int i=0;i<3;i++){
                                    fila[i] = rs.getObject(i+1);
                                } 
                                codigo.add(rs.getString("codigo"));
                                stockEntregado.add(rs.getInt("stockEntregado"));
                                stockDevuelto.add(rs.getInt("stockDevuelto"));
                            }
                            for(int i=0;i<codigo.size();i++){
                                try{
                                    int cantidad = stockEntregado.get(i) + stockDevuelto.get(i);
                                    PreparedStatement ps1 = conectar.cn.prepareStatement(query1); 
                                    ps1.setInt(1, cantidad);
                                    ps1.setInt(2, 0);
                                    ps1.setString(3, "REVISION");
                                    ps1.setString(4, codigo.get(i));
                                    ps1.setString(5, cargoEntrega);
                                    int ejecutar = ps1.executeUpdate();

                                    PreparedStatement ps3 = conectar.cn.prepareStatement(query3); 
                                    ps3.setInt(1, conectar.StockPromotor(codigo.get(i), promotor) - stockDevuelto.get(i));
                                    ps3.setString(2, codigo.get(i));
                                    int ejecutar2= ps3.executeUpdate();
                                }catch(SQLException ex){
                                     System.out.println("El error es:\t" + ex);
                                }
                            }
                            try{
                                PreparedStatement ps2 = conectar.cn.prepareStatement(query2);
                                ps2.setString(1,"REVISION");
                                ps2.setString(2,null);
                                ps2.setDate(3, null);
                                ps2.setString(4,null);
                                ps2.setDate(5, null);
                                ps2.setString(6,null);
                                ps2.setDate(7, null);
                                ps2.setString(8,cargoEntrega);
                                int ejecutarlimpieza = ps2.executeUpdate();
                                int mensaje1 = JOptionPane.showOptionDialog(null, "Se grabo correctamente", "Grabado", JOptionPane.DEFAULT_OPTION,
                                        JOptionPane.INFORMATION_MESSAGE, null, null, null);
                                actualizarTabla();

                            }catch(SQLException ex){
                                System.out.println("El error es:\t" + ex);
                            }


                        }catch(SQLException e){
                            System.out.println("El error es:\t" + e);
                        }
                    }
                }
                else{
                    int mensaje = JOptionPane.showOptionDialog(null, "Operacion invalida, documento no cuenta con cargo de devolucion", "ERROR", JOptionPane.DEFAULT_OPTION,
                                 JOptionPane.ERROR_MESSAGE, null, null, null); 
                }
                
            }else{
                int mensaje = JOptionPane.showOptionDialog(null, "Operacion invalida, documento no esta en REVISION o DEVUELTO", "ERROR", JOptionPane.DEFAULT_OPTION,
                                 JOptionPane.ERROR_MESSAGE, null, null, null); 
            }
        }
    }//GEN-LAST:event_bEliminarDevolActionPerformed

    private void bDevolucionParcialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bDevolucionParcialActionPerformed
        int filaseleccionada=tablaControl.getSelectedRow();
        SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
        if(filaseleccionada==-1){
            JOptionPane.showMessageDialog(null, "No se ha seleccionado ningun documento");
        }else{
            String comparacion = ((String) String.valueOf(tablaControl.getValueAt(filaseleccionada, 0)));
            if(comparacion.equals("REVISION") || comparacion.equals("POR DEVOLVER")){
                String cargoEntrega= (String) String.valueOf(tablaControl.getValueAt(filaseleccionada, 1)); 
                String fechaString =  (String) tablaControl.getValueAt(filaseleccionada, 2);
                Date fecha =null;
                try{
                    fecha = formato.parse(fechaString);
                }catch(ParseException ex){
                    ex.printStackTrace();
                }
                String colegio = (String) String.valueOf(tablaControl.getValueAt(filaseleccionada, 3)); 
                devolucionParcial dp = new devolucionParcial(new javax.swing.JFrame(),true);
                if(comparacion.equals("POR DEVOLVER")){
                    dp.estado="POR DEVOLVER";
                    
                }
                dp.campana=campana;
                dp.promotor=promotor;
                dp.recibeDatos(cargoEntrega, fecha, colegio);
                dp.cargoEntrega=cargoEntrega;
                dp.completarTabla();
                dp.setVisible(true);  
                if(!dp.isVisible()){
                    actualizarTabla();
                }
            }else{
                int mensaje = JOptionPane.showOptionDialog(null, "Operacion invalida, documento no esta en REVISION o POR DEVOLVER", "ERROR", JOptionPane.DEFAULT_OPTION,
                                 JOptionPane.ERROR_MESSAGE, null, null, null);
            }
            
        }
        
    }//GEN-LAST:event_bDevolucionParcialActionPerformed

    private void bEditarEstadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bEditarEstadoActionPerformed
        int filaseleccionada=tablaControl.getSelectedRow();
        if(filaseleccionada==-1){
            JOptionPane.showMessageDialog(null, "No se ha seleccionado ningun documento");
        }else{
            String comparacion = ((String) String.valueOf(tablaControl.getValueAt(filaseleccionada, 0)));
            if(comparacion.equals("REVISION") || comparacion.equals("PEDIDO")){
                String cargoEntrega= (String) String.valueOf(tablaControl.getValueAt(filaseleccionada, 1)); 
                cambiarEstado ce = new cambiarEstado(new javax.swing.JFrame(), true);
                ce.cargoEntrega=cargoEntrega;
                ce.setVisible(true);
                if(!ce.isVisible()){
                    actualizarTabla();
                }
            }else{
                int mensaje = JOptionPane.showOptionDialog(null, "Operacion invalida, documento no esta en REVISION o PEDIDO", "ERROR", JOptionPane.DEFAULT_OPTION,
                                 JOptionPane.ERROR_MESSAGE, null, null, null);
            }
            
        }
    }//GEN-LAST:event_bEditarEstadoActionPerformed

    private void bConteoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bConteoActionPerformed
        int filaseleccionada=tablaControl.getSelectedRow();
        SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
        if(filaseleccionada==-1){
            JOptionPane.showMessageDialog(null, "No se ha seleccionado ningun documento");
        }else{
            String comparacion = ((String) String.valueOf(tablaControl.getValueAt(filaseleccionada, 0)));
            if(comparacion.equals("PEDIDO")){
                String cargoEntrega= (String) String.valueOf(tablaControl.getValueAt(filaseleccionada, 1)); 
                String fechaString =  (String) tablaControl.getValueAt(filaseleccionada, 2);
                Date fecha =null;
                try{
                    fecha = formato.parse(fechaString);
                }catch(ParseException ex){
                    ex.printStackTrace();
                }
                String colegio = (String) String.valueOf(tablaControl.getValueAt(filaseleccionada, 3)); 
                conteoControl cc = new conteoControl(new javax.swing.JFrame(),true);
                
                cc.campana=campana;
                cc.promotor=promotor;
                cc.recibeDatos(cargoEntrega, fecha, colegio);
                cc.cargoEntrega=cargoEntrega;
                cc.completarTabla();
                cc.setVisible(true);
                if(!cc.isVisible()){
                    actualizarTabla();
                }
                
            }else{
                int mensaje = JOptionPane.showOptionDialog(null, "Operacion invalida, documento no esta en PEDIDO", "ERROR", JOptionPane.DEFAULT_OPTION,
                                 JOptionPane.ERROR_MESSAGE, null, null, null);
            }
        }
    }//GEN-LAST:event_bConteoActionPerformed

    private void bEliminarConteoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bEliminarConteoActionPerformed
        int filaseleccionada=tablaControl.getSelectedRow();
        ArrayList<Integer> stockEntregado = new ArrayList<Integer>();
        ArrayList<Integer> stockDevuelto = new ArrayList<Integer>();
        ArrayList<String> codigo= new ArrayList<String>();
        String query1= "update MOV_DOCUMENTOS_COLEGIOS set stockEntregado=?,stockDevuelto=?,estado=?,conteo=? where codigo=? and numCargoEntrega=? and campana=\'"+campana+"\'";  
        String query2= "update DOCUMENTOS_COLEGIOS set estado=?,numCargoDevol=?,fechaDevol=?,numCargoDevolAD1=?,fechaDevolAD1=?,numCargoDevolAD2=?,fechaDevolAD2=? where numCargoEntrega=? and campana=\'"+campana+"\'";
        String query3="UPDATE \"STOCK_"+promotor+"\""+" set stock=? where codigo=?;";
        if(filaseleccionada==-1){
            JOptionPane.showMessageDialog(null, "No se ha seleccionado ningun documento");
        }else{
            String comparacion = ((String) String.valueOf(tablaControl.getValueAt(filaseleccionada, 0)));
            String cargoEntrega = ((String) String.valueOf(tablaControl.getValueAt(filaseleccionada, 1)));
            String cargoDevol = ((String) String.valueOf(tablaControl.getValueAt(filaseleccionada, 7)));
            if(comparacion.equals("PEDIDO") || comparacion.equals("TITULO GRATUITO") || comparacion.equals("POR DEVOLVER")){
                    int mensaje = JOptionPane.showConfirmDialog(null, "¿Desea eliminar conteo?\n*También se eliminaran cargos de devolucion.", "Confirmar eliminacion de conteo", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                    if(mensaje==0){
                        try{
                            ResultSet rs = st.executeQuery("SELECT codigo,stockEntregado,stockDevuelto from MOV_DOCUMENTOS_COLEGIOS WHERE numCargoEntrega=\'"+cargoEntrega+"\' and campana=\'"+campana+"\'");
                            while(rs.next()){
                                Object [] fila = new Object[3];
                                for (int i=0;i<3;i++){
                                    fila[i] = rs.getObject(i+1);
                                } 
                                codigo.add(rs.getString("codigo"));
                                stockEntregado.add(rs.getInt("stockEntregado"));
                                stockDevuelto.add(rs.getInt("stockDevuelto"));
                            }
                            for(int i=0;i<codigo.size();i++){
                                try{
                                    int cantidad = stockEntregado.get(i) + stockDevuelto.get(i);
                                    PreparedStatement ps1 = conectar.cn.prepareStatement(query1); 
                                    ps1.setInt(1, cantidad);
                                    ps1.setInt(2, 0);
                                    ps1.setString(3, "PEDIDO");
                                    ps1.setInt(4, 0);
                                    ps1.setString(5, codigo.get(i));
                                    ps1.setString(6, cargoEntrega);
                                    int ejecutar = ps1.executeUpdate();

                                    PreparedStatement ps3 = conectar.cn.prepareStatement(query3); 
                                    ps3.setInt(1, conectar.StockPromotor(codigo.get(i), promotor) - stockDevuelto.get(i));
                                    ps3.setString(2, codigo.get(i));
                                    int ejecutar2= ps3.executeUpdate();
                                }catch(SQLException ex){
                                     System.out.println("El error es:\t" + ex);
                                }
                            }
                            try{
                                PreparedStatement ps2 = conectar.cn.prepareStatement(query2);
                                ps2.setString(1,"PEDIDO");
                                ps2.setString(2,null);
                                ps2.setDate(3, null);
                                ps2.setString(4,null);
                                ps2.setDate(5, null);
                                ps2.setString(6,null);
                                ps2.setDate(7, null);
                                ps2.setString(8,cargoEntrega);
                                int ejecutarlimpieza = ps2.executeUpdate();
                                int mensaje1 = JOptionPane.showOptionDialog(null, "Se grabo correctamente", "Grabado", JOptionPane.DEFAULT_OPTION,
                                        JOptionPane.INFORMATION_MESSAGE, null, null, null);
                                actualizarTabla();

                            }catch(SQLException ex){
                                System.out.println("El error es:\t" + ex);
                            }


                        }catch(SQLException e){
                            System.out.println("El error es:\t" + e);
                        }
                    }
                
                
            }else{
                int mensaje = JOptionPane.showOptionDialog(null, "Operacion invalida, documento no esta en REVISION o DEVUELTO", "ERROR", JOptionPane.DEFAULT_OPTION,
                                 JOptionPane.ERROR_MESSAGE, null, null, null); 
            }
        }
    }//GEN-LAST:event_bEliminarConteoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(controlPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(controlPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(controlPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(controlPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                controlPrincipal dialog = new controlPrincipal(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bConsultar;
    private javax.swing.JButton bConteo;
    private javax.swing.JButton bDevolTotal;
    private javax.swing.JButton bDevolucionParcial;
    private javax.swing.JButton bEditar;
    private javax.swing.JButton bEditarEstado;
    private javax.swing.JButton bEliminar;
    private javax.swing.JButton bEliminarConteo;
    private javax.swing.JButton bEliminarDevol;
    private javax.swing.JButton bNuevo;
    private javax.swing.JButton bSalir;
    private javax.swing.JComboBox<String> comboFiltro;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrolCliente;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JLabel lColegios;
    private javax.swing.JLabel lControl;
    private javax.swing.JPanel paneBotonEntrega;
    private javax.swing.JPanel panelBotones;
    private javax.swing.JPanel panelFiltro;
    private javax.swing.JPanel panelInformacion;
    private javax.swing.JPanel panelTabla;
    private javax.swing.JTable tablaControl;
    private javax.swing.JTextField textBuscador;
    // End of variables declaration//GEN-END:variables
}
