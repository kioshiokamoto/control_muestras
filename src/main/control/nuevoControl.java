/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.control;

import com.toedter.calendar.JTextFieldDateEditor;
import conexionBD.conexionBD;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.NumberFormat;
import java.text.ParsePosition;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import main.muestras.modificacionStockSelectMuestra;

/**
 *
 * @author Okamoto
 */
public class nuevoControl extends javax.swing.JDialog {
    private LineBorder border = new LineBorder(Color.RED,2);
    int xx,xy;
    DefaultTableModel modelo = new DefaultTableModel(){
        public boolean isCellEditable(int rowIndex,int columnIndex){return false;}
    };
    conexionBD conectar = new conexionBD();
    Statement st = conectar.conexion();
    public String promotor,contacto,cargoContacto,distritoColegio,campana;

    /**
     * Creates new form nuevoControl
     */
    public nuevoControl(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null); 
        this.getRootPane().setBorder(border);
        modificarFecha();
    }
    public void modificarFecha(){
        java.util.Date fechaActual = new java.util.Date();
        fechaEntrega.setDate(fechaActual);   
        JTextFieldDateEditor dateEditor = (JTextFieldDateEditor)fechaEntrega.getComponent(1);
        dateEditor.setHorizontalAlignment(JTextField.CENTER);
    }
    public void completarTabla(){
        modelo.addColumn("Codigo");
        modelo.addColumn("Descripcion");
        modelo.addColumn("Cantidad");
        
        
        TableColumnModel columnModel = tablaArticulos.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(150);
        columnModel.getColumn(1).setPreferredWidth(350);
        columnModel.getColumn(2).setPreferredWidth(109);
       
        
        tablaArticulos.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        modelo.setRowCount(0);
        
//        try{
//            ResultSet rs = st.executeQuery("SELECT estado,numCargoEntrega,fechaEntrega,colegio,distrito,contactoColegio,cargoContacto,numCargoDevol,fechaDevol FROM DOCUMENTOS_COLEGIOS");
//            while(rs.next()){
//                Object [] fila = new Object[9];
//                for (int i=0;i<9;i++){
//                    fila[i] = rs.getObject(i+1);
//                }     
//                modelo.addRow(fila);
//            }
//        }catch(SQLException e){
//            System.err.println("El error: "+ e);
//        };
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lControl = new javax.swing.JLabel();
        panelRegistro = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        textCargo = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        fechaEntrega = new com.toedter.calendar.JDateChooser();
        textColegio = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        textObservacion = new javax.swing.JTextArea();
        panelBotones = new javax.swing.JPanel();
        bSalir = new javax.swing.JButton();
        bGrabar = new javax.swing.JButton();
        bAnadir = new javax.swing.JButton();
        bEditar = new javax.swing.JButton();
        bEliminar = new javax.swing.JButton();
        panelArticulos = new javax.swing.JPanel();
        jScrolCliente = new javax.swing.JScrollPane();
        tablaArticulos = new javax.swing.JTable(modelo);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(246, 71, 71));

        lControl.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        lControl.setForeground(new java.awt.Color(255, 255, 255));
        lControl.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lControl.setText("Nueva entrega a colegio");
        lControl.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                lControlMouseDragged(evt);
            }
        });
        lControl.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                lControlMousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lControl, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 688, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lControl, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        jLabel1.setText("Cargo de entrega:");

        jLabel2.setText("Colegio:");

        jLabel3.setText("Observación:");

        textCargo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textCargoKeyTyped(evt);
            }
        });

        jLabel4.setText("Fecha de entrega:");

        fechaEntrega.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N

        textColegio.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                textColegioMouseClicked(evt);
            }
        });
        textColegio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                textColegioKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textColegioKeyTyped(evt);
            }
        });

        textObservacion.setColumns(20);
        textObservacion.setRows(5);
        jScrollPane1.setViewportView(textObservacion);

        javax.swing.GroupLayout panelRegistroLayout = new javax.swing.GroupLayout(panelRegistro);
        panelRegistro.setLayout(panelRegistroLayout);
        panelRegistroLayout.setHorizontalGroup(
            panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelRegistroLayout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(panelRegistroLayout.createSequentialGroup()
                        .addComponent(textCargo, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(60, 60, 60)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(fechaEntrega, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(textColegio)
                    .addComponent(jScrollPane1))
                .addContainerGap(39, Short.MAX_VALUE))
        );
        panelRegistroLayout.setVerticalGroup(
            panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelRegistroLayout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(textCargo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel4))
                    .addComponent(fechaEntrega, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(textColegio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelRegistroLayout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );

        bSalir.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        bSalir.setText("Salir");
        bSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bSalirActionPerformed(evt);
            }
        });

        bGrabar.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        bGrabar.setText("Grabar");
        bGrabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bGrabarActionPerformed(evt);
            }
        });

        bAnadir.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        bAnadir.setText("Añadir");
        bAnadir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bAnadirActionPerformed(evt);
            }
        });

        bEditar.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        bEditar.setText("Editar");
        bEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bEditarActionPerformed(evt);
            }
        });

        bEliminar.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        bEliminar.setText("Eliminar");
        bEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bEliminarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelBotonesLayout = new javax.swing.GroupLayout(panelBotones);
        panelBotones.setLayout(panelBotonesLayout);
        panelBotonesLayout.setHorizontalGroup(
            panelBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelBotonesLayout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(bAnadir, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(bEditar, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(bEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(bGrabar, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30))
        );
        panelBotonesLayout.setVerticalGroup(
            panelBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBotonesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bSalir)
                    .addComponent(bGrabar)
                    .addComponent(bAnadir)
                    .addComponent(bEditar)
                    .addComponent(bEliminar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tablaArticulos.getTableHeader().setResizingAllowed(false);
        tablaArticulos.getTableHeader().setReorderingAllowed(false);
        jScrolCliente.setViewportView(tablaArticulos);

        javax.swing.GroupLayout panelArticulosLayout = new javax.swing.GroupLayout(panelArticulos);
        panelArticulos.setLayout(panelArticulosLayout);
        panelArticulosLayout.setHorizontalGroup(
            panelArticulosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelArticulosLayout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(jScrolCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 615, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelArticulosLayout.setVerticalGroup(
            panelArticulosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelArticulosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrolCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 286, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelRegistro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(panelArticulos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelBotones, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(panelRegistro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelArticulos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelBotones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 543, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void lControlMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lControlMouseDragged
        int x = evt.getXOnScreen();
        int y = evt.getYOnScreen();
        this.setLocation(x-xx,y-xy);
    }//GEN-LAST:event_lControlMouseDragged

    private void lControlMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lControlMousePressed
        xx=evt.getX();
        xy=evt.getY();
    }//GEN-LAST:event_lControlMousePressed

    private void bSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bSalirActionPerformed
        dispose();
    }//GEN-LAST:event_bSalirActionPerformed

    private void textColegioMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_textColegioMouseClicked
        if(evt.getClickCount()==2){
            selectorColegio();
        }
    }//GEN-LAST:event_textColegioMouseClicked

    private void textColegioKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textColegioKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_F1){
            selectorColegio();
        }
    }//GEN-LAST:event_textColegioKeyPressed

    private void textCargoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textCargoKeyTyped
        char c=evt.getKeyChar();
        if(!isNumeric(String.valueOf(c))){
            getToolkit().beep();
                evt.consume();
        }
        
        if(textCargo.getText().length()==10){
            evt.consume();
        }
        
    }//GEN-LAST:event_textCargoKeyTyped
    public void selectorStock(){
       agregarArticulo aa= new agregarArticulo(new javax.swing.JFrame(),true);
       aa.promotor=promotor;
        aa.setVisible(true);
        if(!aa.isVisible() && filtro(aa.textCodigo.getText())){
            if(aa.bandera==0){
                Object [] fila = new Object[3];
                fila[0]= aa.textCodigo.getText();
                fila[1]= aa.textDescrip.getText();
                fila[2]= aa.textCantidad.getText();
                modelo.addRow(fila); 
            }      
        } 
        else{
            int mensaje = JOptionPane.showOptionDialog(null, "No se permite ingresar articulo nuevamente en tabla", "ERROR", JOptionPane.DEFAULT_OPTION,
                     JOptionPane.ERROR_MESSAGE, null, null, null);   
        }
    }
    private boolean filtro(String texto){
        for (int i = 0; i < tablaArticulos.getRowCount(); i++){
             if(tablaArticulos.getValueAt(i, 0).toString().equals(texto)){
//                System.out.println("Ya existe articulo en tabla");
                return false;
             }
        }
        return true;
    }
    private void bAnadirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bAnadirActionPerformed
        selectorStock();
    }//GEN-LAST:event_bAnadirActionPerformed

    private void bEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bEliminarActionPerformed
        int seleccionar = tablaArticulos.getSelectedRow();
        if(seleccionar==-1){
            JOptionPane.showMessageDialog(null, "No se ha seleccionado ningun artículo");
        }
        else{
            modelo.removeRow(seleccionar);
        }
    }//GEN-LAST:event_bEliminarActionPerformed

    private void bEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bEditarActionPerformed
        int seleccionar = tablaArticulos.getSelectedRow();
        
        if (seleccionar >= 0) {
            agregarArticulo aa = new agregarArticulo(new javax.swing.JFrame(),true);
            aa.promotor=promotor;
            Object[] filaselect = {
                tablaArticulos.getValueAt(seleccionar, 0),
                tablaArticulos.getValueAt(seleccionar, 1),
                tablaArticulos.getValueAt(seleccionar, 2)
                
            };
            aa.textCodigo.setText(String.valueOf(filaselect[0]));
            aa.textDescrip.setText(String.valueOf(filaselect[1]));
            aa.textCantidad.setText(String.valueOf(filaselect[2]));
            aa.setVisible(true);
                
            if(!aa.isVisible() && !aa.textCodigo.getText().equals("") && !aa.textCantidad.getText().equals("") && aa.bandera==0){
                  tablaArticulos.setValueAt(aa.textCodigo.getText(), seleccionar, 0);
                  tablaArticulos.setValueAt(aa.textDescrip.getText(), seleccionar, 1);
                  tablaArticulos.setValueAt(aa.textCantidad.getText(), seleccionar, 2);      
            } 
        } 
        else{
            JOptionPane.showMessageDialog(null, "No se ha seleccionado ningun artículo");
        }
    }//GEN-LAST:event_bEditarActionPerformed

    private void bGrabarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bGrabarActionPerformed
        int seleccionar = tablaArticulos.getRowCount();
        String codigo,descripcion,observacion;
        String cargoDeEntrega= textCargo.getText();
        String colegio = textColegio.getText();
        int cantidad,stockPromotor;
        String query = "INSERT INTO MOV_DOCUMENTOS_COLEGIOS (rucProm,numCargoEntrega,colegio,codigo,descrip,stockEntregado,estado,campana) values (?,?,?,?,?,?,'REVISION',?);";
        String query2 = "INSERT INTO DOCUMENTOS_COLEGIOS (rucProm,estado,numCargoEntrega,fechaEntrega,colegio,distrito,contactoColegio,cargoContacto,observacion,campana) values (?,'REVISION',?,?,?,?,?,?,?,?);";
        String query3="UPDATE \"STOCK_"+promotor+"\""+" set stock=? where codigo=?;";
        java.util.Date utilDate = fechaEntrega.getDate();
        java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
        if(seleccionar>=1 && !textCargo.getText().isEmpty() && !textColegio.getText().isEmpty()  ){
            try{
                 if(!conectar.ifExists("Select * from DOCUMENTOS_COLEGIOS where numCargoEntrega=?;", textCargo.getText())){
                     for(int i=0; i<seleccionar;i++){
                        codigo=(String) tablaArticulos.getValueAt(i, 0);
                        descripcion=(String) tablaArticulos.getValueAt(i, 1);
                        cantidad= Integer.parseInt((String) tablaArticulos.getValueAt(i, 2));
                        PreparedStatement ps = conectar.cn.prepareStatement(query); 
                        PreparedStatement ps2 = conectar.cn.prepareStatement(query3); 
                        
                        ps.setString(1, promotor);
                        ps.setString(2, cargoDeEntrega);
                        ps.setString(3, colegio);
                        ps.setString(4, codigo);
                        ps.setString(5, descripcion);
                        ps.setInt(6, cantidad);
                        ps.setString(7, campana);
                        
                        stockPromotor= conectar.StockPromotor(codigo, promotor);
                        ps2.setInt(1, stockPromotor - cantidad);
                        ps2.setString(2, codigo);
                        
                        int ejecucion =    ps.executeUpdate();
                        int ejecucionStockPromotor= ps2.executeUpdate();
                     }
                     try{
                        PreparedStatement ps3 = conectar.cn.prepareStatement(query2); 
                        ps3.setString(1,promotor);
                        ps3.setString(2, cargoDeEntrega);
                        ps3.setDate(3, sqlDate);
                        ps3.setString(4, colegio);
                        ps3.setString(5,distritoColegio);
                        ps3.setString(6, contacto);
                        ps3.setString(7, cargoContacto);
                        ps3.setString(8,textObservacion.getText());
                        ps3.setString(9, campana);
                        int ejecucion =    ps3.executeUpdate();
                        
                        int mensaje = JOptionPane.showOptionDialog(null, "Se grabo correctamente", "Grabado", JOptionPane.DEFAULT_OPTION,
                                     JOptionPane.INFORMATION_MESSAGE, null, null, null);

                        if(mensaje==0){
                            setVisible(false);
                        }
                     }catch(SQLException e){
                         System.err.println("El error" + e);
                     }
                     
                 }else{
                    int mensaje = JOptionPane.showOptionDialog(null, "Error al grabar, ya existe documento", "ERROR", JOptionPane.DEFAULT_OPTION,
                                 JOptionPane.ERROR_MESSAGE, null, null, null); 
                    dispose();
                 }
            }catch(SQLException ex){
                System.err.println("El error" + ex);
            }
        }else{
            int mensaje = JOptionPane.showOptionDialog(null, "Error al grabar, Falta ingresar datos en documento", "ERROR", JOptionPane.DEFAULT_OPTION,
                                 JOptionPane.ERROR_MESSAGE, null, null, null); 
        }
    }//GEN-LAST:event_bGrabarActionPerformed

    private void textColegioKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textColegioKeyTyped
        evt.consume();
    }//GEN-LAST:event_textColegioKeyTyped
    //Metodo para validar si lo ingresado es numerico   
    public static boolean isNumeric(String str){
        NumberFormat formatter = NumberFormat.getInstance();
        ParsePosition pos = new ParsePosition(0);
        formatter.parse(str, pos);
        return str.length() == pos.getIndex();
    }
    public void selectorColegio(){
        selectorColegio sc= new selectorColegio(new javax.swing.JFrame(),true);
        sc.promotor=promotor;
        sc.setVisible(true);
        if(!sc.isVisible()&& sc.bandera==0){
            textColegio.setText(sc.colegio);
            contacto = sc.contacto;
            cargoContacto=sc.cargoContacto;
            distritoColegio=sc.distrito;
            sc.dispose();
        }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(nuevoControl.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(nuevoControl.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(nuevoControl.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(nuevoControl.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                nuevoControl dialog = new nuevoControl(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bAnadir;
    private javax.swing.JButton bEditar;
    private javax.swing.JButton bEliminar;
    private javax.swing.JButton bGrabar;
    private javax.swing.JButton bSalir;
    private com.toedter.calendar.JDateChooser fechaEntrega;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrolCliente;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lControl;
    private javax.swing.JPanel panelArticulos;
    private javax.swing.JPanel panelBotones;
    private javax.swing.JPanel panelRegistro;
    private javax.swing.JTable tablaArticulos;
    private javax.swing.JTextField textCargo;
    private javax.swing.JTextField textColegio;
    private javax.swing.JTextArea textObservacion;
    // End of variables declaration//GEN-END:variables
}
