/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.clientes;

import java.awt.Color;
import javax.swing.border.LineBorder;
import conexionBD.conexionBD;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.Timer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableRowSorter;
import main.muestras.consultaDeDocSelect;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.swing.JRViewer;
/**
 *
 * @author Okamoto
 */
public class Clientes extends javax.swing.JFrame {
    private LineBorder border = new LineBorder(Color.RED,2);
    int xx,xy;
    DefaultTableModel modelo = new DefaultTableModel(){
        public boolean isCellEditable(int rowIndex,int columnIndex){return false;}
    };
    conexionBD conectar = new conexionBD();
    Statement st = conectar.conexion();
    
    
    
    
    

    /**
     * Creates new form crearClientes
     */
    public Clientes() {
        initComponents();
        this.setLocationRelativeTo(null); 
        this.getRootPane().setBorder(border);
        completarTabla();
        
    }
//    private void filtro(String consulta, JTable jtableBuscar){
//        DefaultTableModel dm = (DefaultTableModel) jtableBuscar.getModel();
//        TableRowSorter<DefaultTableModel> tr = new TableRowSorter<>(dm);
//        jtableBuscar.setRowSorter(tr);
//        tr.setRowFilter(RowFilter.regexFilter(consulta));
//    }
    private void completarTabla(){
        modelo.addColumn("Codigo");
        modelo.addColumn("Razon Social");
        modelo.addColumn("Promotor Asig.");
        modelo.addColumn("Direccion");
        modelo.addColumn("Contacto");
        modelo.addColumn("Cargo");
        modelo.addColumn("Telefono");
        modelo.addColumn("Departamento");
        modelo.addColumn("Provincia");
        modelo.addColumn("Distrito");
        modelo.addColumn("META");
        TableColumnModel columnModel = tablaCliente.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(100);
        columnModel.getColumn(1).setPreferredWidth(200);
        columnModel.getColumn(2).setPreferredWidth(100);
        columnModel.getColumn(3).setPreferredWidth(150);
        columnModel.getColumn(4).setPreferredWidth(150);
        columnModel.getColumn(5).setPreferredWidth(100);
        columnModel.getColumn(6).setPreferredWidth(100);
        columnModel.getColumn(7).setPreferredWidth(100);
        columnModel.getColumn(8).setPreferredWidth(100);
        columnModel.getColumn(9).setPreferredWidth(100);
        columnModel.getColumn(10).setPreferredWidth(60);
        tablaCliente.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        modelo.setRowCount(0);
        try{
            ResultSet rs = st.executeQuery("SELECT clRuc,clRazSoc,clPromAsig,clDirec,clContacto,cLCargoContacto,clTelefonoContacto,clDep,clProv,clDistr,clMeta FROM CLIENTES WHERE clEstad=1 ");
            while(rs.next()){
                Object [] fila = new Object[11];
                for (int i=0;i<11;i++){
                    fila[i] = rs.getObject(i+1);
                }     
                modelo.addRow(fila);
            }
        }catch(SQLException e){
            System.err.println("El error: "+ e);
        };
        
    }
    public void actualizarTabla(){
        modelo.setRowCount(0);
        if(checkTodos.isSelected()){
            modelo.setRowCount(0);
         
            
            try{
                ResultSet rs = st.executeQuery("SELECT clRuc,clRazSoc,clPromAsig,clDirec,clContacto,cLCargoContacto,clTelefonoContacto,clDep,clProv,clDistr,clMeta FROM CLIENTES WHERE clEstad=1 ");
                while(rs.next()){
                    Object [] fila = new Object[11];
                    for (int i=0;i<11;i++){
                        fila[i] = rs.getObject(i+1);
                    }     
                    modelo.addRow(fila);
                }
            }catch(SQLException e){
                System.err.println("El error: "+ e);
            };
            
            
            
        }
        else{
            modelo.setRowCount(0);
            try{
                ResultSet rs = st.executeQuery("SELECT clRuc,clRazSoc,clPromAsig,clDirec,clContacto,cLCargoContacto,clTelefonoContacto,clDep,clProv,clDistr,clMeta FROM CLIENTES");
                while(rs.next()){
                    Object [] fila = new Object[11];
                    for (int i=0;i<11;i++){
                        fila[i] = rs.getObject(i+1);
                    }     
                    modelo.addRow(fila);
                }
            }catch(SQLException e){
                System.err.println("El error: "+ e);
            };
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lClientes = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        lBuscar = new javax.swing.JLabel();
        textBuscador = new javax.swing.JTextField();
        lFiltrarPor = new javax.swing.JLabel();
        comboFiltro = new javax.swing.JComboBox<>();
        checkTodos = new javax.swing.JCheckBox();
        bActualizar = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrolCliente = new javax.swing.JScrollPane();
        tablaCliente = new javax.swing.JTable(modelo);
        jPanel4 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        bReporte = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(246, 71, 71));

        lClientes.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        lClientes.setForeground(new java.awt.Color(255, 255, 255));
        lClientes.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lClientes.setText("Clientes");
        lClientes.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                lClientesMouseDragged(evt);
            }
        });
        lClientes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                lClientesMousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lClientes, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lClientes, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        lBuscar.setText("Buscar:");

        textBuscador.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                textBuscadorKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textBuscadorKeyTyped(evt);
            }
        });

        lFiltrarPor.setText("Por:");
        lFiltrarPor.setEnabled(false);

        comboFiltro.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "RUC", "Razon Social","Contacto","Departamento","Provincia","Distrito" }));
        comboFiltro.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        checkTodos.setSelected(true);
        checkTodos.setText("Solo activos");
        checkTodos.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        checkTodos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkTodosActionPerformed(evt);
            }
        });

        bActualizar.setText("Actualizar");
        bActualizar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bActualizarMouseClicked(evt);
            }
        });
        bActualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bActualizarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(lBuscar)
                        .addGap(18, 18, 18)
                        .addComponent(textBuscador, javax.swing.GroupLayout.PREFERRED_SIZE, 563, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(bActualizar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lFiltrarPor)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(checkTodos)
                    .addComponent(comboFiltro, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(76, 76, 76))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lBuscar)
                    .addComponent(textBuscador, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lFiltrarPor)
                    .addComponent(comboFiltro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(checkTodos)
                    .addComponent(bActualizar, javax.swing.GroupLayout.PREFERRED_SIZE, 26, Short.MAX_VALUE))
                .addContainerGap(8, Short.MAX_VALUE))
        );

        tablaCliente.getTableHeader().setResizingAllowed(false);
        tablaCliente.getTableHeader().setReorderingAllowed(false);
        jScrolCliente.setViewportView(tablaCliente);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(26, Short.MAX_VALUE)
                .addComponent(jScrolCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 858, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrolCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 386, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(16, Short.MAX_VALUE))
        );

        jButton1.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jButton1.setText("Nuevo");
        jButton1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton1MouseClicked(evt);
            }
        });
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jButton2.setText("Editar");
        jButton2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton2MouseClicked(evt);
            }
        });
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jButton3.setText("Salir");
        jButton3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        bReporte.setText("Reporte");
        bReporte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bReporteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(bReporte, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2)
                    .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(bReporte))
                .addContainerGap(19, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        dispose();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void lClientesMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lClientesMousePressed
        xx=evt.getX();
        xy=evt.getY();
    }//GEN-LAST:event_lClientesMousePressed

    private void lClientesMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lClientesMouseDragged
        int x = evt.getXOnScreen();
        int y = evt.getYOnScreen();
        this.setLocation(x-xx,y-xy);
    }//GEN-LAST:event_lClientesMouseDragged

    private void jButton1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MouseClicked
        
    }//GEN-LAST:event_jButton1MouseClicked

    private void checkTodosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkTodosActionPerformed
        if(checkTodos.isSelected()){
            modelo.setRowCount(0);
         
            
            try{
                ResultSet rs = st.executeQuery("SELECT clRuc,clRazSoc,clPromAsig,clDirec,clContacto,cLCargoContacto,clTelefonoContacto,clDep,clProv,clDistr,clMeta FROM CLIENTES WHERE clEstad=1 ");
                while(rs.next()){
                    Object [] fila = new Object[11];
                    for (int i=0;i<11;i++){
                        fila[i] = rs.getObject(i+1);
                    }     
                    modelo.addRow(fila);
                }
            }catch(SQLException e){
                System.err.println("El error: "+ e);
            };
            
            
            
        }
        else{
            modelo.setRowCount(0);
            try{
                ResultSet rs = st.executeQuery("SELECT clRuc,clRazSoc,clPromAsig,clDirec,clContacto,cLCargoContacto,clTelefonoContacto,clDep,clProv,clDistr,clMeta FROM CLIENTES ");
                while(rs.next()){
                    Object [] fila = new Object[11];
                    for (int i=0;i<11;i++){
                        fila[i] = rs.getObject(i+1);
                    }     
                    modelo.addRow(fila);
                }
            }catch(SQLException e){
                System.err.println("El error: "+ e);
            };
        }
    }//GEN-LAST:event_checkTodosActionPerformed

    private void textBuscadorKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textBuscadorKeyReleased
            
    }//GEN-LAST:event_textBuscadorKeyReleased

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        creaCliente cc= new creaCliente(this,true);
        cc.setVisible(true);
        if(!cc.isVisible()){
            actualizarTabla();
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton2MouseClicked
    }//GEN-LAST:event_jButton2MouseClicked

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        int filaseleccionada;
        try{
            filaseleccionada= tablaCliente.getSelectedRow();
            if (filaseleccionada==-1){
                JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna fila");
            }
            else{
                String ruc = (String)tablaCliente.getValueAt(filaseleccionada, 0);
                String razon = (String)tablaCliente.getValueAt(filaseleccionada, 1);
                String promotor = (String)tablaCliente.getValueAt(filaseleccionada, 2);
                String dir = (String)tablaCliente.getValueAt(filaseleccionada, 3);
                String dep = (String)tablaCliente.getValueAt(filaseleccionada, 7);
                String prov = (String)tablaCliente.getValueAt(filaseleccionada, 8);
                String dist= (String)tablaCliente.getValueAt(filaseleccionada, 9);
                String contacto = (String)tablaCliente.getValueAt(filaseleccionada, 4);
                String cargo = (String)tablaCliente.getValueAt(filaseleccionada, 5);
                String telefono = (String)tablaCliente.getValueAt(filaseleccionada, 6);
                String meta = (tablaCliente.getValueAt(filaseleccionada, 10)).toString();
                editaCliente ec= new editaCliente(this,true);
                ec.recibeDatos(ruc, razon, promotor, dir, dep, prov, dist,contacto,cargo,telefono,meta);
                ec.setVisible(true);
                if(!ec.isVisible()){
                    actualizarTabla();
                    
                }
            } 
        }catch(HeadlessException ex){  
            
        } 
    }//GEN-LAST:event_jButton2ActionPerformed

    private void bActualizarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bActualizarMouseClicked
        modelo.setRowCount(0);
        if(checkTodos.isSelected()){
            modelo.setRowCount(0);
         
            
            try{
                ResultSet rs = st.executeQuery("SELECT clRuc,clRazSoc,clPromAsig,clDirec,clContacto,cLCargoContacto,clTelefonoContacto,clDep,clProv,clDistr,clMeta FROM CLIENTES WHERE clEstad=1 ");
                while(rs.next()){
                    Object [] fila = new Object[11];
                    for (int i=0;i<11;i++){
                        fila[i] = rs.getObject(i+1);
                    }     
                    modelo.addRow(fila);
                }
            }catch(SQLException e){
                System.err.println("El error: "+ e);
            };
            
            
            
        }
        else{
            modelo.setRowCount(0);
            try{
                ResultSet rs = st.executeQuery("SELECT clRuc,clRazSoc,clPromAsig,clDirec,clContacto,cLCargoContacto,clTelefonoContacto,clDep,clProv,clDistr,clMeta FROM CLIENTES ");
                while(rs.next()){
                    Object [] fila = new Object[11];
                    for (int i=0;i<11;i++){
                        fila[i] = rs.getObject(i+1);
                    }     
                    modelo.addRow(fila);
                }
            }catch(SQLException e){
                System.err.println("El error: "+ e);
            };
        }
    }//GEN-LAST:event_bActualizarMouseClicked
    TableRowSorter trs=null;
    private void textBuscadorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textBuscadorKeyTyped
        if(comboFiltro.getSelectedIndex()==0){
            textBuscador.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent ke){
                trs.setRowFilter(RowFilter.regexFilter("(?i)"+textBuscador.getText(), 0));
            }
            });
            trs =new TableRowSorter(modelo);
            tablaCliente.setRowSorter(trs);
        }
        if(comboFiltro.getSelectedIndex()==1){
            
            textBuscador.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent ke){
                trs.setRowFilter(RowFilter.regexFilter("(?i)"+textBuscador.getText(), 1));
            }
            });
            trs =new TableRowSorter(modelo);
            tablaCliente.setRowSorter(trs);
        }
        if(comboFiltro.getSelectedIndex()==2){
            
            textBuscador.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent ke){
                trs.setRowFilter(RowFilter.regexFilter("(?i)"+textBuscador.getText(), 4));
            }
            });
            trs =new TableRowSorter(modelo);
            tablaCliente.setRowSorter(trs);
        }
        //Departamento
        if(comboFiltro.getSelectedIndex()==3){
            textBuscador.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent ke){
                trs.setRowFilter(RowFilter.regexFilter("(?i)"+textBuscador.getText(), 7));
            }
            });
            trs =new TableRowSorter(modelo);
            tablaCliente.setRowSorter(trs);
        }
        if(comboFiltro.getSelectedIndex()==4){
            textBuscador.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent ke){
                trs.setRowFilter(RowFilter.regexFilter("(?i)"+textBuscador.getText(), 8));
            }
            });
            trs =new TableRowSorter(modelo);
            tablaCliente.setRowSorter(trs);
        }
        if(comboFiltro.getSelectedIndex()==5){
            textBuscador.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent ke){
                trs.setRowFilter(RowFilter.regexFilter("(?i)"+textBuscador.getText(), 9));
            }
            });
            trs =new TableRowSorter(modelo);
            tablaCliente.setRowSorter(trs);
        }
    }//GEN-LAST:event_textBuscadorKeyTyped

    private void bActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bActualizarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_bActualizarActionPerformed

    private void bReporteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bReporteActionPerformed
        String actividad = "1";
        String titulo="Reporte de clientes activos";
//        if(!checkTodos.isSelected()){
//            actividad="0";
//            titulo="Reporte de clientes activos e inactivos";
//        }
        conectar.conexion();
        JasperReport jr = null;
        JDialog reporte = new JDialog(new javax.swing.JFrame(),true);
        reporte.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getClassLoader().getResource("imagenes/coredise.png")));
        reporte.setSize(950,1050);
        reporte.setLocationRelativeTo(null);
        reporte.setTitle("Reporte de Clientes activos");
        try {
            Map parametro = new HashMap();
            parametro.put("parameter1", actividad);
            parametro.put("parameter2", titulo);
            JasperPrint jp = JasperFillManager.fillReport(this.getClass().getClassLoader().getResourceAsStream("reportes/clientes.jasper"),parametro,conectar.getConexion());
            
            JRViewer jv = new JRViewer(jp);
            jv.setZoomRatio((float) 0.75);
            reporte.getContentPane().add(jv);
            
            reporte.setVisible(true);
        } catch (JRException ex) {
            Logger.getLogger(consultaDeDocSelect.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }//GEN-LAST:event_bReporteActionPerformed
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Clientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Clientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Clientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Clientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Clientes().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bActualizar;
    private javax.swing.JButton bReporte;
    private javax.swing.JCheckBox checkTodos;
    private javax.swing.JComboBox<String> comboFiltro;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrolCliente;
    private javax.swing.JLabel lBuscar;
    private javax.swing.JLabel lClientes;
    private javax.swing.JLabel lFiltrarPor;
    private javax.swing.JTable tablaCliente;
    private javax.swing.JTextField textBuscador;
    // End of variables declaration//GEN-END:variables
}
