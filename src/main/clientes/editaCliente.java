/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.clientes;

import conexionBD.conexionBD;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.border.LineBorder;
import static main.control.nuevoControl.isNumeric;

/**
 *
 * @author Okamoto
 */
public class editaCliente extends javax.swing.JDialog {
    private LineBorder border = new LineBorder(Color.RED,2);
    int xx,xy;
    conexionBD conectar = new conexionBD();
    Statement st = conectar.conexion();
    public ArrayList <String> promotores = new ArrayList<String>();
    public ArrayList <String> ruc = new ArrayList<String>();
    
    
    public editaCliente(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null); 
        this.getRootPane().setBorder(border);
        anadirCombo();
        completarCombo();
        
        
    }
    public void completarCombo(){
        
        try{
            ResultSet rs = st.executeQuery("select promApellidos,promRuc from promotores where promActiv='ACTIVO' order by promApellidos asc");
            while(rs.next()){
                textAsignado.addItem(rs.getString("promApellidos"));
                promotores.add(rs.getString("promApellidos"));
                ruc.add(rs.getString("promRuc"));
            }
        }catch(SQLException e){
            System.err.println("El error: "+ e);
        };
        
    }
    public void recibeDatos(String ruc, String razon, String promot,String dir, String dep, String prov, String dist,String contacto,String cargo,String telefono,String meta){
        textRuc.setText(ruc);
        textRaz.setText(razon);
        textAsignado.getModel().setSelectedItem(promot);
        textDirec.setText(dir);
        comboDep.getModel().setSelectedItem(dep);
        comboProv.getModel().setSelectedItem(prov);
        comboDist.getModel().setSelectedItem(dist);   
        textContacto.setText(contacto);
        textCargo.setText(cargo);
        textTelefono.setText(telefono);
        textMeta.setText(meta);
        
    }
    public void anadirCombo(){
        comboDep.addItem("Amazonas");
        comboDep.addItem("Áncash");
        comboDep.addItem("Apurímac");
        comboDep.addItem("Arequipa");
        comboDep.addItem("Ayacucho");
        comboDep.addItem("Cajamarca");
        comboDep.addItem("Callao");
        comboDep.addItem("Cusco");
        comboDep.addItem("Huancavelica");
        comboDep.addItem("Huánuco");
        comboDep.addItem("Ica");
        comboDep.addItem("Junín");
        comboDep.addItem("La Libertad");
        comboDep.addItem("Lambayeque");
        comboDep.addItem("Lima");
        comboDep.addItem("Loreto");
        comboDep.addItem("Madre de Dios");
        comboDep.addItem("Moquegua");
        comboDep.addItem("Pasco");
        comboDep.addItem("Piura");
        comboDep.addItem("Puno");
        comboDep.addItem("San Martín");
        comboDep.addItem("Tacna");
        comboDep.addItem("Tumbes");
        comboDep.addItem("Ucayali");  
        
        comboDep.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(comboDep.getSelectedItem().toString().equals("Amazonas")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Chachapoyas");
                    comboProv.addItem("Bagua");
                    comboProv.addItem("Bongara");
                    comboProv.addItem("Condorcanqui");
                    comboProv.addItem("Luya");
                    comboProv.addItem("Rodríguez de Mendoza");
                    comboProv.addItem("Utcubamba");      
                }
                if(comboDep.getSelectedItem().toString().equals("Áncash")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Huaraz");
                    comboProv.addItem("Aija");
                    comboProv.addItem("Antonio Raimondi");
                    comboProv.addItem("Asunción");
                    comboProv.addItem("Bolognesi");
                    comboProv.addItem("Carhuaz");
                    comboProv.addItem("Carlos Fermín Fitzcarrald");
                    comboProv.addItem("Casma");
                    comboProv.addItem("Corongo");
                    comboProv.addItem("Huari");
                    comboProv.addItem("Huarmey");
                    comboProv.addItem("Huaylas");
                    comboProv.addItem("Mariscal Luzuriaga");
                    comboProv.addItem("Ocros");
                    comboProv.addItem("Pallasca");
                    comboProv.addItem("Pomabamba");
                    comboProv.addItem("Recuay");
                    comboProv.addItem("Santa");
                    comboProv.addItem("Sihuas");
                    comboProv.addItem("Yungay");
                    
            
                }
                if(comboDep.getSelectedItem().toString().equals("Apurímac")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Abancay");
                    comboProv.addItem("Andahuaylas");
                    comboProv.addItem("Antabamba");
                    comboProv.addItem("Aymaraes");
                    comboProv.addItem("Cotabambas");
                    comboProv.addItem("Chincheros");
                    comboProv.addItem("Grau");
                    
            
                }
                if(comboDep.getSelectedItem().toString().equals("Arequipa")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Arequipa");
                    comboProv.addItem("Camaná");
                    comboProv.addItem("Caravelí");
                    comboProv.addItem("Castilla");
                    comboProv.addItem("Caylloma");
                    comboProv.addItem("Condesuyos");
                    comboProv.addItem("Islay");
                    comboProv.addItem("La Unión");
                    
            
                }
                if(comboDep.getSelectedItem().toString().equals("Ayacucho")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Huamanga");
                    comboProv.addItem("Cangallo");
                    comboProv.addItem("Huanca Sancos");
                    comboProv.addItem("Huanta");
                    comboProv.addItem("La Mar");
                    comboProv.addItem("Lucanas");
                    comboProv.addItem("Parinacochas");
                    comboProv.addItem("Páucar del Sara Sara");
                    comboProv.addItem("Sucre");
                    comboProv.addItem("Víctor Fajardo");
                    comboProv.addItem("Vilcashuamán");
                    
            
                }
                if(comboDep.getSelectedItem().toString().equals("Cajamarca")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Cajamarca");
                    comboProv.addItem("Cajabamba");
                    comboProv.addItem("Celendín");
                    comboProv.addItem("Chota");
                    comboProv.addItem("Contumaza");
                    comboProv.addItem("Cutervo");
                    comboProv.addItem("Hualgayoc");
                    comboProv.addItem("Jaén");
                    comboProv.addItem("San Ignacio");
                    comboProv.addItem("San Marcos");
                    comboProv.addItem("San Miguel");
                    comboProv.addItem("San Pablo");
                    comboProv.addItem("Santa Cruz");
                    
                }
                if(comboDep.getSelectedItem().toString().equals("Callao")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Prov. Const. del Callao");
                    
            
                }
                if(comboDep.getSelectedItem().toString().equals("Cusco")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Cuzco");
                    comboProv.addItem("Acomayo");
                    comboProv.addItem("Anta");
                    comboProv.addItem("Calca");
                    comboProv.addItem("Canas");
                    comboProv.addItem("Canchis");
                    comboProv.addItem("Chumbivilcas");
                    comboProv.addItem("Espinar");
                    comboProv.addItem("La Convención");
                    comboProv.addItem("Paruro");
                    comboProv.addItem("Paucartambo");
                    comboProv.addItem("Quispicanchi");
                    comboProv.addItem("Urubamba");
                    
            
            
                }
                if(comboDep.getSelectedItem().toString().equals("Huancavelica")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Huancavelica");
                    comboProv.addItem("Acobamba");
                    comboProv.addItem("Angaraes");
                    comboProv.addItem("Castrovirreyna");
                    comboProv.addItem("Churcampa");
                    comboProv.addItem("Huaytará");
                    comboProv.addItem("Tayacaja");
                    
            
                }
                if(comboDep.getSelectedItem().toString().equals("Huánuco")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Huánuco");
                    comboProv.addItem("Ambo");
                    comboProv.addItem("Dos de Mayo");
                    comboProv.addItem("Huacaybamba");
                    comboProv.addItem("Huamalíes");
                    comboProv.addItem("Leoncio Prado");
                    comboProv.addItem("Marañón");
                    comboProv.addItem("Pachitea");
                    comboProv.addItem("Puerto Inca");
                    comboProv.addItem("Lauricocha");
                    comboProv.addItem("Yarowilca");
                    
            
                }
                if(comboDep.getSelectedItem().toString().equals("Ica")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Ica");
                    comboProv.addItem("Chincha");
                    comboProv.addItem("Nazca");
                    comboProv.addItem("Palpa");
                    comboProv.addItem("Pisco");
                   
            
                }
                if(comboDep.getSelectedItem().toString().equals("Junín")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Huancayo");
                    comboProv.addItem("Chanchamayo");
                    comboProv.addItem("Chupaca");
                    comboProv.addItem("Concepción");
                    comboProv.addItem("Jauja");
                    comboProv.addItem("Junín");
                    comboProv.addItem("Satipo");
                    comboProv.addItem("Tarma");
                    comboProv.addItem("Yauli");
                }
                if(comboDep.getSelectedItem().toString().equals("La Libertad")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Trujillo");
                    comboProv.addItem("Ascope");
                    comboProv.addItem("Bolívar");
                    comboProv.addItem("Chepén");
                    comboProv.addItem("Gran Chimú");
                    comboProv.addItem("Julcán");
                    comboProv.addItem("Otuzco");
                    comboProv.addItem("Pacasmayo");
                    comboProv.addItem("Pataz");
                    comboProv.addItem("Sánchez Carrión");
                    comboProv.addItem("Santiago de Chuco");
                    comboProv.addItem("Virú");
                    
            
                }
                if(comboDep.getSelectedItem().toString().equals("Lambayeque")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Chiclayo");
                    comboProv.addItem("Ferreñafe");
                    comboProv.addItem("Lambayeque");
                    
            
                }
                if(comboDep.getSelectedItem().toString().equals("Lima")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Lima");
                    comboProv.addItem("Barranca");
                    comboProv.addItem("Cajatambo");
                    comboProv.addItem("Canta");
                    comboProv.addItem("Cañete");
                    comboProv.addItem("Huaral");
                    comboProv.addItem("Huarochirí");
                    comboProv.addItem("Huaura");
                    comboProv.addItem("Oyón");
                    comboProv.addItem("Yauyos");
                    comboProv.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if(comboProv.getSelectedItem().toString().equals("Lima")){
                            comboDist.removeAllItems();
                            comboDist.addItem("<DISTRITO>");
                            comboDist.addItem("Ancón");
                            comboDist.addItem("Ate Vitarte");
                            comboDist.addItem("Barranco");
                            comboDist.addItem("Breña");
                            comboDist.addItem("Carabayllo");
                            comboDist.addItem("Chaclacayo");
                            comboDist.addItem("Chorrillos");
                            comboDist.addItem("Cieneguilla");
                            comboDist.addItem("Comas");
                            comboDist.addItem("El Agustino");
                            comboDist.addItem("Independencia");
                            comboDist.addItem("Jesús María");
                            comboDist.addItem("La Molina");
                            comboDist.addItem("La Victoria");
                            comboDist.addItem("Lima");
                            comboDist.addItem("Lince");
                            comboDist.addItem("Los Olivos");
                            comboDist.addItem("Lurigancho");
                            comboDist.addItem("Lurín");
                            comboDist.addItem("Magdalena del Mar");
                            comboDist.addItem("Miraflores");
                            comboDist.addItem("Pachacamac");
                            comboDist.addItem("Pucusana");
                            comboDist.addItem("Pueblo Libre");
                            comboDist.addItem("Puente Piedra");
                            comboDist.addItem("Punta Hermosa");
                            comboDist.addItem("Punta Negra");
                            comboDist.addItem("Rímac");
                            comboDist.addItem("San Bartolo");
                            comboDist.addItem("San Borja");
                            comboDist.addItem("San Isidro");
                            comboDist.addItem("San Juan de Lurigancho");
                            comboDist.addItem("San Juan de Miraflores");
                            comboDist.addItem("San Luis");
                            comboDist.addItem("San Martín de Porres");
                            comboDist.addItem("San Miguel");
                            comboDist.addItem("Santa Anita");
                            comboDist.addItem("Santa María del Mar");
                            comboDist.addItem("Santa Rosa");
                            comboDist.addItem("Santiago de Surco");
                            comboDist.addItem("Surquillo");
                            comboDist.addItem("Villa El Salvador");
                            comboDist.addItem("Villa María del Triunfo");
                        }
                    }
                });
            
                }
                if(comboDep.getSelectedItem().toString().equals("Loreto")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Maynas");
                    comboProv.addItem("Alto Amazonas");
                    comboProv.addItem("Datem del Marañón");
                    comboProv.addItem("Loreto");
                    comboProv.addItem("Mariscal Ramón Castilla");
                    comboProv.addItem("Putumayo");
                    comboProv.addItem("Requena");
                    comboProv.addItem("Ucayali");
                    
            
                }
                if(comboDep.getSelectedItem().toString().equals("Madre de Dios")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Tambopata");
                    comboProv.addItem("Manu");
                    comboProv.addItem("Tahuamanu");
                    
            
                }
                if(comboDep.getSelectedItem().toString().equals("Moquegua")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Mariscal Nieto");
                    comboProv.addItem("General Sánchez Cerro");
                    comboProv.addItem("Ilo");
                    
            
                }
                if(comboDep.getSelectedItem().toString().equals("Pasco")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Pasco");
                    comboProv.addItem("Daniel Alcides Carrión");
                    comboProv.addItem("Oxapampa");
                    
            
                }
                if(comboDep.getSelectedItem().toString().equals("Piura")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Piura");
                    comboProv.addItem("Ayabaca");
                    comboProv.addItem("Huancabamba");
                    comboProv.addItem("Morropón");
                    comboProv.addItem("Paita");
                    comboProv.addItem("Sechura");
                    comboProv.addItem("Sullana");
                    comboProv.addItem("Talara");
                    
            
                }
                if(comboDep.getSelectedItem().toString().equals("Puno")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Puno");
                    comboProv.addItem("Azángaro");
                    comboProv.addItem("Carabaya");
                    comboProv.addItem("Chucuito");
                    comboProv.addItem("El Collao");
                    comboProv.addItem("Huancané");
                    comboProv.addItem("Lampa");
                    comboProv.addItem("Melgar");
                    comboProv.addItem("Moho");
                    comboProv.addItem("San Antonio de Putina");
                    comboProv.addItem("San Román");
                    comboProv.addItem("Sandia");
                    comboProv.addItem("Yunguyo");
                    
            
                }
                if(comboDep.getSelectedItem().toString().equals("San Martín")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Moyobamba");
                    comboProv.addItem("Bellavista");
                    comboProv.addItem("El Dorado");
                    comboProv.addItem("Huallaga");
                    comboProv.addItem("Lamas");
                    comboProv.addItem("Mariscal Cáceres");
                    comboProv.addItem("Picota");
                    comboProv.addItem("Rioja");
                    comboProv.addItem("San Martín");
                    comboProv.addItem("Tocache");
                    
            
                }
                if(comboDep.getSelectedItem().toString().equals("Tacna")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Tacna");
                    comboProv.addItem("Candarave");
                    comboProv.addItem("Jorge Basadre");
                    comboProv.addItem("Tarata");
                    
            
                }
                if(comboDep.getSelectedItem().toString().equals("Tumbes")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Tumbes");
                    comboProv.addItem("Contralmirante Villar");
                    comboProv.addItem("Zarumilla");
                    
            
                }
                if(comboDep.getSelectedItem().toString().equals("Ucayali")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Coronel Portillo");
                    comboProv.addItem("Atalaya");
                    comboProv.addItem("Padre Abad");
                    comboProv.addItem("Purús");
                   
            
                }
            }
            
        });
        
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        lRuc = new javax.swing.JLabel();
        lRazon = new javax.swing.JLabel();
        lDireccion = new javax.swing.JLabel();
        lAsignado = new javax.swing.JLabel();
        lDepa = new javax.swing.JLabel();
        lProv = new javax.swing.JLabel();
        lDistrito = new javax.swing.JLabel();
        textRuc = new javax.swing.JTextField();
        textRaz = new javax.swing.JTextField();
        textDirec = new javax.swing.JTextField();
        comboProv = new javax.swing.JComboBox<>();
        comboDep = new javax.swing.JComboBox<>();
        comboDist = new javax.swing.JComboBox<>();
        inactivo = new javax.swing.JCheckBox();
        textAsignado = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        textContacto = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        textTelefono = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        textCargo = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        textMeta = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        bGrabar = new javax.swing.JButton();
        bSalir = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        titulo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);

        lRuc.setText("RUC:");

        lRazon.setText("Razon Social:");

        lDireccion.setText("Direccion:");

        lAsignado.setText("Promotor asignado:");

        lDepa.setText("Departamento:");

        lProv.setText("Provincia:");

        lDistrito.setText("Distrito:");

        textRuc.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        textRuc.setEnabled(false);

        textRaz.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N

        textDirec.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N

        comboProv.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "<PROVINCIA>" }));

        comboDep.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "<DEPARTAMENTO>" }));

        comboDist.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "<DISTRITO>" }));

        inactivo.setText("Inactivo");

        textAsignado.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione una Opcion" }));

        jLabel1.setText("Contacto:");

        textContacto.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N

        jLabel3.setText("Teléfono:");

        textTelefono.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        textTelefono.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textTelefonoKeyTyped(evt);
            }
        });

        jLabel2.setText("Cargo:");

        textCargo.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N

        jLabel4.setText("Meta:");

        textMeta.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textMetaKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(52, 52, 52)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3))
                        .addGap(12, 12, 12)
                        .addComponent(textTelefono)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(jLabel2))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel4)))
                        .addGap(328, 328, 328))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(lAsignado)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(textAsignado, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(textContacto, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                                        .addComponent(lRuc)
                                        .addGap(39, 39, 39)
                                        .addComponent(textRuc))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                                        .addComponent(lDireccion)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(textDirec))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                                        .addComponent(lProv)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(comboProv, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(lRazon)
                                        .addGap(18, 18, 18)
                                        .addComponent(textRaz))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(lDepa)
                                            .addComponent(lDistrito))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(textMeta, javax.swing.GroupLayout.DEFAULT_SIZE, 151, Short.MAX_VALUE)
                                            .addComponent(comboDep, 0, 151, Short.MAX_VALUE)
                                            .addComponent(comboDist, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(textCargo)))))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(inactivo, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(122, 122, 122))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lRuc)
                    .addComponent(lRazon)
                    .addComponent(textRuc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(textRaz, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lDireccion)
                    .addComponent(lDepa)
                    .addComponent(textDirec, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboDep, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lProv)
                    .addComponent(comboProv, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lDistrito)
                    .addComponent(comboDist, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(textContacto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(textCargo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(textTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(textMeta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lAsignado)
                    .addComponent(textAsignado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                .addComponent(inactivo)
                .addContainerGap())
        );

        bGrabar.setText("Grabar");
        bGrabar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bGrabarMouseClicked(evt);
            }
        });
        bGrabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bGrabarActionPerformed(evt);
            }
        });

        bSalir.setText("Salir");
        bSalir.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bSalirMouseClicked(evt);
            }
        });
        bSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bSalirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(301, Short.MAX_VALUE)
                .addComponent(bGrabar, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(38, 38, 38)
                .addComponent(bSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bGrabar)
                    .addComponent(bSalir)))
        );

        jPanel1.setBackground(new java.awt.Color(246, 71, 71));

        titulo.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        titulo.setForeground(new java.awt.Color(255, 255, 255));
        titulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        titulo.setText("Crear Cliente");
        titulo.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                tituloMouseDragged(evt);
            }
        });
        titulo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tituloMousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(titulo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(titulo)
                .addGap(0, 6, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 536, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(19, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bSalirActionPerformed
        dispose();

    }//GEN-LAST:event_bSalirActionPerformed

    private void tituloMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tituloMouseDragged
        int x = evt.getXOnScreen();
        int y = evt.getYOnScreen();
        this.setLocation(x-xx,y-xy);
    }//GEN-LAST:event_tituloMouseDragged

    private void tituloMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tituloMousePressed
        xx=evt.getX();
        xy=evt.getY();
    }//GEN-LAST:event_tituloMousePressed

    private void bSalirMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bSalirMouseClicked
        dispose();
    }//GEN-LAST:event_bSalirMouseClicked

    private void bGrabarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bGrabarMouseClicked
        String ruc=textRuc.getText();
        String razon=textRaz.getText();
        String direc=textDirec.getText();
        String promotorAsig=(String) textAsignado.getSelectedItem();
        String depa=(String) comboDep.getSelectedItem();
        String provin=(String) comboProv.getSelectedItem();
        String dist=(String) comboDist.getSelectedItem();
        String contacto = textContacto.getText();
        String cargoContacto = textCargo.getText();
        String telefono = textTelefono.getText();
        int meta = Integer.parseInt(textMeta.getText());
        String activ;
        if(inactivo.isSelected()){
            activ="0";
        }
        else{
            activ="1";
        }
        conexionBD conectar = new conexionBD();
        conectar.conexion();
        ResultSet rs = null;
        String query = "UPDATE CLIENTES SET clRazSoc=?,clDirec=?,clContacto=?,cLCargoContacto=?,clTelefonoContacto=?,clDep=?,clProv=?,clDistr=?,clPromAsig=?,clEstad=?,clmeta=?  WHERE clRuc=?";
        if(cargoContacto.equals("")||contacto.equals("")||depa.equals("<DEPARTAMENTO>") || provin.equals("<PROVINCIA>") || dist.equals("<DISTRITO>") || promotorAsig.equals("Seleccione una Opcion") || razon.equals("")|| direc.equals("")){
            JOptionPane.showOptionDialog(null, "No se puede grabar, verifique información", "Error", JOptionPane.DEFAULT_OPTION,
             JOptionPane.ERROR_MESSAGE, null, null, null);
        }
        else{
            try{
            PreparedStatement ps = conectar.cn.prepareStatement(query);         
            ps.setString(1,razon);
            ps.setString(2,direc);
            ps.setString(6,depa);
            ps.setString(7,provin);
            ps.setString(8,dist);
            ps.setString(9,promotorAsig);
            ps.setString(10,activ);
            ps.setInt(11, meta);
            ps.setString(12,ruc);
            ps.setString(3,contacto );
            ps.setString(4,cargoContacto);
            ps.setString(5,telefono);
            int ejecucion =    ps.executeUpdate();
            int mensaje = JOptionPane.showOptionDialog(null, "Se grabo correctamente", "Grabado", JOptionPane.DEFAULT_OPTION,
             JOptionPane.INFORMATION_MESSAGE, null, null, null);
            if(mensaje==0){
                dispose();
            }
            
            }catch(SQLException e){
                System.err.println("El error" + e);
            }
        }
    }//GEN-LAST:event_bGrabarMouseClicked

    private void textMetaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textMetaKeyTyped
        char c=evt.getKeyChar();
        if(!isNumeric(String.valueOf(c))){
            getToolkit().beep();
                evt.consume();
        }
        
        if(textMeta.getText().length()==10){
            evt.consume();
        }
    }//GEN-LAST:event_textMetaKeyTyped

    private void textTelefonoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textTelefonoKeyTyped
        char c=evt.getKeyChar();
        if(!isNumeric(String.valueOf(c))){
            getToolkit().beep();
                evt.consume();
        }
        
        if(textTelefono.getText().length()==9){
            evt.consume();
        }
    }//GEN-LAST:event_textTelefonoKeyTyped

    private void bGrabarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bGrabarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_bGrabarActionPerformed
   
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(editaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(editaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(editaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(editaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                editaCliente dialog = new editaCliente(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bGrabar;
    private javax.swing.JButton bSalir;
    public javax.swing.JComboBox<String> comboDep;
    public javax.swing.JComboBox<String> comboDist;
    public javax.swing.JComboBox<String> comboProv;
    private javax.swing.JCheckBox inactivo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JLabel lAsignado;
    private javax.swing.JLabel lDepa;
    private javax.swing.JLabel lDireccion;
    private javax.swing.JLabel lDistrito;
    private javax.swing.JLabel lProv;
    private javax.swing.JLabel lRazon;
    private javax.swing.JLabel lRuc;
    private javax.swing.JComboBox<String> textAsignado;
    private javax.swing.JTextField textCargo;
    private javax.swing.JTextField textContacto;
    public javax.swing.JTextField textDirec;
    private javax.swing.JTextField textMeta;
    public static javax.swing.JTextField textRaz;
    public javax.swing.JTextField textRuc;
    private javax.swing.JTextField textTelefono;
    private javax.swing.JLabel titulo;
    // End of variables declaration//GEN-END:variables
}
