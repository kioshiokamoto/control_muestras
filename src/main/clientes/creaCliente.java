/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.clientes;

import conexionBD.conexionBD;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.border.LineBorder;
import static main.control.nuevoControl.isNumeric;

/**
 *
 * @author Okamoto
 */
public class creaCliente extends javax.swing.JDialog {
    private LineBorder border = new LineBorder(Color.RED,2);
    int xx,xy;
    conexionBD conectar = new conexionBD();
    Statement st = conectar.conexion();
    public ArrayList <String> promotores = new ArrayList<String>();
    public ArrayList <String> ruc = new ArrayList<String>();
    public creaCliente(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null); 
        this.getRootPane().setBorder(border);
        anadirCombo();
        completarCombo();
    }
    
    
    public void anadirCombo(){
        
        comboDep.addItem("Amazonas");
        comboDep.addItem("Ancash");
        comboDep.addItem("Apurimac");
        comboDep.addItem("Arequipa");
        comboDep.addItem("Ayacucho");
        comboDep.addItem("Cajamarca");
        comboDep.addItem("Callao");
        comboDep.addItem("Cusco");
        comboDep.addItem("Huancavelica");
        comboDep.addItem("Huanuco");
        comboDep.addItem("Ica");
        comboDep.addItem("Junin");
        comboDep.addItem("La Libertad");
        comboDep.addItem("Lambayeque");
        comboDep.addItem("Lima");
        comboDep.addItem("Loreto");
        comboDep.addItem("Madre de Dios");
        comboDep.addItem("Moquegua");
        comboDep.addItem("Pasco");
        comboDep.addItem("Piura");
        comboDep.addItem("Puno");
        comboDep.addItem("San Martín");
        comboDep.addItem("Tacna");
        comboDep.addItem("Tumbes");
        comboDep.addItem("Ucayali");  
        
        comboDep.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(comboDep.getSelectedItem().toString().equals("Amazonas")){
                    comboProv.removeAllItems();
                    
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Chachapoyas");
                    comboProv.addItem("Bagua");
                    comboProv.addItem("Bongara");
                    comboProv.addItem("Condorcanqui");
                    comboProv.addItem("Luya");
                    comboProv.addItem("Rodriguez de Mendoza");
                    comboProv.addItem("Utcubamba");  
                    comboProv.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if(comboProv.getSelectedItem().toString().equals("Chachapoyas")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("Asuncion");
                                comboDist.addItem("Balsas");
                                comboDist.addItem("Chachapoyas");
                                comboDist.addItem("Cheto");
                                comboDist.addItem("Chiliquin");
                                comboDist.addItem("Chuquibamba");
                                comboDist.addItem("Granada");
                                comboDist.addItem("Huancas");
                                comboDist.addItem("La Jalca");
                                comboDist.addItem("Leimebamba");
                                comboDist.addItem("Levanto");
                                comboDist.addItem("Magdalena");
                                comboDist.addItem("Mariscal Castilla");
                                comboDist.addItem("Molinopampa");
                                comboDist.addItem("Montevideo");
                                comboDist.addItem("Olleros");
                                comboDist.addItem("Quinjalca");
                                comboDist.addItem("San Francisco de Daguas");
                                comboDist.addItem("San Isidro de Maino");
                                comboDist.addItem("Soloco");
                                comboDist.addItem("Sonche");
                                
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Bagua")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("Bagua");
                                comboDist.addItem("La Peca");
                                comboDist.addItem("Aramango");
                                comboDist.addItem("Copallín");
                                comboDist.addItem("El Parco");
                                comboDist.addItem("Imaza");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Bongara")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("Jumbilla");
                                comboDist.addItem("Chisquilla");
                                comboDist.addItem("Churuja");
                                comboDist.addItem("Corosha");
                                comboDist.addItem("Cuispes");
                                comboDist.addItem("Florida");
                                comboDist.addItem("Jazan");
                                comboDist.addItem("Recta");
                                comboDist.addItem("San Carlos");
                                comboDist.addItem("Shipasbamba");
                                comboDist.addItem("Valera");
                                comboDist.addItem("Yambrasbamba");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Condorcanqui")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("El Cenepa");
                                comboDist.addItem("Nieva");
                                comboDist.addItem("Rio Santiago");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Luya")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("Camporredondo");
                                comboDist.addItem("Cocabamba");
                                comboDist.addItem("Colcamar");
                                comboDist.addItem("Conila");
                                comboDist.addItem("Inguilpata");
                                comboDist.addItem("Lamud");
                                comboDist.addItem("Longuita");
                                comboDist.addItem("Lonya Chico");
                                comboDist.addItem("Luya");
                                comboDist.addItem("Luya Viejo");
                                comboDist.addItem("Maria");
                                comboDist.addItem("Ocalli");
                                comboDist.addItem("Ocumal");
                                comboDist.addItem("Pisuquía");
                                comboDist.addItem("Providencia");
                                comboDist.addItem("San Cristobal");
                                comboDist.addItem("San Francisco del Yeso");
                                comboDist.addItem("San Jeronimo");
                                comboDist.addItem("San Juan de Lopecancha");
                                comboDist.addItem("Santa Catalina");
                                comboDist.addItem("Santo Tomas");
                                comboDist.addItem("Tingo");
                                comboDist.addItem("Trita");                                
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Rodriguez de Mendoza")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("San Nicolas");
                                comboDist.addItem("Chirimoto");
                                comboDist.addItem("Cochamal");
                                comboDist.addItem("Huambo");
                                comboDist.addItem("Limabamba");
                                comboDist.addItem("Longar");
                                comboDist.addItem("Mariscal Benavides");
                                comboDist.addItem("Milpuc");
                                comboDist.addItem("Omia");
                                comboDist.addItem("Santa Rosa");
                                comboDist.addItem("Totora");
                                comboDist.addItem("Vista Alegre");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Utcubamba")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("Bagua Grande");
                                comboDist.addItem("Cajaruro");
                                comboDist.addItem("Cumba");
                                comboDist.addItem("El Milagro");
                                comboDist.addItem("Jamalca");
                                comboDist.addItem("Lonya Grande");
                                comboDist.addItem("Yamon");
                            }                    
                            
                            
                        }
                    });
                }
                if(comboDep.getSelectedItem().toString().equals("Ancash")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Huaraz");
                    comboProv.addItem("Aija");
                    comboProv.addItem("Antonio Raimondi");
                    comboProv.addItem("Asuncion");
                    comboProv.addItem("Bolognesi");
                    comboProv.addItem("Carhuaz");
                    comboProv.addItem("Carlos Fermín Fitzcarrald");
                    comboProv.addItem("Casma");
                    comboProv.addItem("Corongo");
                    comboProv.addItem("Huari");
                    comboProv.addItem("Huarmey");
                    comboProv.addItem("Huaylas");
                    comboProv.addItem("Mariscal Luzuriaga");
                    comboProv.addItem("Ocros");
                    comboProv.addItem("Pallasca");
                    comboProv.addItem("Pomabamba");
                    comboProv.addItem("Recuay");
                    comboProv.addItem("Santa");
                    comboProv.addItem("Sihuas");
                    comboProv.addItem("Yungay");
                    comboProv.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if(comboProv.getSelectedItem().toString().equals("Huaraz")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("Huaraz");
                                comboDist.addItem("Cochabamba");
                                comboDist.addItem("Colcabamba");
                                comboDist.addItem("Huanchay");
                                comboDist.addItem("Independencia");
                                comboDist.addItem("Jangas");
                                comboDist.addItem("La Libertad");
                                comboDist.addItem("Olleros");
                                comboDist.addItem("Pampas Grande");
                                comboDist.addItem("Pariacoto");
                                comboDist.addItem("Pira");
                                comboDist.addItem("Tarica");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Aija")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("Aija");
                                comboDist.addItem("Coris");
                                comboDist.addItem("Huacllan");
                                comboDist.addItem("La Merced");
                                comboDist.addItem("Succha");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Antonio Raimondi")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("Llamellin");
                                comboDist.addItem("Aczo");
                                comboDist.addItem("Chaccho");
                                comboDist.addItem("Chingas");
                                comboDist.addItem("Mirgas");
                                comboDist.addItem("San Juan de Rontoy");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Asuncion")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("Acochaca");
                                comboDist.addItem("Chacas");
                                
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Bolognesi")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ABELARDO PARDO LEZAMETA");
                                comboDist.addItem("ANTONIO RAYMONDI");
                                comboDist.addItem("AQUIA");
                                comboDist.addItem("CAJACAY");
                                comboDist.addItem("CANIS");
                                comboDist.addItem("CHIQUIAN");
                                comboDist.addItem("COLQUIOC");
                                comboDist.addItem("HUALLANCA");
                                comboDist.addItem("HUASTA");
                                comboDist.addItem("HUAYLLACAYAN");
                                comboDist.addItem("LA PRIMAVERA");
                                comboDist.addItem("MANGAS");
                                comboDist.addItem("PACLLON");
                                comboDist.addItem("SAN MIGUEL DE CORPANQUI");
                                comboDist.addItem("TICLLOS");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Carhuaz")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ACOPAMPA");
                                comboDist.addItem("AMASHCA");
                                comboDist.addItem("ANTA");
                                comboDist.addItem("ATAQUERO");
                                comboDist.addItem("CARHUAZ");
                                comboDist.addItem("MARCARA");
                                comboDist.addItem("PARIAHUANCA");
                                comboDist.addItem("SAN MIGUEL DE ACO");
                                comboDist.addItem("SHILLA");
                                comboDist.addItem("TINCO");
                                comboDist.addItem("YUNGAR");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Carlos Fermin Fitzcarrald")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("SAN LUIS");
                                comboDist.addItem("SAN NICOLAS");
                                comboDist.addItem("YAUYA");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Casma")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("BUENA VISTA ALTA");
                                comboDist.addItem("CASMA");
                                comboDist.addItem("COMANDANTE NOEL");
                                comboDist.addItem("YAUTAN");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Corongo")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ACO");
                                comboDist.addItem("BAMBAS");
                                comboDist.addItem("CORONGO");
                                comboDist.addItem("CUSCA");
                                comboDist.addItem("LA PAMPA");
                                comboDist.addItem("YANAC");
                                comboDist.addItem("YUPÁN");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Huari")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ANRA");
                                comboDist.addItem("CAJAY");
                                comboDist.addItem("CHAVIN DE HUANTAR");
                                comboDist.addItem("HUACACHI");
                                comboDist.addItem("HUACHIS");
                                comboDist.addItem("HUANTAR");
                                comboDist.addItem("HUARI");
                                comboDist.addItem("MASIN");
                                comboDist.addItem("PAUCAS");
                                comboDist.addItem("PONTO");
                                comboDist.addItem("RAHUAPAMPA");
                                comboDist.addItem("RAPAYAN");
                                comboDist.addItem("SAN MARCOS");
                                comboDist.addItem("SAN PEDRO DE CHANA");
                                comboDist.addItem("UCO");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Huarmey")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("COCHAPETI");
                                comboDist.addItem("CULEBRAS");
                                comboDist.addItem("HUARMEY");
                                comboDist.addItem("HUAYAN");
                                comboDist.addItem("MALVAS");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Huaylas")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CARAZ");
                                comboDist.addItem("HUALLANCA");
                                comboDist.addItem("HUATA");
                                comboDist.addItem("HUAYLAS");
                                comboDist.addItem("MATO");
                                comboDist.addItem("PAMPAROMAS");
                                comboDist.addItem("PUEBLO LIBRE");
                                comboDist.addItem("SANTA CRUZ");
                                comboDist.addItem("SANTO TORIBIO");
                                comboDist.addItem("YURACMARCA");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Mariscal Luzuriaga")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CASCA");
                                comboDist.addItem("ELEAZAR GUZMAN BARRON");
                                comboDist.addItem("FIDEL OLIVAS ESCUDERO");
                                comboDist.addItem("LLAMA");
                                comboDist.addItem("LLUMPA");
                                comboDist.addItem("LUCMA");
                                comboDist.addItem("MUSGA");
                                comboDist.addItem("PISCOBAMBA");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Ocros")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ACAS");
                                comboDist.addItem("CAJAMARQUILLA");
                                comboDist.addItem("CARHUAPAMPA");
                                comboDist.addItem("COCHAS");
                                comboDist.addItem("CONGAS");
                                comboDist.addItem("LLIPA");
                                comboDist.addItem("OCROS");
                                comboDist.addItem("SAN CRISTOBAL DE RAJAN");
                                comboDist.addItem("SAN PEDRO");
                                comboDist.addItem("SANTIAGO DE CHILCAS");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Pallasca")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("BOLOGNESI");
                                comboDist.addItem("CABANA");
                                comboDist.addItem("CONCHUCOS");
                                comboDist.addItem("HUACASCHUQUE");
                                comboDist.addItem("HUANDOVAL");
                                comboDist.addItem("LACABAMBA");
                                comboDist.addItem("LLAPO");
                                comboDist.addItem("PALLASCA");
                                comboDist.addItem("PAMPAS");
                                comboDist.addItem("SANTA ROSA");
                                comboDist.addItem("TAUCA");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Pomabamba")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("HUAYLLAN");
                                comboDist.addItem("PAROBAMBA");
                                comboDist.addItem("POMABAMBA");
                                comboDist.addItem("QUINUABAMBA");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Recuay")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CATAC");
                                comboDist.addItem("COTAPARACO");
                                comboDist.addItem("HUAYLLAPAMPA");
                                comboDist.addItem("LLACLLIN");
                                comboDist.addItem("MARCA");
                                comboDist.addItem("PAMPAS CHICO");
                                comboDist.addItem("PARARIN");
                                comboDist.addItem("RECUAY");
                                comboDist.addItem("TAPACOCHA");
                                comboDist.addItem("TICAPAMPA");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Santa")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CACERES DEL PERU");
                                comboDist.addItem("CHIMBOTE");
                                comboDist.addItem("COISHCO");
                                comboDist.addItem("MACATE");
                                comboDist.addItem("MORO");
                                comboDist.addItem("NEPEÑA");
                                comboDist.addItem("NUEVO CHIMBOTE");
                                comboDist.addItem("SAMANCO");
                                comboDist.addItem("SANTA");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Sihuas")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ACOBAMBA");
                                comboDist.addItem("ALFONSO UGARTE");
                                comboDist.addItem("CASHAPAMPA");
                                comboDist.addItem("CHINGALPO");
                                comboDist.addItem("HUAYLLABAMBA");
                                comboDist.addItem("QUICHES");
                                comboDist.addItem("RAGASH");
                                comboDist.addItem("SAN JUAN");
                                comboDist.addItem("SICSIBAMBA");
                                comboDist.addItem("SIHUAS");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Yungay")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CASCAPARA");
                                comboDist.addItem("MANCOS");
                                comboDist.addItem("MATACOTO");
                                comboDist.addItem("QUILLO");
                                comboDist.addItem("RANRAHIRCA");
                                comboDist.addItem("SHUPLUY");
                                comboDist.addItem("YANAMA");
                                comboDist.addItem("YUNGAY");
                            } 
                            
                        }
                    });
                    
            
                }
                if(comboDep.getSelectedItem().toString().equals("Apurimac")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Abancay");
                    comboProv.addItem("Andahuaylas");
                    comboProv.addItem("Antabamba");
                    comboProv.addItem("Aymaraes");
                    comboProv.addItem("Cotabambas");
                    comboProv.addItem("Chincheros");
                    comboProv.addItem("Grau");
                    comboProv.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if(comboProv.getSelectedItem().toString().equals("Abancay")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ABANCAY");
                                comboDist.addItem("CHACOCHE");
                                comboDist.addItem("CIRCA");
                                comboDist.addItem("CURAHUASI");
                                comboDist.addItem("HUANIPACA");
                                comboDist.addItem("LAMBRAMA");
                                comboDist.addItem("PICHIRHUA");
                                comboDist.addItem("SAN PEDRO DE CACHORA ");
                                comboDist.addItem("TAMBURCO");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Andahuaylas")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ANDAHUAYLAS");
                                comboDist.addItem("ANDARAPA");
                                comboDist.addItem("CHIARA");
                                comboDist.addItem("HUANCARAMA");
                                comboDist.addItem("HUANCARAY");
                                comboDist.addItem("HUAYANA");
                                comboDist.addItem("JOSE MARIA ARGUEDAS");
                                comboDist.addItem("KAQUIABAMBA");
                                comboDist.addItem("KISHUARA");
                                comboDist.addItem("PACOBAMBA");
                                comboDist.addItem("PACUCHA");
                                comboDist.addItem("PAMPACHIRI");
                                comboDist.addItem("POMACOCHA");
                                comboDist.addItem("SAN ANTONIO DE CACHI");
                                comboDist.addItem("SAN JERÓNIMO");
                                comboDist.addItem("SAN MIGUEL DE CHACCRAMPA");
                                comboDist.addItem("SANTA MARÍA DE CHICMO");
                                comboDist.addItem("TALAVERA DE LA REINA");
                                comboDist.addItem("TUMAY HUARACA");
                                comboDist.addItem("TURPO");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Antabamba")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ANTABAMBA");
                                comboDist.addItem("EL ORO");
                                comboDist.addItem("HUAQUIRCA");
                                comboDist.addItem("JUAN ESPINOZA MEDRANO");
                                comboDist.addItem("OROPESA");
                                comboDist.addItem("PACHACONAS");
                                comboDist.addItem("SABAINO");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Aymaraes")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CAPAYA");
                                comboDist.addItem("CARAYBAMBA");
                                comboDist.addItem("CHALHUANCA");
                                comboDist.addItem("CHAPIMARCA");
                                comboDist.addItem("COLCABAMBA");
                                comboDist.addItem("COTARUSE");
                                comboDist.addItem("HUAYLLO");
                                comboDist.addItem("JUSTO APU SAHUARAURA");
                                comboDist.addItem("LUCRE");
                                comboDist.addItem("POCOHUANCA");
                                comboDist.addItem("SAN JUAN DE CHACÑA");
                                comboDist.addItem("SAÑAYCA");
                                comboDist.addItem("SORAYA");
                                comboDist.addItem("TAPAIRIHUA");
                                comboDist.addItem("TINTAY");
                                comboDist.addItem("TORAYA");
                                comboDist.addItem("YANACA");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Cotabambas")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CHALLHUAHUACHO");
                                comboDist.addItem("COTABAMBAS");
                                comboDist.addItem("COYLLURQUI");
                                comboDist.addItem("HAQUIRA");
                                comboDist.addItem("MARA");
                                comboDist.addItem("TAMBOBAMBA");
                                
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Chincheros")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ANCCOHUAYLLO");
                                comboDist.addItem("CHINCHEROS");
                                comboDist.addItem("COCHARCAS");
                                comboDist.addItem("HUACCANA");
                                comboDist.addItem("OCOBAMBA");
                                comboDist.addItem("ONGOY");
                                comboDist.addItem("RANRACANCHA");
                                comboDist.addItem("URANMARCA");
                                
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Grau")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CHUQUIBAMBILLA");
                                comboDist.addItem("CURASCO");
                                comboDist.addItem("CURPAHUASI");
                                comboDist.addItem("GAMARRA");
                                comboDist.addItem("HUAYLLATI");
                                comboDist.addItem("MAMARA");
                                comboDist.addItem("MICAELA BASTIDAS");
                                comboDist.addItem("PATAYPAMPA");
                                comboDist.addItem("PROGRESO");
                                comboDist.addItem("SAN ANTONIO");
                                comboDist.addItem("SANTA ROSA");
                                comboDist.addItem("TURPAY");
                                comboDist.addItem("VILCABAMBA");
                                comboDist.addItem("VIRUNDO");
                            } 
                            
                            
                        }
                    });
                    
            
                }
                if(comboDep.getSelectedItem().toString().equals("Arequipa")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Arequipa");
                    comboProv.addItem("Camana");
                    comboProv.addItem("Caravelí");
                    comboProv.addItem("Castilla");
                    comboProv.addItem("Caylloma");
                    comboProv.addItem("Condesuyos");
                    comboProv.addItem("Islay");
                    comboProv.addItem("La Union");
                    comboProv.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if(comboProv.getSelectedItem().toString().equals("Arequipa")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ALTO SELVA ALEGRE");
                                comboDist.addItem("AREQUIPA");
                                comboDist.addItem("CAYMA");
                                comboDist.addItem("CERRO COLORADO");
                                comboDist.addItem("CHARACATO");
                                comboDist.addItem("CHIGUATA");
                                comboDist.addItem("JACOBO HUNTER");
                                comboDist.addItem("JOSÉ LUIS BUSTAMANTE Y RIVERO");
                                comboDist.addItem("LA JOYA");
                                comboDist.addItem("MARIANO MELGAR");
                                comboDist.addItem("MIRAFLORES");
                                comboDist.addItem("MOLLEBAYA");
                                comboDist.addItem("PAUCARPATA");
                                comboDist.addItem("POCSI");
                                comboDist.addItem("POLOBAYA");
                                comboDist.addItem("QUEQUEÑA");
                                comboDist.addItem("SABANDÍA");
                                comboDist.addItem("SACHACA");
                                comboDist.addItem("SAN JUAN DE SIGUAS");
                                comboDist.addItem("SAN JUAN DE TARUCANI");
                                comboDist.addItem("SANTA ISABEL DE SIGUAS");
                                comboDist.addItem("SANTA RITA DE SIGUAS");
                                comboDist.addItem("SOCABAYA");
                                comboDist.addItem("TIABAYA");
                                comboDist.addItem("UCHUMAYO");
                                comboDist.addItem("VITOR");
                                comboDist.addItem("YANAHUARA");
                                comboDist.addItem("YARABAMBA");
                                comboDist.addItem("YURA");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Camana")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CAMANÁ");
                                comboDist.addItem("JOSÉ MARÍA QUIMPER");
                                comboDist.addItem("MARIANO NICOLÁS VALCÁRCEL");
                                comboDist.addItem("MARISCAL CÁCERES");
                                comboDist.addItem("NICOLÁS DE PIÉROLA");
                                comboDist.addItem("OCOÑA");
                                comboDist.addItem("QUILCA");
                                comboDist.addItem("SAMUEL PASTOR");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Caravelí")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ACARÍ");
                                comboDist.addItem("ATICO");
                                comboDist.addItem("ATIQUIPA");
                                comboDist.addItem("BELLA UNIÓN");
                                comboDist.addItem("CAHUACHO");
                                comboDist.addItem("CARAVELI");
                                comboDist.addItem("CHALA");
                                comboDist.addItem("CHAPARRA");
                                comboDist.addItem("HUANUHUANU");
                                comboDist.addItem("JAQUI");
                                comboDist.addItem("LOMAS");
                                comboDist.addItem("QUICACHA");
                                comboDist.addItem("YAUCA");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Castilla")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ANDAGUA");
                                comboDist.addItem("APLAO");
                                comboDist.addItem("AYO");
                                comboDist.addItem("CHACHAS");
                                comboDist.addItem("CHILCAYMARCA");
                                comboDist.addItem("CHOCO");
                                comboDist.addItem("HUANCARQUI");
                                comboDist.addItem("MACHAGUAY");
                                comboDist.addItem("ORCOPAMPA");
                                comboDist.addItem("PAMPACOLCA");
                                comboDist.addItem("TIPÁN");
                                comboDist.addItem("UÑÓN");
                                comboDist.addItem("URACA");
                                comboDist.addItem("VIRACO");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Caylloma")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ACHOMA");
                                comboDist.addItem("CABANACONDE");
                                comboDist.addItem("CALLALLI");
                                comboDist.addItem("CAYLLOMA");
                                comboDist.addItem("CHIVAY");
                                comboDist.addItem("COPORAQUE");
                                comboDist.addItem("HUAMBO");
                                comboDist.addItem("HUANCA");
                                comboDist.addItem("ICHUPAMPA");
                                comboDist.addItem("LARI");
                                comboDist.addItem("LLUTA");
                                comboDist.addItem("MACA");
                                comboDist.addItem("MADRIGAL");
                                comboDist.addItem("MAJES");
                                comboDist.addItem("SAN ANTONIO DE CHUCA");
                                comboDist.addItem("SIBAYO");
                                comboDist.addItem("TAPAY");
                                comboDist.addItem("TISCO");
                                comboDist.addItem("TUTI");
                                comboDist.addItem("YANQUE");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Condesuyos")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ANDARAY");
                                comboDist.addItem("CAYARANI");
                                comboDist.addItem("CHICHAS");
                                comboDist.addItem("CHUQUIBAMBA");
                                comboDist.addItem("IRAY");
                                comboDist.addItem("RÍO GRANDE");
                                comboDist.addItem("SALAMANCA");
                                comboDist.addItem("YANAQUIHUA");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Islay")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("COCACHACRA");
                                comboDist.addItem("DEÁN VALDIVIA");
                                comboDist.addItem("ISLAY");
                                comboDist.addItem("MEJÍA");
                                comboDist.addItem("MOLLENDO");
                                comboDist.addItem("PUNTA DE BOMBÓN");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("La Union")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ALCA");
                                comboDist.addItem("CHARCANA");
                                comboDist.addItem("COTAHUASI");
                                comboDist.addItem("HUAYNACOTAS");
                                comboDist.addItem("PAMPAMARCA");
                                comboDist.addItem("PUYCA");
                                comboDist.addItem("QUECHUALLA");
                                comboDist.addItem("SAYLA");
                                comboDist.addItem("TAURIA");
                                comboDist.addItem("TOMEPAMPA");
                                comboDist.addItem("TORO");
                            } 
                            
                        }
                    });
                    
            
                }
                if(comboDep.getSelectedItem().toString().equals("Ayacucho")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Huamanga");
                    comboProv.addItem("Cangallo");
                    comboProv.addItem("Huanca Sancos");
                    comboProv.addItem("Huanta");
                    comboProv.addItem("La Mar");
                    comboProv.addItem("Lucanas");
                    comboProv.addItem("Parinacochas");
                    comboProv.addItem("Paucar del Sara Sara");
                    comboProv.addItem("Sucre");
                    comboProv.addItem("Victor Fajardo");
                    comboProv.addItem("Vilcashuaman");
                    comboProv.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if(comboProv.getSelectedItem().toString().equals("Huamanga")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ACOCRO");
                                comboDist.addItem("ACOS VINCHOS");
                                comboDist.addItem("AYACUCHO");
                                comboDist.addItem("CARMEN ALTO");
                                comboDist.addItem("CHIARA");
                                comboDist.addItem("JESUS NAZARENO");
                                comboDist.addItem("OCROS");
                                comboDist.addItem("PACAYCASA");
                                comboDist.addItem("QUINUA");
                                comboDist.addItem("SAN JOSE DE TICLLAS");
                                comboDist.addItem("SAN JUAN BAUTISTA");
                                comboDist.addItem("SANTIAGO DE PISCHA");
                                comboDist.addItem("SOCOS");
                                comboDist.addItem("TAMBILLO");
                                comboDist.addItem("VINCHOS");
                            }                    
                            
                            if(comboProv.getSelectedItem().toString().equals("Cangallo")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CANGALLO");
                                comboDist.addItem("CHUSCHI");
                                comboDist.addItem("LOS MOROCHUCOS");
                                comboDist.addItem("MARÍA PARADO DE BELLIDO");
                                comboDist.addItem("PARAS");
                                comboDist.addItem("TOTOS");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Huanca Sancos")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CARAPO");
                                comboDist.addItem("SACSAMARCA");
                                comboDist.addItem("SANCOS");
                                comboDist.addItem("SANTIAGO DE LUCANAMARCA");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Huanta")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("AYAHUANCO");
                                comboDist.addItem("CANAYRE");
                                comboDist.addItem("HUAMANGUILLA");
                                comboDist.addItem("HUANTA");
                                comboDist.addItem("IGUAIN");
                                comboDist.addItem("LLOCHEGUA");
                                comboDist.addItem("LURICOCHA");
                                comboDist.addItem("SANTILLANA");
                                comboDist.addItem("SIVIA");
                                
                            } 
                            if(comboProv.getSelectedItem().toString().equals("La Mar")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ANCHIHUAY");
                                comboDist.addItem("ANCO");
                                comboDist.addItem("AYNA");
                                comboDist.addItem("CHILCAS");
                                comboDist.addItem("CHUNGUI");
                                comboDist.addItem("LUIS CARRANZA");
                                comboDist.addItem("SAMUGARI");
                                comboDist.addItem("SAN MIGUEL");
                                comboDist.addItem("SANTA ROSA");
                                comboDist.addItem("TAMBO");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Lucanas")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("AUCARA");
                                comboDist.addItem("CABANA");
                                comboDist.addItem("CARMEN SALCEDO");
                                comboDist.addItem("CHAVIÑA");
                                comboDist.addItem("CHIPAO");
                                comboDist.addItem("HUAC-HUAS");
                                comboDist.addItem("LARAMATE");
                                comboDist.addItem("LEONCIO PRADO");
                                comboDist.addItem("LLAUTA");
                                comboDist.addItem("LUCANAS");
                                comboDist.addItem("OCAÑA");
                                comboDist.addItem("OTOCA");
                                comboDist.addItem("PUQUIO");
                                comboDist.addItem("SAISA");
                                comboDist.addItem("SAN CRISTÓBAL");
                                comboDist.addItem("SAN JUAN");
                                comboDist.addItem("SAN PEDRO");
                                comboDist.addItem("SAN PEDRO DE PALCO");
                                comboDist.addItem("SANCOS");
                                comboDist.addItem("SANTA ANA DE HUAYCAHUACHO");
                                comboDist.addItem("SANTA LUCIA");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Parinacochas")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CHUMPI");
                                comboDist.addItem("CORACORA");
                                comboDist.addItem("CORONEL CASTAÑEDA");
                                comboDist.addItem("PACAPAUSA");
                                comboDist.addItem("PULLO");
                                comboDist.addItem("PUYUSCA");
                                comboDist.addItem("SAN FRANCISCO DE RAVACAYCO");
                                comboDist.addItem("UPAHUACHO");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Paucar del Sara Sara")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("COLTA");
                                comboDist.addItem("CORCULLA");
                                comboDist.addItem("LAMPA");
                                comboDist.addItem("MARCABAMBA");
                                comboDist.addItem("OYOLO");
                                comboDist.addItem("PARARCA");
                                comboDist.addItem("PAUSA");
                                comboDist.addItem("SAN JAVIER DE ALPABAMBA");
                                comboDist.addItem("SAN JOSE DE USHUA");
                                comboDist.addItem("SARA SARA");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Sucre")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("BELÉN");
                                comboDist.addItem("CHALCOS");
                                comboDist.addItem("CHILCAYOC");
                                comboDist.addItem("HUACAÑA");
                                comboDist.addItem("MORCOLLA");
                                comboDist.addItem("PAICO");
                                comboDist.addItem("QUEROBAMBA");
                                comboDist.addItem("SAN PEDRO DE LARCAY");
                                comboDist.addItem("SAN SALVADOR DE QUIJE");
                                comboDist.addItem("SANTIAGO DE PAUCARAY");
                                comboDist.addItem("SORAS");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Victor Fajardo")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ALCAMENCA");
                                comboDist.addItem("APONGO");
                                comboDist.addItem("ASQUIPATA");
                                comboDist.addItem("CANARIA");
                                comboDist.addItem("CAYARA");
                                comboDist.addItem("COLCA");
                                comboDist.addItem("HUAMANQUIQUIA");
                                comboDist.addItem("HUANCAPI");
                                comboDist.addItem("HUANCARAYLLA");
                                comboDist.addItem("HUALLA");
                                comboDist.addItem("SARHUA");
                                comboDist.addItem("VILCANCHOS");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Vilcashuaman")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ACCOMARCA");
                                comboDist.addItem("CARHUANCA");
                                comboDist.addItem("CONCEPCIÓN");
                                comboDist.addItem("HUAMBALPA");
                                comboDist.addItem("INDEPENDENCIA");
                                comboDist.addItem("SAURAMA");
                                comboDist.addItem("VILCASHUAMÁN");
                                comboDist.addItem("VISCHONGO");
                            }   
                            
                        }
                    });
                    
            
                }
                if(comboDep.getSelectedItem().toString().equals("Cajamarca")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Cajamarca");
                    comboProv.addItem("Cajabamba");
                    comboProv.addItem("Celendin");
                    comboProv.addItem("Chota");
                    comboProv.addItem("Contumaza");
                    comboProv.addItem("Cutervo");
                    comboProv.addItem("Hualgayoc");
                    comboProv.addItem("Jaen");
                    comboProv.addItem("San Ignacio");
                    comboProv.addItem("San Marcos");
                    comboProv.addItem("San Miguel");
                    comboProv.addItem("San Pablo");
                    comboProv.addItem("Santa Cruz");
                    comboProv.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if(comboProv.getSelectedItem().toString().equals("Cajamarca")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ASUNCIÓN");
                                comboDist.addItem("CAJAMARCA");
                                comboDist.addItem("CHETILLA");
                                comboDist.addItem("COSPAN");
                                comboDist.addItem("ENCAÑADA");
                                comboDist.addItem("JESÚS");
                                comboDist.addItem("LLACANORA");
                                comboDist.addItem("LOS BAÑOS DEL INCA");
                                comboDist.addItem("MAGDALENA");
                                comboDist.addItem("MATARA");
                                comboDist.addItem("NAMORA");
                                comboDist.addItem("SAN JUAN");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Cajabamba")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CACHACHI");
                                comboDist.addItem("CAJABAMBA");
                                comboDist.addItem("CONDEBAMBA");
                                comboDist.addItem("SITACOCHA");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Celendin")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CELENDÍN");
                                comboDist.addItem("CHUMUCH");
                                comboDist.addItem("CORTEGANA");
                                comboDist.addItem("HUASMIN");
                                comboDist.addItem("JORGE CHAVEZ");
                                comboDist.addItem("JOSÉ GÁLVEZ");
                                comboDist.addItem("LA LIBERTAD DE PALLÁN");
                                comboDist.addItem("MIGUEL IGLESIAS");
                                comboDist.addItem("OXAMARCA");
                                comboDist.addItem("SOROCHUCO");
                                comboDist.addItem("SUCRE");
                                comboDist.addItem("UTCO");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Chota")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ANGUIA");
                                comboDist.addItem("CHADIN");
                                comboDist.addItem("CHALAMARCA");
                                comboDist.addItem("CHIGUIRIP");
                                comboDist.addItem("CHIMBAN");
                                comboDist.addItem("CHOROPAMPA");
                                comboDist.addItem("CHOTA");
                                comboDist.addItem("COCHABAMBA");
                                comboDist.addItem("CONCHAN");
                                comboDist.addItem("HUAMBOS");
                                comboDist.addItem("LAJAS");
                                comboDist.addItem("LLAMA");
                                comboDist.addItem("MIRACOSTA");
                                comboDist.addItem("PACCHA");
                                comboDist.addItem("PION");
                                comboDist.addItem("QUEROCOTO");
                                comboDist.addItem("SAN JUAN DE LICUPIS");
                                comboDist.addItem("TACABAMBA");
                                comboDist.addItem("TOCMOCHE");
                            }
                            if(comboProv.getSelectedItem().toString().equals("Contumaza")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CHILETE");
                                comboDist.addItem("CONTUMAZÁ");
                                comboDist.addItem("CUPISNIQUE");
                                comboDist.addItem("GUZMANGO");
                                comboDist.addItem("SAN BENITO");
                                comboDist.addItem("SANTA CRUZ DE TOLED");
                                comboDist.addItem("TANTARICA");
                                comboDist.addItem("YONAN");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Cutervo")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CALLAYUC");
                                comboDist.addItem("CHOROS");
                                comboDist.addItem("CUJILLO");
                                comboDist.addItem("CUTERVO");
                                comboDist.addItem("LA RAMADA");
                                comboDist.addItem("PIMPINGOS");
                                comboDist.addItem("QUEROCOTILLO");
                                comboDist.addItem("SAN ANDRÉS DE CUTERVO");
                                comboDist.addItem("SAN JUAN DE CUTERVO");
                                comboDist.addItem("SAN LUIS DE LUCMA");
                                comboDist.addItem("SANTA CRUZ");
                                comboDist.addItem("SANTO DOMINGO DE LA CAPILLA");
                                comboDist.addItem("SANTO TOMÁS");
                                comboDist.addItem("SOCOTA");
                                comboDist.addItem("TORIBIO CASANOVA");
                            }
                            if(comboProv.getSelectedItem().toString().equals("Hualgayoc")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("BAMBAMARCA");
                                comboDist.addItem("CHUGUR");
                                comboDist.addItem("HUALGAYOC");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Jaen")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("BELLAVISTA");
                                comboDist.addItem("CHONTALÍ");
                                comboDist.addItem("COLASAY");
                                comboDist.addItem("HUABAL");
                                comboDist.addItem("JAÉN");
                                comboDist.addItem("LAS PIRIAS");
                                comboDist.addItem("POMAHUACA");
                                comboDist.addItem("PUCARÁ");
                                comboDist.addItem("SALLIQUE");
                                comboDist.addItem("SAN FELIPE");
                                comboDist.addItem("SAN JOSÉ DEL ALTO");
                                comboDist.addItem("SANTA ROSA");
                            }
                            if(comboProv.getSelectedItem().toString().equals("San Ignacio")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CHIRINOS");
                                comboDist.addItem("HUARANGO");
                                comboDist.addItem("LA COIPA");
                                comboDist.addItem("NAMBALLE");
                                comboDist.addItem("SAN IGNACIO");
                                comboDist.addItem("SAN JOSÉ DE LOURDES");
                                comboDist.addItem("TABACONAS");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("San Marcos")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CHANCAY");
                                comboDist.addItem("EDUARDO VILLANUEVA");
                                comboDist.addItem("GREGORIO PITA");
                                comboDist.addItem("ICHOCÁN");
                                comboDist.addItem("JOSÉ MANUEL QUIROZ");
                                comboDist.addItem("JOSÉ SABOGAL");
                                comboDist.addItem("PEDRO GÁLVEZ");
                            }
                            if(comboProv.getSelectedItem().toString().equals("San Miguel")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("BOLÍVAR");
                                comboDist.addItem("CALQUIS");
                                comboDist.addItem("CATILLUC");
                                comboDist.addItem("EL PRADO");
                                comboDist.addItem("LA FLORIDA");
                                comboDist.addItem("LLAPA");
                                comboDist.addItem("NANCHOC");
                                comboDist.addItem("NIEPOS");
                                comboDist.addItem("SAN GREGORIO");
                                comboDist.addItem("SAN MIGUEL");
                                comboDist.addItem("SAN SILVESTRE DE COCHÁN");
                                comboDist.addItem("TONGOD");
                                comboDist.addItem("UNIÓN AGUA BLANCA");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("San Pablo")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("SAN BERNARDINO");
                                comboDist.addItem("SAN LUIS");
                                comboDist.addItem("SAN PABLO");
                                comboDist.addItem("TUMBADEN");
                            }
                            if(comboProv.getSelectedItem().toString().equals("Santa Cruz")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ANDABAMBA");
                                comboDist.addItem("CATACHE");
                                comboDist.addItem("CHANCAYBAÑOS");
                                comboDist.addItem("LA ESPERANZA");
                                comboDist.addItem("NINABAMBA");
                                comboDist.addItem("PULÁN");
                                comboDist.addItem("SANTA CRUZ");
                                comboDist.addItem("SAUCEPAMPA");
                                comboDist.addItem("SEXI");
                                comboDist.addItem("UTICYACU");
                                comboDist.addItem("YAUYUCAN");
                            }                    
                            
                        }
                    });
                    
                }
                if(comboDep.getSelectedItem().toString().equals("Callao")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Prov. Const. del Callao");
                    comboProv.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if(comboProv.getSelectedItem().toString().equals("Prov. Const. del Callao")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("Bellavista");
                                comboDist.addItem("Callao");
                                comboDist.addItem("Carmen de la Legua Reynoso");
                                comboDist.addItem("La Perla");
                                comboDist.addItem("La Puna");
                                comboDist.addItem("Mi Perú");
                                comboDist.addItem("Ventanilla");
                            }                    
                        }
                    });
                    
            
                }
                if(comboDep.getSelectedItem().toString().equals("Cusco")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Cuzco");
                    comboProv.addItem("Acomayo");
                    comboProv.addItem("Anta");
                    comboProv.addItem("Calca");
                    comboProv.addItem("Canas");
                    comboProv.addItem("Canchis");
                    comboProv.addItem("Chumbivilcas");
                    comboProv.addItem("Espinar");
                    comboProv.addItem("La Convención");
                    comboProv.addItem("Paruro");
                    comboProv.addItem("Paucartambo");
                    comboProv.addItem("Quispicanchi");
                    comboProv.addItem("Urubamba");
                    comboProv.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if(comboProv.getSelectedItem().toString().equals("Cuzco")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CCORCA");
                                comboDist.addItem("CUSCO");
                                comboDist.addItem("POROY");
                                comboDist.addItem("SAN JERÓNIMO");
                                comboDist.addItem("SAN SEBASTIÁN");
                                comboDist.addItem("SANTIAGO");
                                comboDist.addItem("SAYLLA");
                                comboDist.addItem("WANCHAQ");
                               
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Acomayo")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ACOMAYO");
                                comboDist.addItem("ACOPIA");
                                comboDist.addItem("ACOS");
                                comboDist.addItem("MOSOC LLACTA");
                                comboDist.addItem("POMACANCHI");
                                comboDist.addItem("RONDOCAN");
                                comboDist.addItem("SANGARARÁ");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Anta")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ANCAHUASI");
                                comboDist.addItem("ANTA");
                                comboDist.addItem("CACHIMAYO");
                                comboDist.addItem("CHINCHAYPUJIO");
                                comboDist.addItem("HUAROCONDO");
                                comboDist.addItem("LIMATAMBO");
                                comboDist.addItem("MOLLEPATA");
                                comboDist.addItem("PUCYURA");
                                comboDist.addItem("ZURITE");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Calca")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CALCA");
                                comboDist.addItem("COYA");
                                comboDist.addItem("LAMAY");
                                comboDist.addItem("LARES");
                                comboDist.addItem("PISAC");
                                comboDist.addItem("SAN SALVADOR");
                                comboDist.addItem("TARAY");
                                comboDist.addItem("YANATILE");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Canas")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CHECCA");
                                comboDist.addItem("KUNTURKANKI");
                                comboDist.addItem("LANGUI");
                                comboDist.addItem("LAYO");
                                comboDist.addItem("PAMPAMARCA");
                                comboDist.addItem("QUEHUE");
                                comboDist.addItem("TUPAC AMARU");
                                comboDist.addItem("YANAOCA");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Canchis")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CHECACUPE");
                                comboDist.addItem("COMBAPATA");
                                comboDist.addItem("MARANGANI");
                                comboDist.addItem("PITUMARCA");
                                comboDist.addItem("SAN PABLO");
                                comboDist.addItem("SAN PEDRO");
                                comboDist.addItem("SICUANI");
                                comboDist.addItem("TINTA");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Chumbivilcas")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CAPACMARCA");
                                comboDist.addItem("CHAMACA");
                                comboDist.addItem("COLQUEMARCA");
                                comboDist.addItem("LIVITACA");
                                comboDist.addItem("LLUSCO");
                                comboDist.addItem("QUIÑOTA");
                                comboDist.addItem("SANTO TOMAS");
                                comboDist.addItem("VELILLE");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Espinar")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ALTO PICHIGUA");
                                comboDist.addItem("CONDOROMA");
                                comboDist.addItem("COPORAQUE");
                                comboDist.addItem("ESPINAR");
                                comboDist.addItem("OCORURO");
                                comboDist.addItem("PALLPATA");
                                comboDist.addItem("PICHIGUA");
                                comboDist.addItem("SUYCKUTAMBO");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("La Convención")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ECHARATI");
                                comboDist.addItem("HUAYOPATA");
                                comboDist.addItem("KIMBIRI");
                                comboDist.addItem("MARANURA");
                                comboDist.addItem("OCOBAMBA");
                                comboDist.addItem("PICHARI");
                                comboDist.addItem("QUELLOUNO");
                                comboDist.addItem("SANTA ANA");
                                comboDist.addItem("SANTA TERESA");
                                comboDist.addItem("VILCABAMBA");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Paruro")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ACCHA");
                                comboDist.addItem("CCAPI");
                                comboDist.addItem("COLCHA");
                                comboDist.addItem("HUANOQUITE");
                                comboDist.addItem("OMACHA");
                                comboDist.addItem("PACCARITAMBO");
                                comboDist.addItem("PARURO");
                                comboDist.addItem("PILLPINTO");
                                comboDist.addItem("YAURISQUE");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Paucartambo")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CAICAY");
                                comboDist.addItem("CHALLABAMBA");
                                comboDist.addItem("COLQUEPATA");
                                comboDist.addItem("HUANCARANI");
                                comboDist.addItem("KOSÑIPATA");
                                comboDist.addItem("PAUCARTAMBO");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Quispicanchi")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ANDAHUAYLILLAS");
                                comboDist.addItem("CAMANTI");
                                comboDist.addItem("CCARHUAYO");
                                comboDist.addItem("CCATCA");
                                comboDist.addItem("CUSIPATA");
                                comboDist.addItem("HUARO");
                                comboDist.addItem("LUCRE");
                                comboDist.addItem("MARCAPATA");
                                comboDist.addItem("OCONGATE");
                                comboDist.addItem("OROPESA");
                                comboDist.addItem("QUIQUIJANA");
                                comboDist.addItem("URCOS");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Urubamba")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CHINCHERO");
                                comboDist.addItem("HUAYLLABAMBA");
                                comboDist.addItem("MACHUPICCHU");
                                comboDist.addItem("MARAS");
                                comboDist.addItem("OLLANTAYTAMBO");
                                comboDist.addItem("URUBAMBA");
                                comboDist.addItem("YUCAY");
                            } 
                            
                        }
                    });
                    
            
            
                }
                if(comboDep.getSelectedItem().toString().equals("Huancavelica")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Huancavelica");
                    comboProv.addItem("Acobamba");
                    comboProv.addItem("Angaraes");
                    comboProv.addItem("Castrovirreyna");
                    comboProv.addItem("Churcampa");
                    comboProv.addItem("Huaytara");
                    comboProv.addItem("Tayacaja");
                    comboProv.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if(comboProv.getSelectedItem().toString().equals("Huancavelica")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ACOBAMBILLA");
                                comboDist.addItem("ACORIA");
                                comboDist.addItem("ASCENSION");
                                comboDist.addItem("CONAYCA");
                                comboDist.addItem("CUENCA");
                                comboDist.addItem("HUACHOCOLPA");
                                comboDist.addItem("HUANCAVELICA");
                                comboDist.addItem("HUANDO");
                                comboDist.addItem("HUAYLLAHUARA");
                                comboDist.addItem("IZCUCHACA");
                                comboDist.addItem("LARIA");
                                comboDist.addItem("MANTA");
                                comboDist.addItem("MARISCAL CÁCERES");
                                comboDist.addItem("MOYA");
                                comboDist.addItem("NUEVO OCCORO");
                                comboDist.addItem("PALCA");
                                comboDist.addItem("PILCHACA");
                                comboDist.addItem("VILCA");
                                comboDist.addItem("YAULI");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Acobamba")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ACOBAMBA");
                                comboDist.addItem("ANDABAMBA");
                                comboDist.addItem("ANTA");
                                comboDist.addItem("CAJA");
                                comboDist.addItem("MARCAS");
                                comboDist.addItem("PAUCARÁ");
                                comboDist.addItem("POMACOCHA");
                                comboDist.addItem("ROSARIO");
                                
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Angaraes")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ANCHONGA");
                                comboDist.addItem("CALLANMARCA");
                                comboDist.addItem("CCOCHACCASA");
                                comboDist.addItem("CHINCHO");
                                comboDist.addItem("CONGALLA");
                                comboDist.addItem("HUANCA-HUANCA");
                                comboDist.addItem("HUAYLLAY GRANDE");
                                comboDist.addItem("JULCAMARCA");
                                comboDist.addItem("LIRCAY");
                                comboDist.addItem("SAN ANTONIO DE ANTAPARCO");
                                comboDist.addItem("SANTO TOMAS DE PATA");
                                comboDist.addItem("SECCLLA");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Castrovirreyna")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ARMA");
                                comboDist.addItem("AURAHUÁ");
                                comboDist.addItem("CAPILLAS");
                                comboDist.addItem("CASTROVIRREYNA");
                                comboDist.addItem("CHUPAMARCA");
                                comboDist.addItem("COCAS");
                                comboDist.addItem("HUACHOS");
                                comboDist.addItem("HUAMATAMBO");
                                comboDist.addItem("MOLLEPAMPA");
                                comboDist.addItem("SAN JUAN");
                                comboDist.addItem("SANTA ANA");
                                comboDist.addItem("TANTARA");
                                comboDist.addItem("TICRAPO");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Churcampa")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ANCO");
                                comboDist.addItem("CHINCHIHUASI");
                                comboDist.addItem("CHURCAMPA");
                                comboDist.addItem("EL CARMEN");
                                comboDist.addItem("LA MERCED");
                                comboDist.addItem("LOCROJA");
                                comboDist.addItem("PACHAMARCA");
                                comboDist.addItem("PAUCARBAMBA");
                                comboDist.addItem("SAN MIGUEL DE MAYOCC");
                                comboDist.addItem("SAN PEDRO DE CORIS");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Huaytara")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("AYAVI");
                                comboDist.addItem("CORDOVA");
                                comboDist.addItem("HUAYACUNDO ARMA");
                                comboDist.addItem("HUAYTARA");
                                comboDist.addItem("LARAMARCA");
                                comboDist.addItem("OCOYO");
                                comboDist.addItem("PILPICHACA");
                                comboDist.addItem("QUERCO");
                                comboDist.addItem("QUITO-ARMA");
                                comboDist.addItem("SAN ANTONIO DE CUSICANCHA");
                                comboDist.addItem("SAN FRANCISCO DE SANGAYAICO");
                                comboDist.addItem("SAN ISIDRO");
                                comboDist.addItem("SANTIAGO DE CHOCORVOS");
                                comboDist.addItem("SANTIAGO DE QUIRAHUARA");
                                comboDist.addItem("SANTO DOMINGO DE CAPILLAS");
                                comboDist.addItem("TAMBO");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Tayacaja")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ACOSTAMBO");
                                comboDist.addItem("ACRAQUIA");
                                comboDist.addItem("AHUAYCHA");
                                comboDist.addItem("COLCABAMBA");
                                comboDist.addItem("DANIEL HERNÁNDEZ");
                                comboDist.addItem("HUACHOCOLPA");
                                comboDist.addItem("HUARIBAMBA");
                                comboDist.addItem("ÑAHUIMPUQUIO");
                                comboDist.addItem("PAMPAS");
                                comboDist.addItem("PAZOS");
                                comboDist.addItem("QUISHUAR");
                                comboDist.addItem("SALCABAMBA");
                                comboDist.addItem("SALCAHUASI");
                                comboDist.addItem("SAN MARCOS DE ROCCHAC");
                                comboDist.addItem("SURCUBAMBA");
                                comboDist.addItem("TINTAY PUNCU");
                            } 
                            
                        }
                    });
                    
            
                }
                if(comboDep.getSelectedItem().toString().equals("Huanuco")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Huanuco");
                    comboProv.addItem("Ambo");
                    comboProv.addItem("Dos de Mayo");
                    comboProv.addItem("Huacaybamba");
                    comboProv.addItem("Huamalies");
                    comboProv.addItem("Leoncio Prado");
                    comboProv.addItem("Marañon");
                    comboProv.addItem("Pachitea");
                    comboProv.addItem("Puerto Inca");
                    comboProv.addItem("Lauricocha");
                    comboProv.addItem("Yarowilca");
                    comboProv.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if(comboProv.getSelectedItem().toString().equals("Huanuco")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("AMARILIS");
                                comboDist.addItem("CHINCHAO");
                                comboDist.addItem("CHURUBAMBA");
                                comboDist.addItem("HUANUCO");
                                comboDist.addItem("MARGOS");
                                comboDist.addItem("PILLCO MARCA");
                                comboDist.addItem("QUISQUI");
                                comboDist.addItem("SAN FRANCISCO DE CAYRAN");
                                comboDist.addItem("SAN PEDRO DE CHAULAN");
                                comboDist.addItem("SANTA MARIA DEL VALLE");
                                comboDist.addItem("YARUMAYO");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Ambo")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("AMBO");
                                comboDist.addItem("CAYNA");
                                comboDist.addItem("COLPAS");
                                comboDist.addItem("CONCHAMARCA");
                                comboDist.addItem("HUACAR");
                                comboDist.addItem("SAN FRANCISCO");
                                comboDist.addItem("SAN RAFAEL");
                                comboDist.addItem("TOMAY KICHWA");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Dos de Mayo")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CHUQUIS");
                                comboDist.addItem("LA UNION");
                                comboDist.addItem("MARIAS");
                                comboDist.addItem("PACHAS");
                                comboDist.addItem("QUIVILLA");
                                comboDist.addItem("RIPAN");
                                comboDist.addItem("SHUNQUI");
                                comboDist.addItem("SILLAPATA");
                                comboDist.addItem("YANAS");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Huacaybamba")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CANCHABAMBA");
                                comboDist.addItem("COCHABAMBA");
                                comboDist.addItem("HUACAYBAMBA");
                                comboDist.addItem("PINRA");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Huamalies")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ARANCAY");
                                comboDist.addItem("CHAVIN DE PARIARCA");
                                comboDist.addItem("JACAS GRANDE");
                                comboDist.addItem("JIRCAN");
                                comboDist.addItem("LLATA");
                                comboDist.addItem("MIRAFLORES");
                                comboDist.addItem("MONZON");
                                comboDist.addItem("PUNCHAO");
                                comboDist.addItem("PUÑOS");
                                comboDist.addItem("SINGA");
                                comboDist.addItem("TANTAMAYO");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Leoncio Prado")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("DANIEL ALOMIAS ROBLES");
                                comboDist.addItem("HERMILIO VALDIZAN");
                                comboDist.addItem("JOSE CRESPO Y CASTILLO");
                                comboDist.addItem("LUYANDO");
                                comboDist.addItem("MARIANO DAMASO BERAUN");
                                comboDist.addItem("RUPA-RUPA");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Marañon")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CHOLON");
                                comboDist.addItem("HUACRACHUCO");
                                comboDist.addItem("SAN BUENAVENTURA");
                                comboDist.addItem("");
                                comboDist.addItem("");
                                comboDist.addItem("");
                                comboDist.addItem("");
                                comboDist.addItem("");
                                comboDist.addItem("");
                                comboDist.addItem("");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Pachitea")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CHAGLLA");
                                comboDist.addItem("MOLINO");
                                comboDist.addItem("PANAO");
                                comboDist.addItem("UMARI");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Puerto Inca")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CODO DEL POZUZO");
                                comboDist.addItem("HONORIA");
                                comboDist.addItem("PUERTO INCA");
                                comboDist.addItem("TOURNAVISTA");
                                comboDist.addItem("YUYAPICHIS");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Lauricocha")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("BAÑOS");
                                comboDist.addItem("JESUS");
                                comboDist.addItem("JIVIA");
                                comboDist.addItem("QUEROPALCA");
                                comboDist.addItem("RONDOS");
                                comboDist.addItem("SAN FRANCISCO DE ASIS");
                                comboDist.addItem("SAN MIGUEL DE CAURI");
                                
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Yarowilca")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("APARICIO POMARES");
                                comboDist.addItem("CAHUAC");
                                comboDist.addItem("CHACABAMBA");
                                comboDist.addItem("CHAVINILLO");
                                comboDist.addItem("CHORAS");
                                comboDist.addItem("JACAS CHICO");
                                comboDist.addItem("OBAS");
                                comboDist.addItem("PAMPAMARCA");
                                
                            } 
                            
                        }
                    });
                    
            
                }
                if(comboDep.getSelectedItem().toString().equals("Ica")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Ica");
                    comboProv.addItem("Chincha");
                    comboProv.addItem("Nazca");
                    comboProv.addItem("Palpa");
                    comboProv.addItem("Pisco");
                    comboProv.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if(comboProv.getSelectedItem().toString().equals("Ica")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ICA");
                                comboDist.addItem("LA TINGUIÑA");
                                comboDist.addItem("LOS AQUIJES");
                                comboDist.addItem("OCUCAJE");
                                comboDist.addItem("PACHACUTEC");
                                comboDist.addItem("PARCONA");
                                comboDist.addItem("PUEBLO NUEVO");
                                comboDist.addItem("SALAS");
                                comboDist.addItem("SAN JOSE DE LOS MOLINOS");
                                comboDist.addItem("SAN JUAN BAUTISTA");
                                comboDist.addItem("SANTIAGO");
                                comboDist.addItem("SUBTANJALLA");
                                comboDist.addItem("TATE");
                                comboDist.addItem("YAUCA DEL ROSARIO");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Chincha")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ALTO LARAN");
                                comboDist.addItem("CHAVIN");
                                comboDist.addItem("CHINCHA ALTA");
                                comboDist.addItem("CHINCHA BAJA");
                                comboDist.addItem("EL CARMEN");
                                comboDist.addItem("GROCIO PRADO");
                                comboDist.addItem("PUEBLO NUEVO");
                                comboDist.addItem("SAN JUAN DE YANAC");
                                comboDist.addItem("SAN PEDRO DE HUACARPANA");
                                comboDist.addItem("SUNAMPE");
                                comboDist.addItem("TAMBO DE MORA");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Nazca")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CHANGUILLO");
                                comboDist.addItem("EL INGENIO");
                                comboDist.addItem("MARCONA");
                                comboDist.addItem("NAZCA");
                                comboDist.addItem("VISTA ALEGRE");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Palpa")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("LLIPATA");
                                comboDist.addItem("PALPA");
                                comboDist.addItem("RIO GRANDE");
                                comboDist.addItem("SANTA CRUZ");
                                comboDist.addItem("TIBILLO");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Pisco")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("HUANCANO");
                                comboDist.addItem("HUMAY");
                                comboDist.addItem("INDEPENDENCIA");
                                comboDist.addItem("PARACAS");
                                comboDist.addItem("PISCO");
                                comboDist.addItem("SAN ANDRES");
                                comboDist.addItem("SAN CLEMENTE");
                                comboDist.addItem("TUPAC AMARU INCA");
                            } 
                            
                        }
                    });
                   
            
                }
                if(comboDep.getSelectedItem().toString().equals("Junin")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Huancayo");
                    comboProv.addItem("Chanchamayo");
                    comboProv.addItem("Chupaca");
                    comboProv.addItem("Concepcion");
                    comboProv.addItem("Jauja");
                    comboProv.addItem("Junin");
                    comboProv.addItem("Satipo");
                    comboProv.addItem("Tarma");
                    comboProv.addItem("Yauli");
                    comboProv.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if(comboProv.getSelectedItem().toString().equals("Huancayo")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CARHUACALLANGA");
                                comboDist.addItem("CHACAPAMPA");
                                comboDist.addItem("CHICCHE");
                                comboDist.addItem("CHILCA");
                                comboDist.addItem("CHONGOS ALTO");
                                comboDist.addItem("CHUPURO");
                                comboDist.addItem("COLCA");
                                comboDist.addItem("CULLHUAS");
                                comboDist.addItem("EL TAMBO");
                                comboDist.addItem("HUACRAPUQUIO");
                                comboDist.addItem("HUALHUAS");
                                comboDist.addItem("HUANCAN");
                                comboDist.addItem("HUANCAYO");
                                comboDist.addItem("HUASICANCHA");
                                comboDist.addItem("HUAYUCACHI");
                                comboDist.addItem("INGENIO");
                                comboDist.addItem("PARIAHUANCA");
                                comboDist.addItem("PILCOMAYO");
                                comboDist.addItem("PUCARA");
                                comboDist.addItem("QUICHUAY");
                                comboDist.addItem("QUILCAS");
                                comboDist.addItem("SAN AGUSTÍN");
                                comboDist.addItem("SAN JERÓNIMO DE TUNÁN");
                                comboDist.addItem("SAÑO");
                                comboDist.addItem("SANTO DOMINGO DE ACOBAMBA");
                                comboDist.addItem("SAPALLANGA");
                                comboDist.addItem("SICAYA");
                                comboDist.addItem("VIQUES");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Chanchamayo")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CHANCHAMAYO");
                                comboDist.addItem("PERENÉ");
                                comboDist.addItem("PICHANAQUI");
                                comboDist.addItem("SAN LUIS DE SHUARO");
                                comboDist.addItem("SAN RAMÓN");
                                comboDist.addItem("VITOC");
                            } if(comboProv.getSelectedItem().toString().equals("Chupaca")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("AHUAC");
                                comboDist.addItem("CHONGOS BAJO");
                                comboDist.addItem("CHUPACA");
                                comboDist.addItem("HUÁCHAC");
                                comboDist.addItem("HUAMANCACA CHICO");
                                comboDist.addItem("SAN JUAN DE JARPA");
                                comboDist.addItem("SAN JUAN DE YSCOS");
                                comboDist.addItem("TRES DE DICIEMBRE");
                                comboDist.addItem("YANACANCHA");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Concepcion")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ACO");
                                comboDist.addItem("ANDAMARCA");
                                comboDist.addItem("CHAMBARA");
                                comboDist.addItem("COCHAS");
                                comboDist.addItem("COMAS");
                                comboDist.addItem("CONCEPCIÓN");
                                comboDist.addItem("HEROÍNAS TOLEDO");
                                comboDist.addItem("MANZANARES");
                                comboDist.addItem("MARISCAL CASTILLA");
                                comboDist.addItem("MATAHUASI");
                                comboDist.addItem("MITO");
                                comboDist.addItem("NUEVE DE JULIO");
                                comboDist.addItem("ORCOTUNA");
                                comboDist.addItem("SAN JOSE DE QUERO");
                                comboDist.addItem("SANTA ROSA DE OCOPA");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Jauja")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ACOLLA");
                                comboDist.addItem("APATA");
                                comboDist.addItem("ATAURA");
                                comboDist.addItem("CANCHAYLLO");
                                comboDist.addItem("CURICACA");
                                comboDist.addItem("EL MANTARO");
                                comboDist.addItem("HUAMALI");
                                comboDist.addItem("HUARIPAMPA");
                                comboDist.addItem("HUERTAS");
                                comboDist.addItem("JANJAILLO");
                                comboDist.addItem("JAUJA");
                                comboDist.addItem("JULCAN");
                                comboDist.addItem("LEONOR ORDOÑEZ");
                                comboDist.addItem("LLOCLLAPAMPA");
                                comboDist.addItem("MARCO");
                                comboDist.addItem("MASMA");
                                comboDist.addItem("MASMA CHICCHE");
                                comboDist.addItem("MOLINOS");
                                comboDist.addItem("MONOBAMBA");
                                comboDist.addItem("MUQUI");
                                comboDist.addItem("MUQUIYAUYO");
                                comboDist.addItem("PACA");
                                comboDist.addItem("PACCHA");
                                comboDist.addItem("PANCAN");
                                comboDist.addItem("PARCO");
                                comboDist.addItem("POMACANCHA");
                                comboDist.addItem("RICRAN");
                                comboDist.addItem("SAN LORENZO");
                                comboDist.addItem("SAN PEDRO DE CHUNAN");
                                comboDist.addItem("SAUSA");
                                comboDist.addItem("SINCOS");
                                comboDist.addItem("TUANAN MARCA");
                                comboDist.addItem("YAULI");
                                comboDist.addItem("YAUYOS");
                            } if(comboProv.getSelectedItem().toString().equals("Junin")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CARHUAMAYO");
                                comboDist.addItem("JUNÍN");
                                comboDist.addItem("ONDORES");
                                comboDist.addItem("ULCUMAYO");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Satipo")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("COVIRIALI");
                                comboDist.addItem("LLAYLLA");
                                comboDist.addItem("MAZAMARI");
                                comboDist.addItem("PAMPA HERMOSA");
                                comboDist.addItem("PANGOA");
                                comboDist.addItem("RÍO NEGRO");
                                comboDist.addItem("RÍO TAMBO");
                                comboDist.addItem("SATIPO");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Tarma")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ACOBAMBA");
                                comboDist.addItem("HUARICOLCA");
                                comboDist.addItem("HUASAHUASI");
                                comboDist.addItem("LA UNION");
                                comboDist.addItem("PALCA");
                                comboDist.addItem("PALCAMAYO");
                                comboDist.addItem("SAN PEDRO DE CAJAS");
                                comboDist.addItem("TAPO");
                                comboDist.addItem("TARMA");
                            } if(comboProv.getSelectedItem().toString().equals("Yauli")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CHACAPALPA");
                                comboDist.addItem("HUAY-HUAY");
                                comboDist.addItem("LA OROYA");
                                comboDist.addItem("MARCAPOMACOCHA");
                                comboDist.addItem("MOROCOCHA");
                                comboDist.addItem("PACCHA");
                                comboDist.addItem("SANTA BARBARA DE CARHUACAYAN");
                                comboDist.addItem("SANTA ROSA DE SACCO");
                                comboDist.addItem("SUITUCANCHA");
                                comboDist.addItem("YAULI");
                            } 
                            
                        }
                    });
                }
                if(comboDep.getSelectedItem().toString().equals("La Libertad")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Trujillo");
                    comboProv.addItem("Ascope");
                    comboProv.addItem("Bolivar");
                    comboProv.addItem("Chepen");
                    comboProv.addItem("Gran Chimu");
                    comboProv.addItem("Julcan");
                    comboProv.addItem("Otuzco");
                    comboProv.addItem("Pacasmayo");
                    comboProv.addItem("Pataz");
                    comboProv.addItem("Sanchez Carrion");
                    comboProv.addItem("Santiago de Chuco");
                    comboProv.addItem("Viru");
                    comboProv.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if(comboProv.getSelectedItem().toString().equals("Trujillo")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("EL PORVENIR");
                                comboDist.addItem("FLORENCIA DE MORA");
                                comboDist.addItem("HUANCHACO");
                                comboDist.addItem("LA ESPERANZA");
                                comboDist.addItem("LAREDO");
                                comboDist.addItem("MOCHE");
                                comboDist.addItem("POROTO");
                                comboDist.addItem("SALAVERRY");
                                comboDist.addItem("SIMBAL");
                                comboDist.addItem("TRUJILLO");
                                comboDist.addItem("VÍCTOR LARCO HERRERA");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Ascope")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ASCOPE");
                                comboDist.addItem("CASA GRANDE");
                                comboDist.addItem("CHICAMA");
                                comboDist.addItem("CHOCOPE");
                                comboDist.addItem("MAGDALENA DE CAO");
                                comboDist.addItem("PAIJÁN");
                                comboDist.addItem("RÁZURI");
                                comboDist.addItem("SANTIAGO DE CAO");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Bolivar")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("BAMBAMARCA");
                                comboDist.addItem("BOLÍVAR");
                                comboDist.addItem("CONDORMARCA");
                                comboDist.addItem("LONGOTEA");
                                comboDist.addItem("UCHUMARCA");
                                comboDist.addItem("UCUNCHA");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Chepen")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CHEPÉN");
                                comboDist.addItem("PACANGA");
                                comboDist.addItem("PUEBLO NUEVO");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Gran Chimu")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CASCAS");
                                comboDist.addItem("LUCMA");
                                comboDist.addItem("MARMOT");
                                comboDist.addItem("SAYAPULLO");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Julcan")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CALAMARCA");
                                comboDist.addItem("CARABAMBA");
                                comboDist.addItem("HUASO");
                                comboDist.addItem("JULCÁN");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Otuzco")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("AGALLPAMPA");
                                comboDist.addItem("CHARAT");
                                comboDist.addItem("HUARANCHAL");
                                comboDist.addItem("LA CUESTA");
                                comboDist.addItem("MACHE");
                                comboDist.addItem("OTUZCO");
                                comboDist.addItem("PARANDAY");
                                comboDist.addItem("SALPO");
                                comboDist.addItem("SINSICAP");
                                comboDist.addItem("USQUIL");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Pacasmayo")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("GUADALUPE");
                                comboDist.addItem("JEQUETEPEQUE");
                                comboDist.addItem("PACASMAYO");
                                comboDist.addItem("SAN JOSÉ");
                                comboDist.addItem("SAN PEDRO DE LLOC");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Pataz")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("BULDIBUYO");
                                comboDist.addItem("CHILLIA");
                                comboDist.addItem("HUANCASPATA");
                                comboDist.addItem("HUAYLILLAS");
                                comboDist.addItem("HUAYO");
                                comboDist.addItem("ONGÓN");
                                comboDist.addItem("PARCOY");
                                comboDist.addItem("PATAZ");
                                comboDist.addItem("PIAS");
                                comboDist.addItem("SANTIAGO DE CHALLAS");
                                comboDist.addItem("TAURIJA");
                                comboDist.addItem("TAYABAMBA");
                                comboDist.addItem("URPAY");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Sanchez Carrion")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CHUGAY");
                                comboDist.addItem("COCHORCO");
                                comboDist.addItem("CURGOS");
                                comboDist.addItem("HUAMACHUCO");
                                comboDist.addItem("MARCABAL");
                                comboDist.addItem("SANAGORÁN");
                                comboDist.addItem("SARÍN");
                                comboDist.addItem("SARTIMBAMBA");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Santiago de Chuco")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ANGASMARCA");
                                comboDist.addItem("CACHICADÁN");
                                comboDist.addItem("MOLLEBAMBA");
                                comboDist.addItem("MOLLEPATA");
                                comboDist.addItem("QUIRUVILCA");
                                comboDist.addItem("SANTA CRUZ DE CHUCA");
                                comboDist.addItem("SANTIAGO DE CHUCO");
                                comboDist.addItem("SITABAMBA");
                            }
                            if(comboProv.getSelectedItem().toString().equals("Viru")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CHAO");
                                comboDist.addItem("GUADALUPITO");
                                comboDist.addItem("VIRÚ");
                            }
                        }
                    });
                    
            
                }
                if(comboDep.getSelectedItem().toString().equals("Lambayeque")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Chiclayo");
                    comboProv.addItem("Ferreñafe");
                    comboProv.addItem("Lambayeque");
                    comboProv.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if(comboProv.getSelectedItem().toString().equals("Chiclayo")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("Chiclayo");
                                comboDist.addItem("Cayaltí");
                                comboDist.addItem("Chongoyape");
                                comboDist.addItem("Eten");
                                comboDist.addItem("Puerto Eten");
                                comboDist.addItem("José Leonardo Ortiz");
                                comboDist.addItem("La Victoria");
                                comboDist.addItem("Lagunas");
                                comboDist.addItem("Monsefú");
                                comboDist.addItem("Nueva Arica");
                                comboDist.addItem("Oyotún");
                                comboDist.addItem("Pátapo");
                                comboDist.addItem("Picsi");
                                comboDist.addItem("Pimentel");
                                comboDist.addItem("Pomalca");
                                comboDist.addItem("Pucalá");
                                comboDist.addItem("Reque");
                                comboDist.addItem("Santa Rosa");
                                comboDist.addItem("Tumán");
                                comboDist.addItem("Zaña");
                            }
                            if(comboProv.getSelectedItem().toString().equals("Ferreñafe")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("Ferreñafe");
                                comboDist.addItem("Cañaris");
                                comboDist.addItem("Incahuasi");
                                comboDist.addItem("Manuel Antonio Mesones Muro");
                                comboDist.addItem("Pítipo");
                                comboDist.addItem("Pueblo Nuevo");
                            }
                            if(comboProv.getSelectedItem().toString().equals("Lambayeque")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("Lambayeque");
                                comboDist.addItem("Chóchope");
                                comboDist.addItem("Íllimo");
                                comboDist.addItem("Jayanca");
                                comboDist.addItem("Mochumí");
                                comboDist.addItem("Mórrope");
                                comboDist.addItem("Motupe");
                                comboDist.addItem("Olmos");
                                comboDist.addItem("Pacora");
                                comboDist.addItem("Salas");
                                comboDist.addItem("San José");
                                comboDist.addItem("Túcume");
                            }
                        }
                    });
                    
            
                }
                if(comboDep.getSelectedItem().toString().equals("Lima")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Lima");
                    comboProv.addItem("Barranca");
                    comboProv.addItem("Cajatambo");
                    comboProv.addItem("Canta");
                    comboProv.addItem("Cañete");
                    comboProv.addItem("Huaral");
                    comboProv.addItem("Huarochiri");
                    comboProv.addItem("Huaura");
                    comboProv.addItem("Oyon");
                    comboProv.addItem("Yauyos");
                    comboProv.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if(comboProv.getSelectedItem().toString().equals("Lima")){
                            comboDist.removeAllItems();
                            comboDist.addItem("<DISTRITO>");
                            comboDist.addItem("Ancón");
                            comboDist.addItem("Ate Vitarte");
                            comboDist.addItem("Barranco");
                            comboDist.addItem("Breña");
                            comboDist.addItem("Carabayllo");
                            comboDist.addItem("Chaclacayo");
                            comboDist.addItem("Chorrillos");
                            comboDist.addItem("Cieneguilla");
                            comboDist.addItem("Comas");
                            comboDist.addItem("El Agustino");
                            comboDist.addItem("Independencia");
                            comboDist.addItem("Jesús María");
                            comboDist.addItem("La Molina");
                            comboDist.addItem("La Victoria");
                            comboDist.addItem("Lima");
                            comboDist.addItem("Lince");
                            comboDist.addItem("Los Olivos");
                            comboDist.addItem("Lurigancho");
                            comboDist.addItem("Lurín");
                            comboDist.addItem("Magdalena del Mar");
                            comboDist.addItem("Miraflores");
                            comboDist.addItem("Pachacamac");
                            comboDist.addItem("Pucusana");
                            comboDist.addItem("Pueblo Libre");
                            comboDist.addItem("Puente Piedra");
                            comboDist.addItem("Punta Hermosa");
                            comboDist.addItem("Punta Negra");
                            comboDist.addItem("Rímac");
                            comboDist.addItem("San Bartolo");
                            comboDist.addItem("San Borja");
                            comboDist.addItem("San Isidro");
                            comboDist.addItem("San Juan de Lurigancho");
                            comboDist.addItem("San Juan de Miraflores");
                            comboDist.addItem("San Luis");
                            comboDist.addItem("San Martín de Porres");
                            comboDist.addItem("San Miguel");
                            comboDist.addItem("Santa Anita");
                            comboDist.addItem("Santa María del Mar");
                            comboDist.addItem("Santa Rosa");
                            comboDist.addItem("Santiago de Surco");
                            comboDist.addItem("Surquillo");
                            comboDist.addItem("Villa El Salvador");
                            comboDist.addItem("Villa María del Triunfo");
                        }
                        if(comboProv.getSelectedItem().toString().equals("Barranca")){
                            comboDist.removeAllItems();
                            comboDist.addItem("<DISTRITO>");
                            comboDist.addItem("Barranca");
                            comboDist.addItem("Paramonga");
                            comboDist.addItem("Pativilca");
                            comboDist.addItem("Supe");
                            comboDist.addItem("Supe Puerto");
                        }
                        if(comboProv.getSelectedItem().toString().equals("Cajatambo")){
                            comboDist.removeAllItems();
                            comboDist.addItem("<DISTRITO>");
                            comboDist.addItem("Manás");
                            comboDist.addItem("Gorgor");
                            comboDist.addItem("Huancapón");
                            comboDist.addItem("Cajatambo");
                            comboDist.addItem("Copa");
                        }
                        if(comboProv.getSelectedItem().toString().equals("Canta")){
                            comboDist.removeAllItems();
                            comboDist.addItem("<DISTRITO>");
                            comboDist.addItem("Canta");
                            comboDist.addItem("Arahuay");
                            comboDist.addItem("Huamantanga");
                            comboDist.addItem("Huaros");
                            comboDist.addItem("Lachaqui");
                            comboDist.addItem("San Buenaventura");
                            comboDist.addItem("Santa Rosa de Quives");
                        }
                        if(comboProv.getSelectedItem().toString().equals("Cañete")){
                            comboDist.removeAllItems();
                            comboDist.addItem("<DISTRITO>");
                            comboDist.addItem("Asia");
                            comboDist.addItem("Calango");
                            comboDist.addItem("Cerro Azul");
                            comboDist.addItem("Chilca");
                            comboDist.addItem("Coayllo");
                            comboDist.addItem("Imperial");
                            comboDist.addItem("Lunahuaná");
                            comboDist.addItem("Mala");
                            comboDist.addItem("Nuevo Imperial");
                            comboDist.addItem("Pacarán");
                            comboDist.addItem("Quilmaná");
                            comboDist.addItem("San Antonio");
                            comboDist.addItem("San Luis");
                            comboDist.addItem("San Vicente de Cañete");
                            comboDist.addItem("Santa Cruz de Flores");
                            comboDist.addItem("Zúñiga");
                        }
                        if(comboProv.getSelectedItem().toString().equals("Huaral")){
                            comboDist.removeAllItems();
                            comboDist.addItem("<DISTRITO>");
                            comboDist.addItem("Atavillos Alto");
                            comboDist.addItem("Atavillos Bajo");
                            comboDist.addItem("Aucallama");
                            comboDist.addItem("Chancay");
                            comboDist.addItem("Huaral");
                            comboDist.addItem("Ihuarí");
                            comboDist.addItem("Lampían");
                            comboDist.addItem("Pacaraos");
                            comboDist.addItem("Santa Cruz de Andamarca");
                            comboDist.addItem("Sumbilca");
                            comboDist.addItem("San Miguel de Acos");
                            comboDist.addItem("Veintisiete de Noviembre");
                            
                        }
                        if(comboProv.getSelectedItem().toString().equals("Huarochiri")){
                            comboDist.removeAllItems();
                            comboDist.addItem("<DISTRITO>");
                            comboDist.addItem("Matucana");
                            comboDist.addItem("Antioquía");
                            comboDist.addItem("Callahuanca");
                            comboDist.addItem("Carampoma");
                            comboDist.addItem("Chicla");
                            comboDist.addItem("Cuenca");
                            comboDist.addItem("Huachupampa");
                            comboDist.addItem("Huanza");
                            comboDist.addItem("Huarochirí");
                            comboDist.addItem("Lahuaytambo");
                            comboDist.addItem("Langa");
                            comboDist.addItem("Laraos");
                            comboDist.addItem("Mariatana");
                            comboDist.addItem("Ricardo Palma");
                            comboDist.addItem("San Andrés de Tupicocha");
                            comboDist.addItem("San Antonio");
                            comboDist.addItem("San Bartolomé");
                            comboDist.addItem("San Damián");
                            comboDist.addItem("San Juan de Iris");
                            comboDist.addItem("San Juan de Tantaranche");
                            comboDist.addItem("San Lorenzo de Quinti");
                            comboDist.addItem("San Mateo");
                            comboDist.addItem("San Mateo de Otao");
                            comboDist.addItem("San Pedro de Casta");
                            comboDist.addItem("San Pedro de Huancayre");
                            comboDist.addItem("Sangallaya");
                            comboDist.addItem("Santa Cruz de Cocachacra");
                            comboDist.addItem("Santa Eulalia");
                            comboDist.addItem("Santiago de Anchucaya");
                            comboDist.addItem("Santiago de Tuna");
                            comboDist.addItem("Santo Domingo de los Olleros");
                            comboDist.addItem("San Jerónimo de Surco");
                        }
                        if(comboProv.getSelectedItem().toString().equals("Huaura")){
                            comboDist.removeAllItems();
                            comboDist.addItem("<DISTRITO>");
                            comboDist.addItem("Ámbar");
                            comboDist.addItem("Caleta de Carquín");
                            comboDist.addItem("Checras");
                            comboDist.addItem("Huacho");
                            comboDist.addItem("Hualmay");
                            comboDist.addItem("Huaura");
                            comboDist.addItem("Leoncio Prado");
                            comboDist.addItem("Paccho");
                            comboDist.addItem("Santa Leonor");
                            comboDist.addItem("Santa María");
                            comboDist.addItem("Sayán");
                            comboDist.addItem("Végueta");
                        }
                        if(comboProv.getSelectedItem().toString().equals("Oyon")){
                            comboDist.removeAllItems();
                            comboDist.addItem("<DISTRITO>");
                            comboDist.addItem("Oyón");
                            comboDist.addItem("Andajes");
                            comboDist.addItem("Caujul");
                            comboDist.addItem("Cochamarca");
                            comboDist.addItem("Naván");
                            comboDist.addItem("Pachangara");
                        }
                        if(comboProv.getSelectedItem().toString().equals("Yauyos")){
                            comboDist.removeAllItems();
                            comboDist.addItem("<DISTRITO>");
                            comboDist.addItem("Yauyos");
                            comboDist.addItem("Alis");
                            comboDist.addItem("Ayauca");
                            comboDist.addItem("Ayavirí");
                            comboDist.addItem("Azángaro");
                            comboDist.addItem("Cacra");
                            comboDist.addItem("Carania");
                            comboDist.addItem("Catahuasi");
                            comboDist.addItem("Chocos");
                            comboDist.addItem("Cochas");
                            comboDist.addItem("Colonia");
                            comboDist.addItem("Hongos");
                            comboDist.addItem("Huampará");
                            comboDist.addItem("Huancaya");
                            comboDist.addItem("Huangáscar");
                            comboDist.addItem("Huantán");
                            comboDist.addItem("Huáñec");
                            comboDist.addItem("Laraos");
                            comboDist.addItem("Lincha");
                            comboDist.addItem("Madeán");
                            comboDist.addItem("Miraflores");
                            comboDist.addItem("Omas");
                            comboDist.addItem("Putinza");
                            comboDist.addItem("Quinches");
                            comboDist.addItem("Quinocay");
                            comboDist.addItem("San Joaquín");
                            comboDist.addItem("San Pedro de Pilas");
                            comboDist.addItem("Tanta");
                            comboDist.addItem("Tauripampa");
                            comboDist.addItem("Tomas");
                            comboDist.addItem("Tupe");
                            comboDist.addItem("Víñac");
                            comboDist.addItem("Vitis");
                        }
                    }
                });
            
                }
                if(comboDep.getSelectedItem().toString().equals("Loreto")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Maynas");
                    comboProv.addItem("Alto Amazonas");
                    comboProv.addItem("Datem del Marañon");
                    comboProv.addItem("Loreto");
                    comboProv.addItem("Mariscal Ramon Castilla");
                    comboProv.addItem("Putumayo");
                    comboProv.addItem("Requena");
                    comboProv.addItem("Ucayali");
                    comboProv.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if(comboProv.getSelectedItem().toString().equals("Maynas")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ALTO NANAY");
                                comboDist.addItem("BELÉN");
                                comboDist.addItem("FERNANDO LORES");
                                comboDist.addItem("INDIANA");
                                comboDist.addItem("IQUITOS");
                                comboDist.addItem("LAS AMAZONAS");
                                comboDist.addItem("MAZÁN");
                                comboDist.addItem("NAPO");
                                comboDist.addItem("PUNCHANA");
                                comboDist.addItem("SAN JUAN BAUTISTA");
                                comboDist.addItem("TORRES CAUSANA");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Alto Amazonas")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("BALSAPUERTO");
                                comboDist.addItem("JEBEROS");
                                comboDist.addItem("LAGUNAS");
                                comboDist.addItem("SANTA CRUZ");
                                comboDist.addItem("TENIENTE CÉSAR LÓPEZ ROJAS");
                                comboDist.addItem("YURIMAGUAS");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Datem del Marañon")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ANDOAS");
                                comboDist.addItem("BARRANCA");
                                comboDist.addItem("CAHUAPANAS");
                                comboDist.addItem("MANSERICHE");
                                comboDist.addItem("MORONA");
                                comboDist.addItem("PASTAZA");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Loreto")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("NAUTA");
                                comboDist.addItem("PARINARI");
                                comboDist.addItem("TIGRE");
                                comboDist.addItem("TROMPETEROS");
                                comboDist.addItem("URARINAS");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Mariscal Ramon Castilla")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("PEBAS");
                                comboDist.addItem("RAMÓN CASTILLA");
                                comboDist.addItem("SAN PABLO");
                                comboDist.addItem("YAVARÍ");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Putumayo")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("PUTUMAYO");
                                comboDist.addItem("ROSA PANDURO");
                                comboDist.addItem("TENIENTE MANUEL CLAVERO");
                                comboDist.addItem("YAGUAS");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Requena")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ALTO TAPICHE");
                                comboDist.addItem("CAPELO");
                                comboDist.addItem("EMILIO SAN MARTÍN");
                                comboDist.addItem("JENARO HERRERA");
                                comboDist.addItem("MAQUÍA");
                                comboDist.addItem("PUINAHUA");
                                comboDist.addItem("REQUENA");
                                comboDist.addItem("SAQUENA");
                                comboDist.addItem("SOPLIN");
                                comboDist.addItem("TAPICHE");
                                comboDist.addItem("YAQUERANA");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Ucayali")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CONTAMANA");
                                comboDist.addItem("INAHUAYA");
                                comboDist.addItem("PADRE MÁRQUEZ");
                                comboDist.addItem("PAMPA HERMOSA");
                                comboDist.addItem("SARAYACU");
                                comboDist.addItem("VARGAS GUERRA");
                            } 
                            
                        }
                    });
                    
            
                }
                if(comboDep.getSelectedItem().toString().equals("Madre de Dios")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Tambopata");
                    comboProv.addItem("Manu");
                    comboProv.addItem("Tahuamanu");
                    comboProv.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if(comboProv.getSelectedItem().toString().equals("Tambopata")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("Inambari");
                                comboDist.addItem("Laberinto");
                                comboDist.addItem("Las Piedras");
                                comboDist.addItem("Tambopata");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Manu")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("Fitzcarrald");
                                comboDist.addItem("Huepetuhe");
                                comboDist.addItem("Madre de Dios");
                                comboDist.addItem("Manu");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Tahuamanu")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("Iberia");
                                comboDist.addItem("Iñapari");
                                comboDist.addItem("Tahuamanu");
                            } 
                            
                        }
                    });
                    
            
                }
                if(comboDep.getSelectedItem().toString().equals("Moquegua")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Mariscal Nieto");
                    comboProv.addItem("General Sanchez Cerro");
                    comboProv.addItem("Ilo");
                    comboProv.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if(comboProv.getSelectedItem().toString().equals("Mariscal Nieto")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CARUMAS");
                                comboDist.addItem("CUCHUMBAYA");
                                comboDist.addItem("MOQUEGUA ");
                                comboDist.addItem("SAMEGUA");
                                comboDist.addItem("SAN CRISTOBAL – CALACOA");
                                comboDist.addItem("TORATA");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("General Sanchez Cerro")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CHOJATA");
                                comboDist.addItem("COALAQUE");
                                comboDist.addItem("ICHUÑA");
                                comboDist.addItem("LA CAPILLA");
                                comboDist.addItem("LLOQUE");
                                comboDist.addItem("MATALAQUE");
                                comboDist.addItem("OMATE");
                                comboDist.addItem("PUQUINA");
                                comboDist.addItem("QUINISTAQUILLAS ");
                                comboDist.addItem("UBINAS");
                                comboDist.addItem("YUNGA ");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Ilo")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("EL ALGARROBAL");
                                comboDist.addItem("ILO");
                                comboDist.addItem("PACOCHA");
                            }
                            
                        }
                    });
                    
            
                }
                if(comboDep.getSelectedItem().toString().equals("Pasco")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Pasco");
                    comboProv.addItem("Daniel Alcides Carrion");
                    comboProv.addItem("Oxapampa");
                    comboProv.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if(comboProv.getSelectedItem().toString().equals("Pasco")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CHAUPIMARCA");
                                comboDist.addItem("HUACHÓN");
                                comboDist.addItem("HUARIACA");
                                comboDist.addItem("HUAYLLAY");
                                comboDist.addItem("NINACACA");
                                comboDist.addItem("PALLANCHACRA");
                                comboDist.addItem("PAUCARTAMBO");
                                comboDist.addItem("SAN FRANCISCO DE ASIS DE YARUSYACAN");
                                comboDist.addItem("SIMÓN BOLÍVAR");
                                comboDist.addItem("TICLACAYAN");
                                comboDist.addItem("TINYAHUARCO");
                                comboDist.addItem("VICCO");
                                comboDist.addItem("YANACANCHA");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Daniel Alcides Carrion")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CHACAYAN");
                                comboDist.addItem("GOYLLARISQUIZGA");
                                comboDist.addItem("PAUCAR");
                                comboDist.addItem("SAN PEDRO DE PILLAO");
                                comboDist.addItem("SANTA ANA DE TUSI");
                                comboDist.addItem("TAPUC");
                                comboDist.addItem("VILCABAMBA");
                                comboDist.addItem("YANAHUANCA");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Oxapampa")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CHONTABAMBA");
                                comboDist.addItem("HUANCABAMBA");
                                comboDist.addItem("OXAPAMPA");
                                comboDist.addItem("PALCAZU");
                                comboDist.addItem("POZUZO");
                                comboDist.addItem("PUERTO BERMÚDEZ");
                                comboDist.addItem("VILLA RICA");
                            } 
                            
                        }
                    });
                    
            
                }
                if(comboDep.getSelectedItem().toString().equals("Piura")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Piura");
                    comboProv.addItem("Ayabaca");
                    comboProv.addItem("Huancabamba");
                    comboProv.addItem("Morropon");
                    comboProv.addItem("Paita");
                    comboProv.addItem("Sechura");
                    comboProv.addItem("Sullana");
                    comboProv.addItem("Talara");
                    comboProv.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if(comboProv.getSelectedItem().toString().equals("Piura")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CASTILLA");
                                comboDist.addItem("CATACAOS");
                                comboDist.addItem("CURA MORI");
                                comboDist.addItem("EL TALLÁN");
                                comboDist.addItem("LA ARENA");
                                comboDist.addItem("LA UNIÓN");
                                comboDist.addItem("LAS LOMAS");
                                comboDist.addItem("PIURA");
                                comboDist.addItem("TAMBOGRANDE");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Ayabaca")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("AYABACA");
                                comboDist.addItem("FRÍAS");
                                comboDist.addItem("JILILÍ");
                                comboDist.addItem("LAGUNAS");
                                comboDist.addItem("MONTERO");
                                comboDist.addItem("PACAIPAMPA");
                                comboDist.addItem("PAIMAS");
                                comboDist.addItem("SAPILLICA");
                                comboDist.addItem("SÍCCHEZ");
                                comboDist.addItem("SUYO");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Huancabamba")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CANCHAQUE");
                                comboDist.addItem("EL CARMEN DE LA FRONTERA");
                                comboDist.addItem("HUANCABAMBA");
                                comboDist.addItem("HUARMACA");
                                comboDist.addItem("LALAQUIZ");
                                comboDist.addItem("SAN MIGUEL DE EL FAIQUE");
                                comboDist.addItem("SÓNDOR");
                                comboDist.addItem("SONDORILLO");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Morropon")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("BUENOS AIRES");
                                comboDist.addItem("CHALACO");
                                comboDist.addItem("CHULUCANAS");
                                comboDist.addItem("LA MATANZA");
                                comboDist.addItem("MORROPÓN");
                                comboDist.addItem("SALITRAL");
                                comboDist.addItem("SAN JUAN DE BIGOTE");
                                comboDist.addItem("SANTA CATALINA DE MOSSA");
                                comboDist.addItem("SANTO DOMINGO");
                                comboDist.addItem("YAMANGO");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Paita")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("AMOTAPE");
                                comboDist.addItem("ARENAL");
                                comboDist.addItem("COLÁN");
                                comboDist.addItem("LA HUACA");
                                comboDist.addItem("PAITA");
                                comboDist.addItem("TAMARINDO");
                                comboDist.addItem("VICHAYAL");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Sechura")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("BELLAVISTA DE LA UNION");
                                comboDist.addItem("BERNAL");
                                comboDist.addItem("CRISTO NOS VALGA");
                                comboDist.addItem("RINCONADA LLICUAR");
                                comboDist.addItem("SECHURA");
                                comboDist.addItem("VICE");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Sullana")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("BELLAVISTA");
                                comboDist.addItem("IGNACIO ESCUDERO");
                                comboDist.addItem("LANCONES");
                                comboDist.addItem("MARCAVELICA");
                                comboDist.addItem("MIGUEL CHECA");
                                comboDist.addItem("QUERECOTILLO");
                                comboDist.addItem("SALITRAL");
                                comboDist.addItem("SULLANA");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Talara")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("EL ALTO");
                                comboDist.addItem("LA BREA");
                                comboDist.addItem("LOBITOS");
                                comboDist.addItem("LOS ÓRGANOS");
                                comboDist.addItem("MÁNCORA");
                                comboDist.addItem("PARIÑAS");
                            } 
                            
                        }
                    });
                    
            
                }
                if(comboDep.getSelectedItem().toString().equals("Puno")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Puno");
                    comboProv.addItem("Azangaro");
                    comboProv.addItem("Carabaya");
                    comboProv.addItem("Chucuito");
                    comboProv.addItem("El Collao");
                    comboProv.addItem("Huancane");
                    comboProv.addItem("Lampa");
                    comboProv.addItem("Melgar");
                    comboProv.addItem("Moho");
                    comboProv.addItem("San Antonio de Putina");
                    comboProv.addItem("San Roman");
                    comboProv.addItem("Sandia");
                    comboProv.addItem("Yunguyo");
                    comboProv.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if(comboProv.getSelectedItem().toString().equals("Puno")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ACORA");
                                comboDist.addItem("AMANTANI");
                                comboDist.addItem("ATUNCOLLA");
                                comboDist.addItem("CAPACHICA");
                                comboDist.addItem("CHUCUITO");
                                comboDist.addItem("COATA");
                                comboDist.addItem("HUATA");
                                comboDist.addItem("MAÑAZO");
                                comboDist.addItem("PAUCARCOLLA");
                                comboDist.addItem("PICHACANI");
                                comboDist.addItem("PLATERIA");
                                comboDist.addItem("PUNO");
                                comboDist.addItem("SAN ANTONIO");
                                comboDist.addItem("TIQUILLACA");
                                comboDist.addItem("VILQUE");
                                
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Azangaro")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ACHAYA");
                                comboDist.addItem("ARAPA");
                                comboDist.addItem("ASILLO");
                                comboDist.addItem("AZANGARO");
                                comboDist.addItem("CAMINACA");
                                comboDist.addItem("CHUPA");
                                comboDist.addItem("JOSE DOMINGO CHOQUEHUANCA");
                                comboDist.addItem("MUÑANI");
                                comboDist.addItem("POTONI");
                                comboDist.addItem("SAMAN");
                                comboDist.addItem("SAN ANTON");
                                comboDist.addItem("SAN JOSE");
                                comboDist.addItem("SAN JUAN DE SALINAS");
                                comboDist.addItem("SANTIAGO DE PUPUJA");
                                comboDist.addItem("TIRAPATA");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Carabaya")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("AJOYANI");
                                comboDist.addItem("AYAPATA");
                                comboDist.addItem("COASA");
                                comboDist.addItem("CORANI");
                                comboDist.addItem("CRUCERO");
                                comboDist.addItem("ITUATA");
                                comboDist.addItem("MACUSANI");
                                comboDist.addItem("OLLACHEA");
                                comboDist.addItem("SAN GABAN");
                                comboDist.addItem("USICAYOS");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Chucuito")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("DESAGUADERO");
                                comboDist.addItem("HUACULLANI");
                                comboDist.addItem("JULI");
                                comboDist.addItem("KELLUYO");
                                comboDist.addItem("PISACOMA");
                                comboDist.addItem("POMATA");
                                comboDist.addItem("ZEPITA");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("El Collao")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CAPAZO");
                                comboDist.addItem("CONDURIRI");
                                comboDist.addItem("ILAVE");
                                comboDist.addItem("PILCUYO");
                                comboDist.addItem("SANTA ROSA");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Huancane")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("COJATA");
                                comboDist.addItem("HUANCANE");
                                comboDist.addItem("HUATASANI");
                                comboDist.addItem("INCHUPALLA");
                                comboDist.addItem("PUSI");
                                comboDist.addItem("ROSASPATA");
                                comboDist.addItem("TARACO");
                                comboDist.addItem("VILQUE CHICO");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Lampa")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CABANILLA");
                                comboDist.addItem("CALAPUJA");
                                comboDist.addItem("LAMPA");
                                comboDist.addItem("NICASIO");
                                comboDist.addItem("OCUVIRI");
                                comboDist.addItem("PALCA");
                                comboDist.addItem("PARATIA");
                                comboDist.addItem("PUCARA");
                                comboDist.addItem("SANTA LUCIA");
                                comboDist.addItem("VILAVILA");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Melgar")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ANTAUTA");
                                comboDist.addItem("AYAVIRI");
                                comboDist.addItem("CUPI");
                                comboDist.addItem("LLALLI");
                                comboDist.addItem("MACARI");
                                comboDist.addItem("NUÑOA");
                                comboDist.addItem("ORURILLO");
                                comboDist.addItem("SANTA ROSA");
                                comboDist.addItem("UMACHIRI");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Moho")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CONIMA");
                                comboDist.addItem("HUAYRAPATA");
                                comboDist.addItem("MOHO");
                                comboDist.addItem("TILALI");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("San Antonio de Putina")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ANANEA");
                                comboDist.addItem("PEDRO VILCA APAZA");
                                comboDist.addItem("PUTINA");
                                comboDist.addItem("QUILCAPUNCU");
                                comboDist.addItem("SINA");
                                
                            } 
                            if(comboProv.getSelectedItem().toString().equals("San Roman")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CABANA");
                                comboDist.addItem("CABANILLAS");
                                comboDist.addItem("CARACOTO");
                                comboDist.addItem("JULIACA");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Sandia")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ALTO INAMBARI");
                                comboDist.addItem("CUYOCUYO");
                                comboDist.addItem("LIMBANI");
                                comboDist.addItem("PATAMBUCO");
                                comboDist.addItem("PHARA");
                                comboDist.addItem("QUIACA");
                                comboDist.addItem("SAN JUAN DEL ORO");
                                comboDist.addItem("SAN PEDRO DE PUTINA PUNCO");
                                comboDist.addItem("SANDIA");
                                comboDist.addItem("YANAHUAYA");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Yunguyo")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ANAPIA");
                                comboDist.addItem("COPANI");
                                comboDist.addItem("CUTURAPI");
                                comboDist.addItem("OLLARAYA");
                                comboDist.addItem("TINICACHI");
                                comboDist.addItem("TINICACHI");
                                comboDist.addItem("TINICACHI");
                            } 
                            
                        }
                    });
            
                }
                if(comboDep.getSelectedItem().toString().equals("San Martín")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Moyobamba");
                    comboProv.addItem("Bellavista");
                    comboProv.addItem("El Dorado");
                    comboProv.addItem("Huallaga");
                    comboProv.addItem("Lamas");
                    comboProv.addItem("Mariscal Caceres");
                    comboProv.addItem("Picota");
                    comboProv.addItem("Rioja");
                    comboProv.addItem("San Martin");
                    comboProv.addItem("Tocache");
                    comboProv.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if(comboProv.getSelectedItem().toString().equals("Moyobamba")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CALZADA");
                                comboDist.addItem("HABANA");
                                comboDist.addItem("JEPELACIO");
                                comboDist.addItem("MOYOBAMBA");
                                comboDist.addItem("SORITOR");
                                comboDist.addItem("YANTALO");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Bellavista")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ALTO BIAVO");
                                comboDist.addItem("BAJO BIAVO");
                                comboDist.addItem("BELLAVISTA");
                                comboDist.addItem("HUALLAGA");
                                comboDist.addItem("SAN PABLO");
                                comboDist.addItem("SAN RAFAEL");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("El Dorado")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("AGUA BLANCA");
                                comboDist.addItem("SAN JOSÉ DE SISA");
                                comboDist.addItem("SAN MARTÍN");
                                comboDist.addItem("SANTA ROSA");
                                comboDist.addItem("SHATOJA");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Huallaga")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ALTO SAPOSOA");
                                comboDist.addItem("EL ESLABÓN");
                                comboDist.addItem("PISCOYACU");
                                comboDist.addItem("SACANCHE");
                                comboDist.addItem("SAPOSOA");
                                comboDist.addItem("TINGO DE SAPOSOA");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Lamas")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ALONSO DE ALVARADO");
                                comboDist.addItem("BARRANQUITA");
                                comboDist.addItem("CAYNARACHI");
                                comboDist.addItem("CUÑUMBUQUI");
                                comboDist.addItem("LAMAS");
                                comboDist.addItem("PINTO RECODO");
                                comboDist.addItem("RUMISAPA");
                                comboDist.addItem("SAN ROQUE DE CUMBAZA");
                                comboDist.addItem("SHANAO");
                                comboDist.addItem("TABALOSOS");
                                comboDist.addItem("ZAPATERO");
                                

                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Mariscal Caceres")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CAMPANILLA");
                                comboDist.addItem("HUICUNGO");
                                comboDist.addItem("JUANJUÍ");
                                comboDist.addItem("PACHIZA");
                                comboDist.addItem("PAJARILLO");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Picota")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("BUENOS AIRES");
                                comboDist.addItem("CASPISAPA");
                                comboDist.addItem("PICOTA");
                                comboDist.addItem("PILLUANA");
                                comboDist.addItem("PUCACACA");
                                comboDist.addItem("SAN CRISTÓBAL");
                                comboDist.addItem("SAN HILARIÓN");
                                comboDist.addItem("SHAMBOYACU");
                                comboDist.addItem("TINGO DE PONASA");
                                comboDist.addItem("TRES UNIDOS");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Rioja")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("AWAJÚN");
                                comboDist.addItem("ELÍAS SOPLÍN VARGAS");
                                comboDist.addItem("NUEVA CAJAMARCA");
                                comboDist.addItem("PARDO MIGUEL");
                                comboDist.addItem("POSIC");
                                comboDist.addItem("RIOJA");
                                comboDist.addItem("SAN FERNANDO");
                                comboDist.addItem("YORONGOS");
                                comboDist.addItem("YURACYACU");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("San Martin")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ALBERTO LEVEAU");
                                comboDist.addItem("CACATACHI");
                                comboDist.addItem("CHAZUTA");
                                comboDist.addItem("CHIPURANA");
                                comboDist.addItem("EL PORVENIR");
                                comboDist.addItem("HUIMBAYOC");
                                comboDist.addItem("JUAN GUERRA");
                                comboDist.addItem("LA BANDA DE SHILCAYO");
                                comboDist.addItem("MORALES");
                                comboDist.addItem("PAPAPLAYA");
                                comboDist.addItem("SAN ANTONIO");
                                comboDist.addItem("SAUCE");
                                comboDist.addItem("SHAPAJA");
                                comboDist.addItem("TARAPOTO");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Tocache")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("NUEVO PROGRESO");
                                comboDist.addItem("PÓLVORA");
                                comboDist.addItem("SHUNTE");
                                comboDist.addItem("TOCACHE");
                                comboDist.addItem("UCHIZA");
                            } 
                        }
                    });
            
                }
                if(comboDep.getSelectedItem().toString().equals("Tacna")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Tacna");
                    comboProv.addItem("Candarave");
                    comboProv.addItem("Jorge Basadre");
                    comboProv.addItem("Tarata");
                    comboProv.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if(comboProv.getSelectedItem().toString().equals("Tacna")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ALTO DE LA ALIANZA");
                                comboDist.addItem("CALANA");
                                comboDist.addItem("CIUDAD NUEVA");
                                comboDist.addItem("CORONEL GREGORIO ALBARRACÍN LANCHIPA");
                                comboDist.addItem("INCLÁN");
                                comboDist.addItem("PACHÍA");
                                comboDist.addItem("PALCA");
                                comboDist.addItem("POCOLLAY");
                                comboDist.addItem("SAMA");
                                comboDist.addItem("TACNA");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Candarave")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CAIRANI");
                                comboDist.addItem("CAMILACA");
                                comboDist.addItem("CANDARAVE");
                                comboDist.addItem("CURIBAYA");
                                comboDist.addItem("HUANUARA");
                                comboDist.addItem("QUILAHUANI");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Jorge Basadre")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ILABAYA");
                                comboDist.addItem("ITE");
                                comboDist.addItem("LOCUMBA");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Tarata")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("ESTIQUE");
                                comboDist.addItem("ESTIQUE PAMPA");
                                comboDist.addItem("HÉROES ALBARRACÍN");
                                comboDist.addItem("SITAJARA");
                                comboDist.addItem("SUSAPAYA");
                                comboDist.addItem("TARATA");
                                comboDist.addItem("TARUCACHI");
                                comboDist.addItem("TICACO");
                            } 
                        }
                    });
            
                }
                if(comboDep.getSelectedItem().toString().equals("Tumbes")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Tumbes");
                    comboProv.addItem("Contralmirante Villar");
                    comboProv.addItem("Zarumilla");
                    comboProv.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if(comboProv.getSelectedItem().toString().equals("Tumbes")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CORRALES");
                                comboDist.addItem("LA CRUZ");
                                comboDist.addItem("PAMPAS DE HOSPITAL");
                                comboDist.addItem("SAN JACINTO");
                                comboDist.addItem("SAN JUAN DE LA VIRGEN");
                                comboDist.addItem("TUMBES");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Contralmirante Villar")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CASITAS");
                                comboDist.addItem("ZORRITOS");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Zarumilla")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("AGUAS VERDES");
                                comboDist.addItem("MATAPALO");
                                comboDist.addItem("PAPAYAL");
                                comboDist.addItem("ZARUMILLA");
                            }
                            
                        }
                    });
            
                }
                if(comboDep.getSelectedItem().toString().equals("Ucayali")){
                    comboProv.removeAllItems();
                    comboProv.addItem("<PROVINCIA>");
                    comboProv.addItem("Coronel Portillo");
                    comboProv.addItem("Atalaya");
                    comboProv.addItem("Padre Abad");
                    comboProv.addItem("Purus");
                    comboProv.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if(comboProv.getSelectedItem().toString().equals("Coronel Portillo")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CALLERÍA");
                                comboDist.addItem("CAMPOVERDE");
                                comboDist.addItem("IPARÍA");
                                comboDist.addItem("MANANTAY");
                                comboDist.addItem("MASISEA");
                                comboDist.addItem("NUEVA REQUENA");
                                comboDist.addItem("YARINACOCHA");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Atalaya")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("RAYMONDI");
                                comboDist.addItem("SEPAHUA");
                                comboDist.addItem("TAHUANÍA");
                                comboDist.addItem("YURÚA");
                            } 
                            if(comboProv.getSelectedItem().toString().equals("Padre Abad")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("CURIMANÁ");
                                comboDist.addItem("IRAZOLA");
                                comboDist.addItem("PADRE ABAD");
                            }                    
                            if(comboProv.getSelectedItem().toString().equals("Purus")){
                                comboDist.removeAllItems();
                                comboDist.addItem("<DISTRITO>");
                                comboDist.addItem("PURÚS");
                            } 
                            
                        }
                    });
                   
            
                }
            }
            
        });
        
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        lRuc = new javax.swing.JLabel();
        lRazon = new javax.swing.JLabel();
        lDireccion = new javax.swing.JLabel();
        lAsignado = new javax.swing.JLabel();
        lDepa = new javax.swing.JLabel();
        lProv = new javax.swing.JLabel();
        lDistrito = new javax.swing.JLabel();
        textRuc = new javax.swing.JTextField();
        textRaz = new javax.swing.JTextField();
        textDirec = new javax.swing.JTextField();
        comboProv = new javax.swing.JComboBox<>();
        comboDep = new javax.swing.JComboBox<>();
        comboDist = new javax.swing.JComboBox<>();
        textAsignado = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        textContacto = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        textCargo = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        textTelefono = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        textMeta = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        bGrabar = new javax.swing.JButton();
        bSalir = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        titulo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);

        lRuc.setText("RUC:");

        lRazon.setText("Razon Social:");

        lDireccion.setText("Direccion:");

        lAsignado.setText("Promotor asignado:");

        lDepa.setText("Departamento:");

        lProv.setText("Provincia:");

        lDistrito.setText("Distrito:");

        textRuc.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        textRuc.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textRucKeyTyped(evt);
            }
        });

        textRaz.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N

        textDirec.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N

        comboProv.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "<PROVINCIA>" }));

        comboDep.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "<DEPARTAMENTO>" }));
        comboDep.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboDepActionPerformed(evt);
            }
        });

        comboDist.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "<DISTRITO>" }));

        textAsignado.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione una Opcion" }));

        jLabel1.setText("Contacto:");

        textContacto.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N

        jLabel2.setText("Cargo:");

        textCargo.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N

        jLabel3.setText("Teléfono:");

        textTelefono.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        textTelefono.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textTelefonoKeyTyped(evt);
            }
        });

        jLabel4.setText("Meta:");

        textMeta.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textMetaKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lDireccion, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(lProv, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel3))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(textContacto)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(comboProv, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addComponent(textTelefono)))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(lRuc, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(textRuc, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
                                    .addComponent(textDirec, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE))))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(lRazon)
                                .addGap(18, 18, 18)
                                .addComponent(textRaz, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lDepa)
                                    .addComponent(lDistrito)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel4))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(textCargo, javax.swing.GroupLayout.DEFAULT_SIZE, 151, Short.MAX_VALUE)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(comboDep, 0, 151, Short.MAX_VALUE)
                                            .addComponent(comboDist, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addComponent(textMeta)))))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(lAsignado)
                        .addGap(18, 18, 18)
                        .addComponent(textAsignado, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(122, 122, 122))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lRuc)
                    .addComponent(lRazon)
                    .addComponent(textRuc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(textRaz, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lDireccion)
                    .addComponent(textDirec, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lDepa)
                    .addComponent(comboDep, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lProv)
                    .addComponent(comboProv, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lDistrito)
                    .addComponent(comboDist, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(textContacto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(textCargo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3)
                        .addComponent(textTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel4)
                        .addComponent(textMeta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lAsignado)
                    .addComponent(textAsignado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(27, Short.MAX_VALUE))
        );

        bGrabar.setText("Grabar");
        bGrabar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bGrabarMouseClicked(evt);
            }
        });

        bSalir.setText("Salir");
        bSalir.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bSalirMouseClicked(evt);
            }
        });
        bSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bSalirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(310, Short.MAX_VALUE)
                .addComponent(bGrabar, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(38, 38, 38)
                .addComponent(bSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bGrabar)
                    .addComponent(bSalir))
                .addContainerGap(18, Short.MAX_VALUE))
        );

        jPanel1.setBackground(new java.awt.Color(246, 71, 71));

        titulo.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        titulo.setForeground(new java.awt.Color(255, 255, 255));
        titulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        titulo.setText("Crear Cliente");
        titulo.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                tituloMouseDragged(evt);
            }
        });
        titulo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tituloMousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(titulo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(titulo)
                .addGap(0, 6, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 545, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bSalirActionPerformed
        dispose();

    }//GEN-LAST:event_bSalirActionPerformed

    private void tituloMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tituloMouseDragged
        int x = evt.getXOnScreen();
        int y = evt.getYOnScreen();
        this.setLocation(x-xx,y-xy);
    }//GEN-LAST:event_tituloMouseDragged

    private void tituloMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tituloMousePressed
        xx=evt.getX();
        xy=evt.getY();
    }//GEN-LAST:event_tituloMousePressed

    private void bSalirMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bSalirMouseClicked
        dispose();
    }//GEN-LAST:event_bSalirMouseClicked
    public void completarCombo(){
        
        try{
            ResultSet rs = st.executeQuery("select promApellidos,promRuc from promotores where promActiv='ACTIVO' order by promApellidos asc");
            while(rs.next()){
                textAsignado.addItem(rs.getString("promApellidos"));
                promotores.add(rs.getString("promApellidos"));
                ruc.add(rs.getString("promRuc"));
            }
        }catch(SQLException e){
            System.err.println("El error: "+ e);
        };
        
    }
    
    private void bGrabarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bGrabarMouseClicked
        String ruc=textRuc.getText();
        String razon=textRaz.getText();
        String direc=textDirec.getText();
        String promotorAsig=(String) textAsignado.getSelectedItem();
        String depa=(String) comboDep.getSelectedItem();
        String provin=(String) comboProv.getSelectedItem();
        String dist=(String) comboDist.getSelectedItem();
        String contacto = textContacto.getText();
        String cargoContacto = textCargo.getText();
        String telefono = textTelefono.getText();
        int meta = Integer.parseInt(textMeta.getText());
        conexionBD conectar = new conexionBD();
        conectar.conexion();
        ResultSet rs = null;
        String query = "INSERT INTO CLIENTES(clRuc,clRazSoc,clDirec,clContacto,cLCargoContacto,clTelefonoContacto,clDep,clProv,clDistr,clPromAsig,clEstad,clMeta) VALUES(?,?,?,?,?,?,?,?,?,?,1,?)";
        if(textRuc.getText().length()< 11 || depa.equals("<DEPARTAMENTO>") || provin.equals("<PROVINCIA>") || dist.equals("<DISTRITO>") ){
            JOptionPane.showOptionDialog(null, "No se puede grabar, verifique información", "Error", JOptionPane.DEFAULT_OPTION,
             JOptionPane.ERROR_MESSAGE, null, null, null);
        }
        else{
            try{
                
                
                PreparedStatement ps = conectar.cn.prepareStatement(query);         
                ps.setString(1,ruc);
                ps.setString(2,razon);
                ps.setString(3,direc);
                ps.setString(4,contacto);
                ps.setString(5,cargoContacto);
                ps.setString(6,telefono);
                ps.setString(7,depa);
                ps.setString(8,provin);
                ps.setString(9,dist);
                ps.setString(10,promotorAsig);
                ps.setInt(11, meta);
                if(!conectar.ifExists("Select * from CLIENTES where clRuc=?;", ruc)){
                    int ejecucion =    ps.executeUpdate();
                    int mensaje = JOptionPane.showOptionDialog(null, "Se grabo correctamente", "Grabado", JOptionPane.DEFAULT_OPTION,
                     JOptionPane.INFORMATION_MESSAGE, null, null, null);
                    //System.out.println(mensaje);
                    if(mensaje==0){
                        dispose();
                    }
                }else{
                    int mensaje = JOptionPane.showOptionDialog(null, "Error al grabar, ya existe cliente", "ERROR", JOptionPane.DEFAULT_OPTION,
                     JOptionPane.ERROR_MESSAGE, null, null, null);
                    
                }
                
                

            }catch(SQLException e){
                System.err.println("El error" + e);
            }
        }
        
    }//GEN-LAST:event_bGrabarMouseClicked

    private void textRucKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textRucKeyTyped
        char c = evt.getKeyChar();
        if(!Character.isDigit(c)){
            evt.consume();
        }
        if(textRuc.getText().length()== 11){
            evt.consume();
        }
    }//GEN-LAST:event_textRucKeyTyped

    private void comboDepActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboDepActionPerformed
        
    }//GEN-LAST:event_comboDepActionPerformed

    private void textMetaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textMetaKeyTyped
        char c=evt.getKeyChar();
        if(!isNumeric(String.valueOf(c))){
            getToolkit().beep();
                evt.consume();
        }
        
        if(textMeta.getText().length()==10){
            evt.consume();
        }
    }//GEN-LAST:event_textMetaKeyTyped

    private void textTelefonoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textTelefonoKeyTyped
        char c=evt.getKeyChar();
        if(!isNumeric(String.valueOf(c))){
            getToolkit().beep();
                evt.consume();
        }
        
        if(textTelefono.getText().length()==9){
            evt.consume();
        }
    }//GEN-LAST:event_textTelefonoKeyTyped
   
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(creaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(creaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(creaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(creaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                creaCliente dialog = new creaCliente(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bGrabar;
    private javax.swing.JButton bSalir;
    private javax.swing.JComboBox<String> comboDep;
    private javax.swing.JComboBox<String> comboDist;
    private javax.swing.JComboBox<String> comboProv;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JLabel lAsignado;
    private javax.swing.JLabel lDepa;
    private javax.swing.JLabel lDireccion;
    private javax.swing.JLabel lDistrito;
    private javax.swing.JLabel lProv;
    private javax.swing.JLabel lRazon;
    private javax.swing.JLabel lRuc;
    private javax.swing.JComboBox<String> textAsignado;
    private javax.swing.JTextField textCargo;
    private javax.swing.JTextField textContacto;
    private javax.swing.JTextField textDirec;
    private javax.swing.JTextField textMeta;
    private javax.swing.JTextField textRaz;
    private javax.swing.JTextField textRuc;
    private javax.swing.JTextField textTelefono;
    private javax.swing.JLabel titulo;
    // End of variables declaration//GEN-END:variables
}
